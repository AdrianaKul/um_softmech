from numpy import *
import time
#import xml.etree.ElementTree as ET
import sys
import os

def get_force(fname):
	f=open(fname,'r')
	result=[];
	new=True

	for r in f.readlines():
        	if r[0]=='*':
                	if not new:
                        	result.append(t)
                       		new=True
                	if r[1:5]=='Time':
                        	r=r.split()[-1]
                       		t= [float(r)]
               		continue

        	r1=map(float,r.split())
        	if new:
                	new=False
                	t.append(r1[1])
       		else:
                	t.append(r1[1])
	result.append(t)
	final = array(result)
	return final

def convert_str(c):
	if isinstance(c, float):
		return '%.16g' %(c)
	d=[]
	for i in range(0,len(c)):
		d.append('%.16g' %(c[i]))
	return d

def convert_str_int(c):
	s=convert_str(c)
	return array(map(float,s))

def cal_vec(filename,cvalue,que):
	tic = time.time()
        ################################## REPLACE THE MAT PARAMETERS #################################
        tree = ET.parse(inputfilename+'.feb')
        root=tree.getroot()
        for material in root.iter('material'):
                for E1 in material.iter('E'):
                        #E1.text=str(cvalue[0]);
                        E1.text = convert_str(cvalue[0])
        '''        for E1 in material.iter('B'):
                        #E1.text=str(cvalue[1]);
                        #E1.text = '%.16g' %(cvalue[1]);
                        E1.text = convert_str(cvalue[1]);
                for E1 in material.iter('E'):
                        E1.text=str(0.2);
                for E1 in material.iter('element_wise'):
                        E1.text=str(1);'''
	outfiles=[]
	for n in root.iter('rigid_body_data'):
		name=n.get('file')
		n.set('file',filename+name)
		outfiles += [filename+name]
        tree.write(filename+'.feb',xml_declaration=True,encoding="ISO-8859-1")

	##################################### SUBMIT THE JOB ###########################################
	print "Submitting the job %s to FEBio" %(filename);
	sys.stdout.flush();
        os.environ['OMP_NUM_THREADS']="1"
        os.system('$HOME/codes/FEBio/febiosource-2.4.0/build/bin/febio2.lnx64 -cnf $HOME/codes/FEBio/febiosource-2.4.0/build/bin/febio.xml -i ' + filename + '.feb -silent')

	toc = time.time()

        ################################# CHECK IF IT NORMALLY TERMINATED ###############################
        f=open(filename+'.log','r')
        f.seek(-38,2)
        if(not f.readline()==' N O R M A L   T E R M I N A T I O N\n'):
                print "Error: Simulation %s.feb did not end normally in %5.2f minutes. Exiting." %(filename,(toc-tic)/60.);
                sys.exit(1);
        f.close();

	print "Simulation %s completed in %5.2f minutes" %(filename,(toc-tic)/60.);
	#disp("-----------------------------------------------");

	###################################### READ THE SH VALUES #####################################
	y=get_force(outfiles[0])
	y=y[:,1]
	y=y[-1]
	###################################### PUT THE RESULTS IN QUEUE ###############################
	que.put(y.flatten('F'))
	return

def constraints(c):
	if min(c)<0:
		return False
	return True

inputfilename=None
