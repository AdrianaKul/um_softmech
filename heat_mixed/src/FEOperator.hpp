#ifndef __FEOPERATOR_HPP__
#define __FEOPERATOR_HPP__

#include <stdlib.h>
#include <Python/Python.h>
#include "../pyhelper.hpp"
#include <BasicFiniteElements.hpp>

namespace Operators{

double dose = 5e-5;
double molar_wt = 853.906;
double vol_coat = 6.0319e-9;
double vol_wall = 1.2517e-6;

double lengthScale = 1e-3;
double timeScale = 12 * 60 * 60;
double ctrnScale = dose / (molar_wt * vol_coat);
double auxScale = ctrnScale; // 0 .127;
double rho_wall = 983000;

double k_on = 0.17;
double k_off = 5.27e-4;
double b_max = 0.127;
double D_c = 1e-13;
double Dm_r = 2e-12;
double Dm_z = 5e-11;

using FaceEle = MoFEM::FaceElementForcesAndSourcesCoreSwitch<
                FaceElementForcesAndSourcesCore::NO_HO_GEOMETRY |
                FaceElementForcesAndSourcesCore::NO_CONTRAVARIANT_TRANSFORM_HDIV |
                FaceElementForcesAndSourcesCore::NO_COVARIANT_TRANSFORM_HCURL>;

using EdgeEle = MoFEM::EdgeElementForcesAndSourcesCoreSwitch<
                EdgeElementForcesAndSourcesCore::NO_HO_GEOMETRY |
                EdgeElementForcesAndSourcesCore::NO_COVARIANT_TRANSFORM_HCURL>;

using OpFaceEle = FaceEle::UserDataOperator;
using OpEdgeEle = EdgeEle::UserDataOperator;

using EntData = DataForcesAndSourcesCore::EntData;

using PostProc = PostProcFaceOnRefinedMesh;

const double K = 1e-2;
int save_every_nth_step = 1;
auto data_fp = fopen("data_file.csv", "w");

struct BlockData {
  int iD;
  double radialDiffuse;
  double axialDiffuse;
  double oN_ks;
  double oFf_ks;
  double mAx_bs;
  double initVal;

  int nFields;
  int dIm;
  std::vector<std::string> fieldNames;
  Range eNts;

  BlockData()
      : radialDiffuse(-1), axialDiffuse(-1), oN_ks(-1), oFf_ks(-1), mAx_bs(-1),
        nFields(-1), dIm(-1), initVal(0) {}
};


struct CommonData {
  MatrixDouble jAc;
  MatrixDouble invJac;

  boost::shared_ptr<VectorDouble> freeDrugValPtr;
  boost::shared_ptr<VectorDouble> boundDrugValPtr;

  boost::shared_ptr<VectorDouble> freeDrugDotPtr;
  boost::shared_ptr<VectorDouble> boundDrugDotPtr;

  boost::shared_ptr<MatrixDouble> freeDrugGradPtr;
  boost::shared_ptr<MatrixDouble> freeDrugFluxPtr;

  boost::shared_ptr<VectorDouble> freeDrugDivPtr;

  CommonData() {
    jAc.resize(3, 3, false);
    invJac.resize(3, 3, false);

    freeDrugValPtr = boost::shared_ptr<VectorDouble>(new VectorDouble());
    boundDrugValPtr = boost::shared_ptr<VectorDouble>(new VectorDouble());

    freeDrugDotPtr = boost::shared_ptr<VectorDouble>(new VectorDouble());
    boundDrugDotPtr = boost::shared_ptr<VectorDouble>(new VectorDouble());

    freeDrugGradPtr = boost::shared_ptr<MatrixDouble>(new MatrixDouble());
    freeDrugFluxPtr = boost::shared_ptr<MatrixDouble>(new MatrixDouble());

    freeDrugDivPtr = boost::shared_ptr<VectorDouble>(new VectorDouble());
  }
}; // CommonData

struct DataValues {
  double TotalDrugContent;
  double FreeDrugCoating;
  double FreeDrugWall;
  double BoundDrugWall;
  double SquareDrugContent;
  double CoatingVolume;
  double WallVolume;
  DataValues() 
        : TotalDrugContent(0)
        , FreeDrugWall(0)
        , FreeDrugCoating(0)
        , BoundDrugWall(0)
        , SquareDrugContent(0)
        , CoatingVolume(0)
        , WallVolume(0){}
};

struct OpFreeDrugSrc {
  OpFreeDrugSrc(boost::shared_ptr<CommonData> common_data_ptr)
      : commonDataPtr(common_data_ptr) {}
  MoFEMErrorCode operator()(VectorDouble &out_vec, MatrixDouble &gp_coords) {
    MoFEMFunctionBegin;
    const size_t nb_gauss_pts = gp_coords.size1();
    out_vec.resize(nb_gauss_pts, false);
    out_vec.clear();
    auto t_free_drug = getFTensor0FromVec(*commonDataPtr->freeDrugValPtr);
    auto t_bound_drug = getFTensor0FromVec(*commonDataPtr->boundDrugValPtr);
    auto t_out_vec = getFTensor0FromVec(out_vec);

    for (int gg = 0; gg != nb_gauss_pts; ++gg) {
      double fd = ctrnScale * t_free_drug;
      double bd = auxScale * t_bound_drug;

      double out = k_on * fd * (b_max - bd) - k_off * bd;

      t_out_vec = -(timeScale / ctrnScale) * out;

      ++t_free_drug;
      ++t_bound_drug;
      ++t_out_vec;
    }
    MoFEMFunctionReturn(0);
  }

private:
  boost::shared_ptr<CommonData> commonDataPtr;
};

struct OpBoundDrugSrc {
  OpBoundDrugSrc(boost::shared_ptr<CommonData> common_data_ptr)
      : commonDataPtr(common_data_ptr) {}
  MoFEMErrorCode operator()(VectorDouble &out_vec, MatrixDouble &gp_coords) {
    MoFEMFunctionBegin;
    const size_t nb_gauss_pts = gp_coords.size1();
    out_vec.resize(nb_gauss_pts, false);
    out_vec.clear();
    auto t_free_drug = getFTensor0FromVec(*commonDataPtr->freeDrugValPtr);
    auto t_bound_drug = getFTensor0FromVec(*commonDataPtr->boundDrugValPtr);

    auto t_out_vec = getFTensor0FromVec(out_vec);

    for (int gg = 0; gg != nb_gauss_pts; ++gg) {
      double fd = ctrnScale * t_free_drug;
      double bd = auxScale * t_bound_drug;

      double out = k_on * fd * (b_max - bd) - k_off * bd;

      t_out_vec = (timeScale / auxScale) * out;

      ++t_free_drug;
      ++t_bound_drug;
      ++t_out_vec;
    }
    MoFEMFunctionReturn(0);
  }

private:
  boost::shared_ptr<CommonData> commonDataPtr;
};

struct OpTotalDrug {
  OpTotalDrug(boost::shared_ptr<CommonData> common_data_ptr)
      : commonDataPtr(common_data_ptr) {}
  MoFEMErrorCode operator()(VectorDouble &out_vec, MatrixDouble &gp_coords) {
    MoFEMFunctionBegin;
    const size_t nb_gauss_pts = gp_coords.size1();
    out_vec.resize(nb_gauss_pts, false);
    out_vec.clear();
    auto t_free_drug = getFTensor0FromVec(*commonDataPtr->freeDrugValPtr);
    auto t_bound_drug = getFTensor0FromVec(*commonDataPtr->boundDrugValPtr);

    auto t_out_vec = getFTensor0FromVec(out_vec);

    for (int gg = 0; gg != nb_gauss_pts; ++gg) {
      t_out_vec = (t_free_drug + t_bound_drug);
      ++t_free_drug;
      ++t_bound_drug;
      ++t_out_vec;
    }
    MoFEMFunctionReturn(0);
  }

private:
  boost::shared_ptr<CommonData> commonDataPtr;
};

struct OpVolume {
  OpVolume(boost::shared_ptr<CommonData> common_data_ptr)
      : commonDataPtr(common_data_ptr) {}
  MoFEMErrorCode operator()(VectorDouble &out_vec, MatrixDouble &gp_coords) {
    MoFEMFunctionBegin;
    const size_t nb_gauss_pts = gp_coords.size1();
    out_vec.resize(nb_gauss_pts, false);
    out_vec.clear();
    auto t_out_vec = getFTensor0FromVec(out_vec);
 

    for (int gg = 0; gg != nb_gauss_pts; ++gg) {
      t_out_vec = 1;
      ++t_out_vec;
    }
    MoFEMFunctionReturn(0);
  }

private:
  boost::shared_ptr<CommonData> commonDataPtr;
};

struct OpSquareDrug {
  OpSquareDrug(boost::shared_ptr<CommonData> common_data_ptr)
      : commonDataPtr(common_data_ptr) {}
  MoFEMErrorCode operator()(VectorDouble &out_vec, MatrixDouble &gp_coords) {
    MoFEMFunctionBegin;
    const size_t nb_gauss_pts = gp_coords.size1();
    out_vec.resize(nb_gauss_pts, false);
    out_vec.clear();
    auto t_free_drug = getFTensor0FromVec(*commonDataPtr->freeDrugValPtr);
    auto t_bound_drug = getFTensor0FromVec(*commonDataPtr->boundDrugValPtr);

    auto t_out_vec = getFTensor0FromVec(out_vec);

    for (int gg = 0; gg != nb_gauss_pts; ++gg) {
      double tot = t_free_drug + t_bound_drug;
      t_out_vec = tot * tot;
      ++t_free_drug;
      ++t_bound_drug;
      ++t_out_vec;
    }
    MoFEMFunctionReturn(0);
  }

private:
  boost::shared_ptr<CommonData> commonDataPtr;
};

struct OpFreeDrug {
  OpFreeDrug(boost::shared_ptr<CommonData> common_data_ptr)
      : commonDataPtr(common_data_ptr) {}
  MoFEMErrorCode operator()(VectorDouble &out_vec, MatrixDouble &gp_coords) {
    MoFEMFunctionBegin;
    const size_t nb_gauss_pts = gp_coords.size1();
    out_vec.resize(nb_gauss_pts, false);
    out_vec.clear();
    auto t_free_drug = getFTensor0FromVec(*commonDataPtr->freeDrugValPtr);
    auto t_out_vec = getFTensor0FromVec(out_vec);

    for (int gg = 0; gg != nb_gauss_pts; ++gg) {
      t_out_vec = t_free_drug;
      ++t_free_drug;
      ++t_out_vec;
    }
    MoFEMFunctionReturn(0);
  }

private:
  boost::shared_ptr<CommonData> commonDataPtr;
};

struct OpBoundDrug {
  OpBoundDrug(boost::shared_ptr<CommonData> common_data_ptr)
      : commonDataPtr(common_data_ptr) {}
  MoFEMErrorCode operator()(VectorDouble &out_vec, MatrixDouble &gp_coords) {
    MoFEMFunctionBegin;
    const size_t nb_gauss_pts = gp_coords.size1();
    out_vec.resize(nb_gauss_pts, false);
    out_vec.clear();
    auto t_bound_drug = getFTensor0FromVec(*commonDataPtr->boundDrugValPtr);
    auto t_out_vec = getFTensor0FromVec(out_vec);

    for (int gg = 0; gg != nb_gauss_pts; ++gg) {
      t_out_vec = t_bound_drug;
      ++t_bound_drug;
      ++t_out_vec;
    }
    MoFEMFunctionReturn(0);
  }

private:
  boost::shared_ptr<CommonData> commonDataPtr;
};

struct OpRhsExplicit : OpFaceEle {
  typedef boost::function<void(VectorDouble &, MatrixDouble &)> VectorFunction;
  OpRhsExplicit(std::string     field_name,
                VectorFunction  vec_func,
                Range           *ents = NULL)
        : OpFaceEle(field_name, OpFaceEle::OPROW)
        , vecFunc(vec_func)
        , eNts(ents){}

  Range *eNts;
  VectorDouble  src_vec;
  VectorFunction  vecFunc;
  VectorDouble   loc_rhs;
  MatrixDouble   loc_lhs;

  MoFEMErrorCode doWork(int side, EntityType type, EntData &row_data) {
    MoFEMFunctionBegin;

    const int nb_row_dofs = row_data.getIndices().size();

    if (!nb_row_dofs)
      MoFEMFunctionReturnHot(0);

    if (eNts) {
      if (eNts->find(getFEEntityHandle()) == eNts->end())
        MoFEMFunctionReturnHot(0);
    }

    loc_rhs.resize(nb_row_dofs, false);
    loc_rhs.clear();

    loc_lhs.resize(nb_row_dofs, nb_row_dofs, false);
    loc_lhs.clear();

    const int nb_integration_pts = getGaussPts().size2();
    auto t_w = getFTensor0IntegrationWeight();
    const double vol = getMeasure();

    auto gp_coords = getCoordsAtGaussPts();
    vecFunc(src_vec, gp_coords);

    auto t_src = getFTensor0FromVec(src_vec);

    for (int gg = 0; gg != nb_integration_pts; ++gg) {
      auto t_row_shape = row_data.getFTensor0N(gg, 0);
      const double a = vol * t_w;
      for (int rr = 0; rr != nb_row_dofs; ++rr) {
        auto t_col_shape = row_data.getFTensor0N(gg, 0);
        loc_rhs[rr] += t_row_shape * t_src * a;
        for (int cc = 0; cc != nb_row_dofs; ++cc) {
          loc_lhs(rr, cc) += a * t_row_shape * t_col_shape;
          ++t_col_shape;
        }
        ++t_row_shape;
      }
      ++t_src;
      ++t_w;
    }
    cholesky_decompose(loc_lhs);
    cholesky_solve(loc_lhs, loc_rhs, ublas::lower());

    CHKERR VecSetOption(getFEMethod()->ts_F, VEC_IGNORE_NEGATIVE_INDICES,
                        PETSC_TRUE);
    CHKERR VecSetValues(getFEMethod()->ts_F, row_data, &*loc_rhs.begin(),
                        ADD_VALUES);

    MoFEMFunctionReturn(0);
  }
};

struct OpRhsHDIV : OpFaceEle {
  OpRhsHDIV(std::string                     flux_field,
            boost::shared_ptr<CommonData>   common_data_ptr,
            BlockData                       &block_data,
            Range                           *ents = NULL)
      : OpFaceEle(flux_field, OpFaceEle::OPROW), 
        commonDataPtr(common_data_ptr),
        blockData(block_data),
        eNts(ents){}

  boost::shared_ptr<CommonData> commonDataPtr;
  Range                         *eNts;
  VectorDouble                  loc_rhs;
  BlockData                     &blockData;

  MoFEMErrorCode doWork(int side, EntityType type, EntData &row_data) {
  MoFEMFunctionBegin;

  const int nb_row_dofs = row_data.getIndices().size();

  if (!nb_row_dofs)
    MoFEMFunctionReturnHot(0);

  if (eNts) {
    if (eNts->find(getFEEntityHandle()) == eNts->end())
      MoFEMFunctionReturnHot(0);
  }

  loc_rhs.resize(nb_row_dofs, false);
  loc_rhs.clear();

  auto t_free_drug_flux = getFTensor1FromMat<3>(*commonDataPtr->freeDrugFluxPtr);
  auto t_free_drug_val = getFTensor0FromVec(*commonDataPtr->freeDrugValPtr);

  const int nb_integration_pts = getGaussPts().size2();
  auto t_w = getFTensor0IntegrationWeight();
  const double vol = getMeasure();

  auto t_kinv = FTensor::Tensor2<double, 3, 3>(1./blockData.radialDiffuse, 0.0, 0.0,
                                               0.0,1./blockData.axialDiffuse, 0.0,
                                               0.0,0.0,0.0);

  FTensor::Index<'i', 3> i;
  FTensor::Index<'j', 3> j;

  for (int gg = 0; gg != nb_integration_pts; ++gg) {
    auto t_row_shape = row_data.getFTensor1N<3>(gg, 0);
    auto t_row_grad = row_data.getFTensor2DiffN<3, 2>(gg, 0);
    const double a = vol * t_w;
    for (int rr = 0; rr != nb_row_dofs; ++rr) {
      double row_div = t_row_grad(0, 0) + t_row_grad(1, 1);
      loc_rhs[rr] += (t_row_shape(i) * t_kinv(i, j) * t_free_drug_flux(j) - row_div * t_free_drug_val) * a;
      ++t_row_shape;
      ++t_row_grad;
    }
    ++t_free_drug_flux;
    ++t_free_drug_val;
    ++t_w;
  }

  CHKERR VecSetOption(getFEMethod()->ts_F, VEC_IGNORE_NEGATIVE_INDICES, PETSC_TRUE);
  CHKERR VecSetValues(getFEMethod()->ts_F, row_data, &*loc_rhs.begin(), ADD_VALUES);

  MoFEMFunctionReturn(0);
  }
}; // OpRhsHDIV

struct OpRhsL2 : OpFaceEle {
  OpRhsL2(std::string                   first_field,
          boost::shared_ptr<CommonData> common_data_ptr, 
          Range                         *ents = NULL)

      : OpFaceEle(first_field, OpFaceEle::OPROW), 
        commonDataPtr(common_data_ptr),
        eNts(ents) {}

  boost::shared_ptr<CommonData> commonDataPtr;
  Range *eNts;
  VectorDouble loc_rhs;

  MoFEMErrorCode doWork(int side, EntityType type, EntData &row_data) {
    MoFEMFunctionBegin;

    const int nb_row_dofs = row_data.getIndices().size();

    if (!nb_row_dofs)
      MoFEMFunctionReturnHot(0);

    if (eNts) {
      if (eNts->find(getFEEntityHandle()) == eNts->end())
        MoFEMFunctionReturnHot(0);
    }

    loc_rhs.resize(nb_row_dofs, false);
    loc_rhs.clear();

    auto t_free_drug_div = getFTensor0FromVec(*commonDataPtr->freeDrugDivPtr);
    auto t_free_drug_dot = getFTensor0FromVec(*commonDataPtr->freeDrugDotPtr);

    const int nb_integration_pts = getGaussPts().size2();
    auto t_w = getFTensor0IntegrationWeight();
    const double vol = getMeasure();


    for (int gg = 0; gg != nb_integration_pts; ++gg) {
      auto t_row_shape = row_data.getFTensor0N(gg, 0);
      const double a = vol * t_w;
      for (int rr = 0; rr != nb_row_dofs; ++rr) {
        loc_rhs[rr] += - t_row_shape * (t_free_drug_dot + t_free_drug_div) * a;
        ++t_row_shape;
      }
      ++t_free_drug_div;
      ++t_free_drug_dot;
      ++t_w;
    }

    CHKERR VecSetOption(getFEMethod()->ts_F, VEC_IGNORE_NEGATIVE_INDICES,
                        PETSC_TRUE);
    CHKERR VecSetValues(getFEMethod()->ts_F, row_data, &*loc_rhs.begin(),
                        ADD_VALUES);

    MoFEMFunctionReturn(0);
  }
}; // OpRhsL2

struct OpRhsL2Field2 : OpFaceEle {
  OpRhsL2Field2(std::string                   first_field,
                boost::shared_ptr<CommonData> common_data_ptr, 
                Range                         *ents = NULL)

      : OpFaceEle(first_field, OpFaceEle::OPROW),
        commonDataPtr(common_data_ptr), eNts(ents) {}

  boost::shared_ptr<CommonData> commonDataPtr;
  Range *eNts;
  VectorDouble loc_rhs;

  MoFEMErrorCode doWork(int side, EntityType type, EntData &row_data) {
    MoFEMFunctionBegin;

    const int nb_row_dofs = row_data.getIndices().size();

    if (!nb_row_dofs)
      MoFEMFunctionReturnHot(0);

    if (eNts) {
      if (eNts->find(getFEEntityHandle()) == eNts->end())
        MoFEMFunctionReturnHot(0);
    }

    loc_rhs.resize(nb_row_dofs, false);
    loc_rhs.clear();

    auto t_bound_drug_dot = getFTensor0FromVec(*commonDataPtr->boundDrugDotPtr);

    const int nb_integration_pts = getGaussPts().size2();
    auto t_w = getFTensor0IntegrationWeight();
    const double vol = getMeasure();

    for (int gg = 0; gg != nb_integration_pts; ++gg) {
      auto t_row_shape = row_data.getFTensor0N(gg, 0);
      const double a = vol * t_w;
      for (int rr = 0; rr != nb_row_dofs; ++rr) {
        loc_rhs[rr] += -t_row_shape * t_bound_drug_dot * a;
        ++t_row_shape;
      }
      ++t_bound_drug_dot;
      ++t_w;
    }

    CHKERR VecSetOption(getFEMethod()->ts_F, VEC_IGNORE_NEGATIVE_INDICES,
                        PETSC_TRUE);
    CHKERR VecSetValues(getFEMethod()->ts_F, row_data, &*loc_rhs.begin(),
                        ADD_VALUES);

    MoFEMFunctionReturn(0);
  }
}; // OpRhsL2

struct OpLhsHDIV_L2 : OpFaceEle {
  OpLhsHDIV_L2(std::string                   hdiv_field, 
               std::string                   l2_field,
               boost::shared_ptr<CommonData> common_data_ptr,
               Range                         *ents = NULL)

      : OpFaceEle(hdiv_field, l2_field, OpFaceEle::OPROWCOL),
        commonDataPtr(common_data_ptr),
        eNts(ents) 
        {
          sYmm = false;
        }


  boost::shared_ptr<CommonData> commonDataPtr;
  Range                         *eNts;
  MatrixDouble                  loc_lhs;

  MoFEMErrorCode doWork(int        row_side, 
                        int        col_side, 
                        EntityType row_type,
                        EntityType col_type, 
                        EntData    &row_data,
                        EntData    &col_data) {
    MoFEMFunctionBegin;
    const int nb_row_dofs = row_data.getIndices().size();
    const int nb_col_dofs = col_data.getIndices().size();

    if (!nb_row_dofs || !nb_col_dofs)
      MoFEMFunctionReturnHot(0);

    if (eNts) {
      if (eNts->find(getFEEntityHandle()) == eNts->end())
        MoFEMFunctionReturnHot(0);
    }

      loc_lhs.resize(nb_row_dofs, nb_col_dofs, false);
      loc_lhs.clear();

      const int nb_integration_pts = getGaussPts().size2();
      auto t_w = getFTensor0IntegrationWeight();

      const double vol = getMeasure();

      for (int gg = 0; gg != nb_integration_pts; ++gg) {
        auto t_row_grad = row_data.getFTensor2DiffN<3, 2>(gg, 0);
        
        const double a = vol * t_w;
        for (int rr = 0; rr != nb_row_dofs; ++rr) {
          auto t_col_shape = col_data.getFTensor0N(gg, 0);
          double row_div = t_row_grad(0, 0) + t_row_grad(1, 1);
          for (int cc = 0; cc != nb_col_dofs; ++cc) {
            loc_lhs(rr, cc) += -(row_div * t_col_shape) * a;
            ++t_col_shape;
          }
          ++t_row_grad;
        }
        ++t_w;
      }
      CHKERR MatSetValues(getFEMethod()->ts_B, row_data, col_data, &loc_lhs(0, 0), ADD_VALUES);

    MoFEMFunctionReturn(0);
  }
}; // OpLhsHDIV_L2

struct OpLhsL2_HDIV : OpFaceEle {
  OpLhsL2_HDIV(std::string l2_field, std::string hdiv_field,
               boost::shared_ptr<CommonData> common_data_ptr,
               Range *ents = NULL)

      : OpFaceEle(l2_field, hdiv_field, OpFaceEle::OPROWCOL),
        commonDataPtr(common_data_ptr), 
        eNts(ents) 
        {
          sYmm = false;
        }

  boost::shared_ptr<CommonData> commonDataPtr;
  Range *eNts;
  MatrixDouble loc_lhs;

  MoFEMErrorCode doWork(int row_side, int col_side, EntityType row_type,
                        EntityType col_type, EntData &row_data,
                        EntData &col_data) {
    MoFEMFunctionBegin;
    const int nb_row_dofs = row_data.getIndices().size();
    const int nb_col_dofs = col_data.getIndices().size();

    if (!nb_row_dofs || !nb_col_dofs)
      MoFEMFunctionReturnHot(0);

    if (eNts) {
      if (eNts->find(getFEEntityHandle()) == eNts->end())
        MoFEMFunctionReturnHot(0);
    }

    loc_lhs.resize(nb_row_dofs, nb_col_dofs, false);
    loc_lhs.clear();

    const int nb_integration_pts = getGaussPts().size2();
    auto t_w = getFTensor0IntegrationWeight();

    const double vol = getMeasure();

    for (int gg = 0; gg != nb_integration_pts; ++gg) {  
      auto t_row_shape = row_data.getFTensor0N(gg, 0);
      
      const double a = vol * t_w;
      for (int rr = 0; rr != nb_row_dofs; ++rr) {
        auto t_col_grad = col_data.getFTensor2DiffN<3, 2>(gg, 0);
        for (int cc = 0; cc != nb_col_dofs; ++cc) {
          double col_div = t_col_grad(0, 0) + t_col_grad(1, 1);
          loc_lhs(rr, cc) += -(t_row_shape * col_div) * a;
          ++t_col_grad;
        }
        ++t_row_shape;
      }
      ++t_w;
    }
    CHKERR MatSetValues(getFEMethod()->ts_B, row_data, col_data, &loc_lhs(0, 0),
                        ADD_VALUES);

    MoFEMFunctionReturn(0);
  }
}; // OpLhsL2_HDIV

struct OpLhsL2_L2 : OpFaceEle {
  OpLhsL2_L2(std::string                   l2_row_field, 
             std::string                   l2_col_field,
             boost::shared_ptr<CommonData> common_data_ptr,
             Range                         *ents = NULL)

      : OpFaceEle(l2_row_field, l2_col_field, OpFaceEle::OPROWCOL),
        commonDataPtr(common_data_ptr), 
        eNts(ents) 
        {
          sYmm = false;
        }

  boost::shared_ptr<CommonData> commonDataPtr;
  Range *eNts;
  MatrixDouble loc_lhs;

  MoFEMErrorCode doWork(int        row_side, 
                        int        col_side, 
                        EntityType row_type,
                        EntityType col_type, 
                        EntData    &row_data,
                        EntData    &col_data) {
    MoFEMFunctionBegin;
    const int nb_row_dofs = row_data.getIndices().size();
    const int nb_col_dofs = col_data.getIndices().size();

    if (!nb_row_dofs || !nb_col_dofs)
      MoFEMFunctionReturnHot(0);

    if (eNts) {
      if (eNts->find(getFEEntityHandle()) == eNts->end())
        MoFEMFunctionReturnHot(0);
    }

    loc_lhs.resize(nb_row_dofs, nb_col_dofs, false);
    loc_lhs.clear();

    const int nb_integration_pts = getGaussPts().size2();
    auto t_w = getFTensor0IntegrationWeight();

    const double ts_shift = getFEMethod()->ts_a;

    const double vol = getMeasure();


    for (int gg = 0; gg != nb_integration_pts; ++gg) {
      auto t_row_shape = row_data.getFTensor0N(gg, 0);
      const double a = vol * t_w;
      for (int rr = 0; rr != nb_row_dofs; ++rr) {
        auto t_col_shape = col_data.getFTensor0N(gg, 0);
        for (int cc = 0; cc != nb_col_dofs; ++cc) {
          loc_lhs(rr, cc) += -ts_shift * t_row_shape * t_col_shape * a;
          ++t_col_shape;
        }
        ++t_row_shape;
      }
      ++t_w;
    }
    CHKERR MatSetValues(getFEMethod()->ts_B, row_data, col_data, &loc_lhs(0, 0),
                        ADD_VALUES);

    MoFEMFunctionReturn(0);
  }
}; // OpLhsHDIV_HDIV

struct OpLhsHDIV_HDIV : OpFaceEle {
  OpLhsHDIV_HDIV(std::string                   hdiv_row_field, 
                 std::string                   hdiv_col_field,
                 boost::shared_ptr<CommonData> common_data_ptr,
                 BlockData                     &block_data,
                 Range                         *ents = NULL)

      : OpFaceEle(hdiv_row_field, hdiv_col_field, OpFaceEle::OPROWCOL),
        commonDataPtr(common_data_ptr), 
        blockData(block_data),
        eNts(ents) 
        {
          sYmm = false;
        }

  boost::shared_ptr<CommonData> commonDataPtr;
  Range *eNts;
  MatrixDouble loc_lhs;
  BlockData  &blockData;

  MoFEMErrorCode doWork(int        row_side, 
                        int        col_side, 
                        EntityType row_type,
                        EntityType col_type, 
                        EntData    &row_data,
                        EntData    &col_data) {
    MoFEMFunctionBegin;
    const int nb_row_dofs = row_data.getIndices().size();
    const int nb_col_dofs = col_data.getIndices().size();

    if (!nb_row_dofs || !nb_col_dofs)
      MoFEMFunctionReturnHot(0);

    if (eNts) {
      if (eNts->find(getFEEntityHandle()) == eNts->end())
        MoFEMFunctionReturnHot(0);
    }

    loc_lhs.resize(nb_row_dofs, nb_col_dofs, false);
    loc_lhs.clear();

    const int nb_integration_pts = getGaussPts().size2();
    auto t_w = getFTensor0IntegrationWeight();

    const double vol = getMeasure();
    
    auto t_kinv = FTensor::Tensor2<double, 3, 3>(1./blockData.radialDiffuse, 0.0, 0.0,
                                                  0.0, 1./blockData.axialDiffuse, 0.0,
                                                  0.0, 0.0, 0.0);

    FTensor::Index<'i', 3> i;
    FTensor::Index<'j', 3> j;

    for (int gg = 0; gg != nb_integration_pts; ++gg) {
      auto t_row_shape = row_data.getFTensor1N<3>(gg, 0);
      const double a = vol * t_w;
      for (int rr = 0; rr != nb_row_dofs; ++rr) {
        auto t_col_shape = col_data.getFTensor1N<3>(gg, 0);
        for (int cc = 0; cc != nb_col_dofs; ++cc) {
          loc_lhs(rr, cc) += (t_row_shape(i) * t_kinv(i, j) * t_col_shape(j)) * a;
          ++t_col_shape;
        }
        ++t_row_shape;
      }
      ++t_w;
    }
    CHKERR MatSetValues(getFEMethod()->ts_B, row_data, col_data, &loc_lhs(0, 0),
                        ADD_VALUES);

    MoFEMFunctionReturn(0);
  }
}; // OpLhsL2_L2


struct OpInitialMass : public OpFaceEle {
  OpInitialMass(const std::string  first_field,
                double             &init_val, 
                Range              *ents)
      : OpFaceEle(first_field, OpFaceEle::OPROW), 
        eNts(ents),
        initVal(init_val){}

  MatrixDouble loc_lhs;
  VectorDouble loc_rhs;
  double &initVal;
  Range *eNts;


  MoFEMErrorCode doWork(int side, EntityType type, EntData &data) {
    MoFEMFunctionBegin;
    const int nb_dofs = data.getIndices().size();

    if (!nb_dofs)
      MoFEMFunctionReturnHot(0);

    if (eNts) {
      if (eNts->find(getFEEntityHandle()) == eNts->end())
        MoFEMFunctionReturnHot(0);
    }

    loc_rhs.resize(nb_dofs, false);
    loc_rhs.clear();

    loc_lhs.resize(nb_dofs, nb_dofs, false);
    loc_lhs.clear();

    int nb_gauss_pts = getGaussPts().size2();

    auto t_w = getFTensor0IntegrationWeight();
    const double vol = getMeasure();

    for (int gg = 0; gg < nb_gauss_pts; gg++) {
      auto t_row_shape = data.getFTensor0N(gg, 0);
      const double a = t_w * vol;
      for (int rr = 0; rr != nb_dofs; rr++) {
        auto t_col_shape = data.getFTensor0N(gg, 0);
        loc_rhs[rr] += a * initVal * t_row_shape;
        for (int cc = 0; cc != nb_dofs; cc++) {
          loc_lhs(rr, cc) += a * t_row_shape * t_col_shape;
          ++t_col_shape;
        }
        ++t_row_shape;
      }
      ++t_w;
    }

    cholesky_decompose(loc_lhs);
    cholesky_solve(loc_lhs, loc_rhs, ublas::lower());

    for (auto &dof : data.getFieldDofs()) {
      dof->getFieldData() = loc_rhs[dof->getEntDofIdx()];
    }

    MoFEMFunctionReturn(0);
  }
}; // OpInitialMass

struct Monitor : public FEMethod {  
  Monitor(Simple                                            *simple_interface,
          boost::shared_ptr<FaceEle>                        integrate_field,
          boost::shared_ptr<PostProc>                       &post_proc,
          MoFEM::Interface                                  &m_field,
          DataValues                                        *data_values_ptr = NULL)
      : postProc(post_proc),
        simpleInterface(simple_interface),
        mField(m_field),
        dataValuePtr(data_values_ptr),
        IntFieldPipeLine(integrate_field){};

  DataValues *dataValuePtr;
  MoFEM::Interface &mField;
  boost::shared_ptr<FaceEle> IntFieldPipeLine;
  MoFEMErrorCode preProcess() {
    MoFEMFunctionBegin;
    
    MoFEMFunctionReturn(0);
  }
  MoFEMErrorCode operator()() { 
    return 0; 
  }

  MoFEMErrorCode postProcess() {
    MoFEMFunctionBegin;
    CHKERR PetscOptionsGetInt(PETSC_NULL, "", "-save_every_nth_step",
                              &save_every_nth_step, PETSC_NULL);
    if (ts_step % save_every_nth_step == 0) {

      CHKERR DMoFEMLoopFiniteElements(simpleInterface->getDM(), simpleInterface->getDomainFEName(), postProc);
      CHKERR postProc->writeFile("output_mxd_" + boost::lexical_cast<std::string>(ts_step) + ".h5m");
      
      dataValuePtr->CoatingVolume = 0;
      dataValuePtr->WallVolume = 0;
      dataValuePtr->FreeDrugCoating = 0;
      dataValuePtr->FreeDrugWall = 0;
      dataValuePtr->BoundDrugWall = 0;
      dataValuePtr->TotalDrugContent = 0;
      dataValuePtr->SquareDrugContent = 0;

      CHKERR DMoFEMLoopFiniteElements(simpleInterface->getDM(), simpleInterface->getDomainFEName(), IntFieldPipeLine);

      auto get_global_value = [&](double &out_val, double *out_val_ptr) {
        out_val = 0;
        Vec outValTotalPerProc;
        CHKERR VecCreateMPI(mField.get_comm(), 1, PETSC_DECIDE, &outValTotalPerProc);
        CHKERR VecSetValue(outValTotalPerProc, mField.get_comm_rank(), *out_val_ptr, INSERT_VALUES);
        CHKERR VecAssemblyBegin(outValTotalPerProc);
        CHKERR VecAssemblyEnd(outValTotalPerProc);
        CHKERR VecSum(outValTotalPerProc, &out_val);
      };

      // CHKERR VecCreateMPI(mField.get_comm(), 1, PETSC_DECIDE,
      //                     &outValTotalPerProc);
      // auto get_global_error = [&]() {
      //   MoFEMFunctionBegin;
      //   CHKERR VecSetValue(outValTotalPerProc, mField.get_comm_rank(),
      //                      *outValPtr, INSERT_VALUES);
      //   MoFEMFunctionReturn(0);
      // };
      // CHKERR get_global_error();
      // CHKERR VecAssemblyBegin(outValTotalPerProc);
      // CHKERR VecAssemblyEnd(outValTotalPerProc);
      // CHKERR VecSum(outValTotalPerProc, &outValTotal);

      double coating_vol = 0.0;
      double wall_vol = 0.0;
      double free_drug_coating = 0.0;
      double free_drug_wall = 0.0;
      double bound_drug_wall = 0.0;
      double total_drug = 0.0;
      double square_drug = 0.0;

      get_global_value(coating_vol, &dataValuePtr->CoatingVolume);
      get_global_value(wall_vol, &dataValuePtr->WallVolume);
      get_global_value(free_drug_wall, &dataValuePtr->FreeDrugWall);
      get_global_value(free_drug_coating, &dataValuePtr->FreeDrugCoating);
      get_global_value(bound_drug_wall, &dataValuePtr->BoundDrugWall);
      get_global_value(total_drug, &dataValuePtr->TotalDrugContent);
      get_global_value(square_drug, &dataValuePtr->SquareDrugContent);

      CHKERR PetscFPrintf(PETSC_COMM_WORLD, data_fp, "%3.5f,", ts_t);
      CHKERR PetscFPrintf(PETSC_COMM_WORLD, data_fp, "%3.16f,", coating_vol);
      CHKERR PetscFPrintf(PETSC_COMM_WORLD, data_fp, "%3.16f,", wall_vol);
      CHKERR PetscFPrintf(PETSC_COMM_WORLD, data_fp, "%3.16f,", free_drug_coating);
      CHKERR PetscFPrintf(PETSC_COMM_WORLD, data_fp, "%3.16f,", free_drug_wall);
      CHKERR PetscFPrintf(PETSC_COMM_WORLD, data_fp, "%3.16f,", bound_drug_wall);
      CHKERR PetscFPrintf(PETSC_COMM_WORLD, data_fp, "%3.16f,", total_drug);
      CHKERR PetscFPrintf(PETSC_COMM_WORLD, data_fp, "%3.16f", square_drug);
      CHKERR PetscFPrintf(PETSC_COMM_WORLD, data_fp, "\n");

      }
    MoFEMFunctionReturn(0);
  }

private:
  boost::shared_ptr<PostProc> postProc;
  Simple *simpleInterface;
}; // Monitor

}; // end Operators

#endif