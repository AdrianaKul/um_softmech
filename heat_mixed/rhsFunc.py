#binding rate [m^3 mol^-1 s^-1]
k_on = 0.17

#unbinding rate [s^-1]
k_off = 5.27e-4

#site binding density [mol m^-3]
b_max = 0.127

#coating diffusivity [m^2 s^-1]
D_c = 1e-13

#media radial diffusivity [m^2 s^-1]
Dm_r = 2e-12

#media axial diffusivity [m^2 s^-1]
Dm_z = 5e-11

#molar weight of blood [g mol^-1]
MW_b = 853.906


#rhsFunc.py

def rhs_func(c_m, b_m):
  return k_on * c_m * (b_max - b_m) - k_off * b_m

def radialDiffuse(domain):
  if(domain == "DOMAIN_1"):
    return D_c
  if(domain  == "DOMAIN_2"):
    return Dm_r

def axialDiffuse(domain):
  if(domain == "DOMAIN_1"):
    return D_c
  if(domain  == "DOMAIN_2"):
    return Dm_z


