#ifndef __CHEMOMECHOPERTATORS_HPP__
#define __CHEMOMECHOPERTATORS_HPP__

#include <stdlib.h>
#include <BasicFiniteElements.hpp>

namespace ChemoMechOps {

using VolEle = MoFEM::VolumeElementForcesAndSourcesCore;
using OpVolEle = VolEle::UserDataOperator;

using FaceEle = MoFEM::FaceElementForcesAndSourcesCore;
using OpFaceEle = FaceEle::UserDataOperator;

using EdgeEle = MoFEM::FaceElementForcesAndSourcesCore;
using OpEdgeEle = EdgeEle::UserDataOperator;

using EntData = DataForcesAndSourcesCore::EntData;

typedef boost::function<FTensor::Tensor2<double, 3, 3>
        (const double, const double)> VectorFunc;

typedef boost::function<FTensor::Tensor1<double, 3>(const double, const double,
                                                    const double, const double)>
    NeumannFunc;

typedef boost::function<FTensor::Tensor2<double, 3, 3>(
    FTensor::Tensor2_symmetric<double,3>, const double, const double,
    const double)>
    DiffuseFunc;

typedef boost::function<double(FTensor::Tensor2<double *, 3, 3>,
                                 const double)> DispKineticFunc;
// Operators
const double c0 = 0.5;
const double c1 = 0.025;
const double c2 = - 0.015;
const double d2 = - 10;
const double d3 = 0.25;

FTensor::Index<'i', 3> i;
FTensor::Index<'j', 3> j;
FTensor::Index<'k', 3> k;
FTensor::Index<'l', 3> l;

auto flg_indicies = []( EntData                              &data,
                        boost::shared_ptr<std::vector<bool>> &boundary_marker, 
                        const int                            &size_dofs,
                        const int                            &comp,
                        const int                            &u_rank){
  MoFEMFunctionBegin;
  if(boundary_marker){
    // cout << "comp : " << comp << " Marker : ";
    for (int r = 0; r != size_dofs / u_rank; r++){
      // cout << (*boundary_marker)[data.getIndices()[u_rank * r + comp]]
      //      << ", ";
      if ((*boundary_marker)[data.getLocalIndices()[u_rank * r + comp]]) {
          data.getIndices()[u_rank * r + comp] = -1;

          
      }
    }
    // cout << endl;
  }
  
  MoFEMFunctionReturn(0);
};


struct PreviousData {
  MatrixDouble dispGrad;
  MatrixDouble dispValue;

  VectorDouble massValue;
  VectorDouble massDot;
  MatrixDouble fluxValue;
  VectorDouble fluxDiv;


  PreviousData() {

  }
};

struct BlockData {
  int block_id;
  double yOung;
  double pOisson;
  double mU;
  double lAmbda;

  double B0;

  FTensor::Ddg<double, 3, 3> tD;



  Range block_ents;




  MoFEMErrorCode getMatParam() {
    MoFEMFunctionBegin;
    B0 = 1e0;
    lAmbda = (yOung * pOisson) / ((1. + pOisson) * (1. - 2. * pOisson));
    mU = yOung / (2. * (1. + pOisson));

    const double coefficient = yOung / ((1 + pOisson) * (1 - 2 * pOisson));

    tD(i, j, k, l) = 0.0;

// Compressible

    tD(0, 0, 0, 0) = 2. * mU + 1. * lAmbda;
    tD(0, 0, 1, 1) = 0. * mU + 1. * lAmbda;
    tD(0, 0, 2, 2) = 0. * mU + 1. * lAmbda;
    tD(0, 1, 0, 1) = 1. * mU + 0. * lAmbda;
    tD(0, 1, 1, 0) = 1. * mU + 0. * lAmbda;
    tD(0, 2, 0, 2) = 1. * mU + 0. * lAmbda;
    tD(0, 2, 2, 0) = 1. * mU + 0. * lAmbda;
    tD(1, 0, 0, 1) = 1. * mU + 0. * lAmbda;
    tD(1, 0, 1, 0) = 1. * mU + 0. * lAmbda;
    tD(1, 1, 0, 0) = 0. * mU + 1. * lAmbda;
    tD(1, 1, 1, 1) = 2. * mU + 1. * lAmbda;
    tD(1, 1, 2, 2) = 0. * mU + 1. * lAmbda;
    tD(1, 2, 1, 2) = 1. * mU + 0. * lAmbda;
    tD(1, 2, 2, 1) = 1. * mU + 0. * lAmbda;
    tD(2, 0, 0, 2) = 1. * mU + 0. * lAmbda;
    tD(2, 0, 2, 0) = 1. * mU + 0. * lAmbda;
    tD(2, 1, 1, 2) = 1. * mU + 0. * lAmbda;
    tD(2, 1, 2, 1) = 1. * mU + 0. * lAmbda;
    tD(2, 2, 0, 0) = 0. * mU + 1. * lAmbda;
    tD(2, 2, 1, 1) = 0. * mU + 1. * lAmbda;
    tD(2, 2, 2, 2) = 2. * mU + 1. * lAmbda;

    // // Incompressible
    // tD(0, 0, 0, 0) = 2. * mU;
    // tD(0, 1, 0, 1) = mU;
    // tD(0, 1, 1, 0) = mU;
    // tD(0, 2, 0, 2) = mU;
    // tD(0, 2, 2, 0) = mU;
    // tD(1, 0, 0, 1) = mU;
    // tD(1, 0, 1, 0) = mU;
    // tD(1, 1, 1, 1) = 2. * mU;
    // tD(1, 2, 1, 2) = mU;
    // tD(1, 2, 2, 1) = mU;
    // tD(2, 0, 0, 2) = mU;
    // tD(2, 0, 2, 0) = mU;
    // tD(2, 1, 1, 2) = mU;
    // tD(2, 1, 2, 1) = mU;
    // tD(2, 2, 2, 2) = 2. * mU;


    // Hook element
    // tD(0, 0, 0, 0) = 1 - pOisson;
    // tD(1, 1, 1, 1) = 1 - pOisson;
    // tD(2, 2, 2, 2) = 1 - pOisson;
    // tD(0, 1, 0, 1) = 0.5 * (1 - 2 * pOisson);
    // tD(0, 2, 0, 2) = 0.5 * (1 - 2 * pOisson);
    // tD(1, 2, 1, 2) = 0.5 * (1 - 2 * pOisson);
    // tD(0, 0, 1, 1) = pOisson;
    // tD(1, 1, 0, 0) = pOisson;
    // tD(0, 0, 2, 2) = pOisson;
    // tD(2, 2, 0, 0) = pOisson;
    // tD(1, 1, 2, 2) = pOisson;
    // tD(2, 2, 1, 1) = pOisson;

    // tD(i, j, k, l) *= coefficient;


        MoFEMFunctionReturn(0);
  }

  BlockData() 
  : yOung(21000)
  , pOisson(0.3) {}
};


struct OpLHS_UU : OpVolEle {
  OpLHS_UU(std::string                          disp_name,
           boost::shared_ptr<PreviousData>      &common_data,
           std::map<int, BlockData>             &block_map)
      : OpVolEle(disp_name, disp_name, OpVolEle::OPROWCOL)
      , setOfBlock(block_map)
      , commonData(common_data)
      {
        sYmm = false;
      }
  FTensor::Tensor2<double, 3, 3> diffDiff;

  MoFEMErrorCode doWork(int        row_side, 
                        int        col_side, 
                        EntityType row_type,
                        EntityType col_type, 
                        EntData    &row_data,
                        EntData    &col_data) {
    MoFEMFunctionBegin;
    const int nb_row_dofs = row_data.getIndices().size();
    const int nb_col_dofs = col_data.getIndices().size();

    if (nb_row_dofs && nb_col_dofs) {

      auto find_block_data = [&]() {
        EntityHandle fe_ent = getFEEntityHandle();
        BlockData *block_raw_ptr = nullptr;
        for (auto &m : setOfBlock) {
          if (m.second.block_ents.find(fe_ent) != m.second.block_ents.end()) {
            block_raw_ptr = &m.second;
            break;
          }
        }
        return block_raw_ptr;
      };

      auto block_data_ptr = find_block_data();
      if (!block_data_ptr)
        SETERRQ(PETSC_COMM_SELF, MOFEM_DATA_INCONSISTENCY, "Block not found");
      auto &block_data = *block_data_ptr;

      block_data.getMatParam();


      auto get_tensor2 = [](MatrixDouble &m, const int r, const int c) {
        return FTensor::Tensor2<double *, 3, 3>(
            &m(r + 0, c + 0), &m(r + 0, c + 1), &m(r + 0, c + 2),
            &m(r + 1, c + 0), &m(r + 1, c + 1), &m(r + 1, c + 2),
            &m(r + 2, c + 0), &m(r + 2, c + 1), &m(r + 2, c + 2));
      };

      locMat.resize(nb_row_dofs, nb_col_dofs, false);
      locMat.clear();

      const int nb_integration_pts = getGaussPts().size2();
      const int row_nb_base_fun = row_data.getN().size2();

      auto t_row_diff = row_data.getFTensor1DiffN<3>();

      auto t_w = getFTensor0IntegrationWeight();
      const double vol = getMeasure();
      for (int gg = 0; gg != nb_integration_pts; ++gg) {
        const double a = vol * t_w;

        int rr = 0;
        for (; rr != nb_row_dofs / 3; ++rr) {


          auto t_col_diff = col_data.getFTensor1DiffN<3>(gg, 0);

          for (int cc = 0; cc != nb_col_dofs / 3; ++cc) {
            auto t_subLocMat = get_tensor2(locMat, 3 * rr, 3 * cc);
            diffDiff(i, j) = t_row_diff(i) * t_col_diff(j);
            t_subLocMat(k, l) += a * diffDiff(i, j) * block_data.tD(k, i, l, j);
            
            ++t_col_diff;
          }
          ++t_row_diff;
        }
        for (; rr != row_nb_base_fun; ++rr)
          ++t_row_diff;
        ++t_w;
      }
      // cout << "Indices UU : " <<row_data.getIndices() << endl;

      CHKERR MatSetValues(getFEMethod()->ts_B, row_data, col_data,
                                      &locMat(0, 0), ADD_VALUES);

      // if (row_side != col_side || row_type != col_type) {
        

      //   locTransMat.resize(nb_col_dofs, nb_row_dofs, false);
      //   noalias(locTransMat) = trans(locMat);
      //   CHKERR MatSetValues(getFEMethod()->ts_B, col_data, row_data,
      //                       &locTransMat(0, 0), ADD_VALUES);

        
      // }
  
    }
    MoFEMFunctionReturn(0);
  }

private:
  boost::shared_ptr<PreviousData> commonData;
  MatrixDouble locMat, locTransMat;
  std::map<int, BlockData> setOfBlock;
};

struct OpRHS_U : OpVolEle {
  OpRHS_U(std::string                          disp_name, 
          boost::shared_ptr<PreviousData>      &common_data,
          std::map<int, BlockData>             &block_map, 
          VectorFunc                           body_source)
      : OpVolEle(disp_name, OpVolEle::OPROW)
      , setOfBlock(block_map)
      , commonData(common_data)
      , bodySource(body_source)
      {}

  MoFEMErrorCode doWork(int row_side, EntityType row_type, EntData &row_data) {
    MoFEMFunctionBegin;
    const int nb_row_dofs = row_data.getIndices().size();

    if (nb_row_dofs) {



      auto find_block_data = [&]() {
        EntityHandle fe_ent = getFEEntityHandle();
        BlockData *block_raw_ptr = nullptr;
        for (auto &m : setOfBlock) {
          if (m.second.block_ents.find(fe_ent) != m.second.block_ents.end()) {
            block_raw_ptr = &m.second;
            break;
          }
        }
        return block_raw_ptr;
      };

      auto block_data_ptr = find_block_data();
      if (!block_data_ptr)
        SETERRQ(PETSC_COMM_SELF, MOFEM_DATA_INCONSISTENCY, "Block not found");
      auto &block_data = *block_data_ptr;

      auto t_dispGrad = getFTensor2FromMat<3, 3>(commonData->dispGrad);
      auto t_massValue = getFTensor0FromVec(commonData->massValue);

      // calculate the stiffness matrix and set the material parameters
      block_data.getMatParam();  

      auto extract_vec = [](VectorDouble &m, const int r) {
        return FTensor::Tensor1<double *, 3>(&m(r + 0), 
                                             &m(r + 1), 
                                             &m(r + 2));
      };

      locRes.resize(nb_row_dofs, false);
      locRes.clear();

      const int nb_integration_pts = getGaussPts().size2();
      const int row_nb_base_fun = row_data.getN().size2();

      auto t_row_val = row_data.getFTensor0N();
      auto t_row_diff = row_data.getFTensor1DiffN<3>();

      auto t_w = getFTensor0IntegrationWeight();
      const double vol = getMeasure();

      auto t_coords = getFTensor1CoordsAtGaussPts();

      const double time = getFEMethod()->ts_t;

      // FTensor::Tensor1<double, 3> t_concentrationForce(0.0, 0.0, 0.0);

      for (int gg = 0; gg != nb_integration_pts; ++gg) {
        const double a = vol * t_w;
  
        // body force
         auto t_concentrationForce = bodySource(t_massValue, d2);


        int rr = 0;
        for (; rr != nb_row_dofs / 3; ++rr) {

          auto t_subLocRes = extract_vec(locRes, 3 * rr);

          t_subLocRes(i) +=
              a *
              (t_row_diff(j) * (block_data.tD(i, j, k, l) * t_dispGrad(k, l)) -
               t_row_diff(j)* t_concentrationForce(j, i));

          ++t_row_val;
          ++t_row_diff;
        }
        for (; rr != row_nb_base_fun; ++rr){
          ++t_row_val;
          ++t_row_diff;
        }
        ++t_massValue;
        ++t_dispGrad;
        ++t_coords;
        ++t_w;
      }

      // cout << "Indices RHS_UU Global : " << row_data.getIndices() << endl;
      // cout << "Indices RHS_UU Local  : " << row_data.getLocalIndices() << endl;
      CHKERR VecSetOption(getFEMethod()->ts_F, VEC_IGNORE_NEGATIVE_INDICES,
                          PETSC_TRUE);
      CHKERR VecSetValues(getFEMethod()->ts_F, row_data, &*locRes.begin(),
                          ADD_VALUES);

    }
    MoFEMFunctionReturn(0);
  }

private:
  boost::shared_ptr<PreviousData> commonData;
  VectorDouble locRes;
  std::map<int, BlockData> setOfBlock;
  VectorFunc bodySource;
};

struct OpRHS_Neumann : OpFaceEle {
  OpRHS_Neumann(std::string                          disp_name,
                int                                  comp,
                NeumannFunc                           boundary_flux,
                Range                                &neumann_boundary_ents)
      : OpFaceEle(disp_name, OpFaceEle::OPROW)
      , NeumannBoundaryEnts(neumann_boundary_ents)
      , cOmp(comp)
      , boundaryFlux(boundary_flux) {}

  MoFEMErrorCode doWork(int row_side, EntityType row_type, EntData &row_data) {
    MoFEMFunctionBegin;
    const int nb_row_dofs = row_data.getIndices().size();

    if (nb_row_dofs) {

      EntityHandle ent = getFEEntityHandle();
      // getFEEntityHandle();

      if (NeumannBoundaryEnts.find(ent) == NeumannBoundaryEnts.end()) {
        MoFEMFunctionReturnHot(0);
      }

      auto extract_vec = [](VectorDouble &m, const int r) {
        return FTensor::Tensor1<double *, 3>(&m(r + 0), &m(r + 1), &m(r + 2));
      };

      locRes.resize(nb_row_dofs, false);
      locRes.clear();

      const int nb_integration_pts = getGaussPts().size2();
      const int row_nb_base_fun = row_data.getN().size2();

      auto t_row_val = row_data.getFTensor0N();

      auto t_w = getFTensor0IntegrationWeight();
      const double vol = getMeasure();

      auto t_coords = getFTensor1CoordsAtGaussPts();
      const double time = getFEMethod()->ts_t;

      for (int gg = 0; gg != nb_integration_pts; ++gg) {
        const double a = vol * t_w;
        
        //boundary traction
        auto t_Fflux = boundaryFlux(t_coords(0), t_coords(1), t_coords(2), time);
        
        int rr = 0;
        for (; rr != nb_row_dofs / 3; ++rr) {

          auto t_subLocRes = extract_vec(locRes, 3 * rr);

          t_subLocRes(cOmp) -=  (a * t_row_val) * t_Fflux(cOmp);

          ++t_row_val;
        }
        for (; rr != row_nb_base_fun; ++rr) {
          ++t_row_val;
        }
        ++t_coords;
        ++t_w;
      }


      CHKERR VecSetOption(getFEMethod()->ts_F, VEC_IGNORE_NEGATIVE_INDICES,
                          PETSC_TRUE);
      CHKERR VecSetValues(getFEMethod()->ts_F, row_data, &*locRes.begin(),
                          ADD_VALUES);

    }
    MoFEMFunctionReturn(0);
  }

private:
  VectorDouble                         locRes;
  int                                  cOmp;
  Range                                NeumannBoundaryEnts;
  NeumannFunc                          boundaryFlux;
};
struct OpBoundaryMassMatrix : OpFaceEle {
  OpBoundaryMassMatrix(std::string                          disp_name, 
                          const int                            u_rank,
                          int                                  comp,
                          const Range                          &essential_ents)
      : OpFaceEle(disp_name, disp_name, OpFaceEle::OPROWCOL)
      , cOmp(comp) 
      , uRank(u_rank)
      , essentialEnts(essential_ents)
      {
        sYmm = true;
      }

  MoFEMErrorCode doWork(int        row_side, 
                        int        col_side, 
                        EntityType row_type,
                        EntityType col_type, 
                        EntData    &row_data,
                        EntData    &col_data) {
    MoFEMFunctionBegin;
    const int nb_row_dofs = row_data.getIndices().size();
    const int nb_col_dofs = col_data.getIndices().size();

    if (nb_row_dofs && nb_col_dofs) {

      EntityHandle ent = getFEEntityHandle();
      // getFEEntityHandle();

      if (essentialEnts.find(ent) == essentialEnts.end()) {
         MoFEMFunctionReturnHot(0);
      }


      locMat.resize(nb_row_dofs, nb_col_dofs, false);
      locMat.clear();

      const int nb_integration_pts = getGaussPts().size2();

      auto t_row_val = row_data.getFTensor0N();

      auto t_w = getFTensor0IntegrationWeight();
      const double vol = getMeasure();
  
      

      for (int gg = 0; gg != nb_integration_pts; ++gg) {
        const double a = vol * t_w;

        for (int rr = 0; rr != nb_row_dofs / uRank; ++rr) {

          auto t_col_val = col_data.getFTensor0N(gg, 0);

          for (int cc = 0; cc != nb_col_dofs / uRank; ++cc) {
            // auto t_subLocMat = get_tensor2(locMat, 3 * rr, 3 * cc);
            // t_subLocMat(cOmp, cOmp) += (a * t_row_val * t_col_val);
            locMat(uRank * rr + cOmp, uRank * cc + cOmp) += (a * t_row_val * t_col_val);

            ++t_col_val;
          }
          ++t_row_val;
        }
        ++t_w;
      }

      


      CHKERR MatSetValues(getFEMethod()->ts_B, row_data, col_data,
                          &locMat(0, 0), ADD_VALUES);
      

      if (row_side != col_side || row_type != col_type) {
        locTransMat.resize(nb_col_dofs, nb_row_dofs, false);
        noalias(locTransMat) = trans(locMat);
        CHKERR MatSetValues(getFEMethod()->ts_B, col_data, row_data,
                            &locTransMat(0, 0), ADD_VALUES);
      }

    }
    MoFEMFunctionReturn(0);
  }

private:
  MatrixDouble locMat, locTransMat;
  Range     essentialEnts;
  int cOmp;
  int uRank;
};

struct OpBCConstraintLHS : OpVolEle {
         OpBCConstraintLHS(std::string                          disp_name,
                        const int                            comp,
                        const int                            u_rank,
                        boost::shared_ptr<std::vector<bool>> boundary_marker = nullptr)
      : OpVolEle(disp_name, disp_name, OpVolEle::OPROWCOL)
      , boundaryMarker(boundary_marker)
      , cOmp(comp) 
      , uRank(u_rank)
      {
        sYmm = false;
      }

  MoFEMErrorCode doWork(int row_side, int col_side, EntityType row_type,
                        EntityType col_type, EntData &row_data,
                        EntData &col_data) {
    MoFEMFunctionBegin;
    const int nb_row_dofs = row_data.getIndices().size();
    const int nb_col_dofs = col_data.getIndices().size();

    if (nb_row_dofs && nb_col_dofs) {
      CHKERR flg_indicies(row_data, boundaryMarker, nb_row_dofs, cOmp, uRank);
    }
    MoFEMFunctionReturn(0);
  }

private:
  int                                  cOmp;
  int                                  uRank;
  boost::shared_ptr<std::vector<bool>> boundaryMarker;

};

struct OpBCConstraint : OpVolEle {
  OpBCConstraint(std::string                          disp_name, 
                    int                                  comp, 
                    const int                            u_rank,
                    boost::shared_ptr<std::vector<bool>> boundary_marker = nullptr)
      : OpVolEle(disp_name, OpVolEle::OPROW)
      , boundaryMarker(boundary_marker)
      , cOmp(comp)
      , uRank(u_rank)
      {}

  MoFEMErrorCode doWork(int row_side, EntityType row_type, EntData &row_data) {
    MoFEMFunctionBegin;
    const int nb_row_dofs = row_data.getIndices().size();

    if (nb_row_dofs){

      CHKERR flg_indicies(row_data, boundaryMarker, nb_row_dofs, cOmp, uRank);
      // cout << "comp :" << cOmp << "  Indices Constraint : " << row_data.getIndices() << endl;
    }
    MoFEMFunctionReturn(0);
  }


private:
  int                                  cOmp;
  int                                  uRank;
  boost::shared_ptr<std::vector<bool>> boundaryMarker;
};

struct OpEssentialRHS_U : OpFaceEle {
  OpEssentialRHS_U(std::string                          disp_name,
                  boost::shared_ptr<PreviousData>      &common_data,
                  const int                             u_rank,
                  int                                  comp,
                  double                               bc_val,
                  const Range                          &essential_ents)
      : OpFaceEle(disp_name, OpFaceEle::OPROW)
      , essentialEnts(essential_ents)
      , cOmp(comp)
      , uRank(u_rank)
      , bcVal(bc_val)
      , commonData(common_data)
      {  }
  double bcVal;
  MoFEMErrorCode doWork(int row_side, EntityType row_type, EntData &row_data) {
    MoFEMFunctionBegin;
    const int nb_row_dofs = row_data.getIndices().size();

    if (nb_row_dofs) {

      EntityHandle ent = getFEEntityHandle();
      // getFEEntityHandle();

      if (essentialEnts.find(ent) == essentialEnts.end()) {
        MoFEMFunctionReturnHot(0);
      }

      auto t_dispValue = getFTensor1FromMat<3>(commonData->dispValue);

      locRes.resize(nb_row_dofs, false);
      locRes.clear();

      const int nb_integration_pts = getGaussPts().size2();

      auto t_row_val = row_data.getFTensor0N();

      auto t_w = getFTensor0IntegrationWeight();
      const double vol = getMeasure();
      // auto t_coords = getFTensor1CoordsAtGaussPts();
      const double time = getFEMethod()->ts_t;

      for (int gg = 0; gg != nb_integration_pts; ++gg) {
        const double a = vol * t_w;

        for (int rr = 0; rr != nb_row_dofs / uRank; ++rr) {
            locRes[uRank * rr + cOmp] += a * t_row_val * (t_dispValue(cOmp) - time * bcVal);
          ++t_row_val;
        }
        ++t_dispValue;
        ++t_w;
      }

      CHKERR VecSetOption(getFEMethod()->ts_F, VEC_IGNORE_NEGATIVE_INDICES,
                          PETSC_TRUE);
      CHKERR VecSetValues(getFEMethod()->ts_F, row_data, &*locRes.begin(),
                          ADD_VALUES);
    }
    MoFEMFunctionReturn(0);
  }

private:
  VectorDouble                         locRes;
  int                                  cOmp;
  int                                  uRank;
  boost::shared_ptr<PreviousData>      commonData;
  Range                                essentialEnts;
};
// %%%%%%%%%%%%%%%%%%%%%%%%Chemical Operators%%%%%%%%%%%%%%%%%%%%%%%%%%

struct OpInitialMass : public OpVolEle {
  OpInitialMass(const std::string &mass_field, 
                Range             &inner_ents,
                double            &init_value)
      : OpVolEle(mass_field, OpVolEle::OPROW)
      , innerEnts(inner_ents) 
      , initValue(init_value)
      {}
  MatrixDouble nN;
  VectorDouble nF;
  double       initValue;
  Range        innerEnts;
  MoFEMErrorCode doWork(int        side, 
                        EntityType type, 
                        EntData    &data) {
    MoFEMFunctionBegin;
    int nb_dofs = data.getIndices().size();
    if (nb_dofs) {
      EntityHandle fe_ent = getFEEntityHandle();
      bool not_inner_side = (innerEnts.find(fe_ent) == innerEnts.end());
      if (not_inner_side) {
        MoFEMFunctionReturnHot(0);
      }
      int nb_gauss_pts = getGaussPts().size2();
      
      nN.resize(nb_dofs, nb_dofs, false);
      nF.resize(nb_dofs, false);
      nN.clear();
      nF.clear();

      auto t_row_mass = data.getFTensor0N();
      auto t_w = getFTensor0IntegrationWeight();
      const double vol = getMeasure();

      for (int gg = 0; gg != nb_gauss_pts; gg++) {
        const double a = t_w * vol;
        // double r = ((double) rand() / (RAND_MAX));
        for (int rr = 0; rr != nb_dofs; rr++) {
          auto t_col_mass = data.getFTensor0N(gg, 0);
          nF[rr] += a * initValue * t_row_mass;
          for (int cc = 0; cc != nb_dofs; cc++) {
            nN(rr, cc) += a * t_row_mass * t_col_mass;
            ++t_col_mass;
          }
          ++t_row_mass;
        }
        ++t_w;
      }

      cholesky_decompose(nN);
      cholesky_solve(nN, nF, ublas::lower());
      for (auto &dof : data.getFieldDofs()) {
        dof->getFieldData() = nF[dof->getEntDofIdx()];
      }
    }
    MoFEMFunctionReturn(0);
  }
};
struct OpFluxEssentialBC : public OpFaceEle {
  OpFluxEssentialBC(std::string flux_field, 
                    Range       &essential_bd_ents,
                    double       flux_ess_value)
      : OpFaceEle(flux_field, OpFaceEle::OPROW),
        essentialEnts(essential_bd_ents), essenValue(flux_ess_value) {}

  double essenValue;
  MoFEMErrorCode doWork(int side, EntityType type, EntData &data) {
    MoFEMFunctionBegin;
    int nb_dofs = data.getIndices().size();
    
    if (nb_dofs) {
      EntityHandle fe_ent = getFEEntityHandle();
      
      bool is_essential = (essentialEnts.find(fe_ent) != essentialEnts.end());
      if (is_essential){
        
      int nb_gauss_pts = getGaussPts().size2();
      int size2 = data.getN().size2();
      if (3 * nb_dofs != static_cast<int>(data.getN().size2()))
        SETERRQ(PETSC_COMM_WORLD, MOFEM_DATA_INCONSISTENCY,
                "wrong number of dofs");
      nN.resize(nb_dofs, nb_dofs, false);
      nF.resize(nb_dofs, false);
      nN.clear();
      nF.clear();

      auto t_row_tau = data.getFTensor1N<3>();

      double *normal_ptr;
      if (getNormalsAtGaussPts().size1() == (unsigned int)nb_gauss_pts) {
        // HO geometry
        normal_ptr = &getNormalsAtGaussPts(0)[0];
      } else {
        // Linear geometry, i.e. constant normal on face
        normal_ptr = &getNormal()[0];
      }
      // set tensor from pointer
      FTensor::Tensor1<const double *, 3> t_normal(normal_ptr, &normal_ptr[1],
                                                    &normal_ptr[2], 3);

      auto t_w = getFTensor0IntegrationWeight();
      const double vol = getMeasure();
      double nrm2 = 0;
      for (int gg = 0; gg < nb_gauss_pts; gg++) {
        if (gg == 0) {
          nrm2 = sqrt(t_normal(i) * t_normal(i));
        }
        const double a = t_w * vol;
        for (int rr = 0; rr != nb_dofs; rr++) {
          FTensor::Tensor1<const double *, 3> t_col_tau(
              &data.getVectorN<3>(gg)(0, HVEC0),
              &data.getVectorN<3>(gg)(0, HVEC1),
              &data.getVectorN<3>(gg)(0, HVEC2), 3);
          nF[rr] += a * essenValue * t_row_tau(i) * t_normal(i) / nrm2;
          for (int cc = 0; cc != nb_dofs; cc++) {
            nN(rr, cc) += a * (t_row_tau(i) * t_normal(i)) *
                          (t_col_tau(i) * t_normal(i)) / (nrm2 * nrm2);
            ++t_col_tau;
          }
          ++t_row_tau;
        }
        // If HO geometry increment t_normal to next integration point
        if (getNormalsAtGaussPts().size1() == (unsigned int)nb_gauss_pts) {
          ++t_normal;
          nrm2 = sqrt(t_normal(i) * t_normal(i));
        }
        ++t_w;
      }

      cholesky_decompose(nN);
      cholesky_solve(nN, nF, ublas::lower());

      for (auto &dof : data.getFieldDofs()) {
        dof->getFieldData() = nF[dof->getEntDofIdx()];
      }
  
    }
    }
    MoFEMFunctionReturn(0);
  }

private:
  MatrixDouble nN;
  VectorDouble nF;
  Range essentialEnts;
};

struct OpSlowRhsV : OpVolEle{
  typedef boost::function<double(const double)> ScalarFunction;
  OpSlowRhsV(std::string                     mass_field,
             boost::shared_ptr<PreviousData> &common_data,
             ScalarFunction                  rhs_m,
             DispKineticFunc                 disp_kinetic_func)
      : OpVolEle(mass_field, OpVolEle::OPROW)
      , commonData(common_data) 
      , rhsM(rhs_m)
      , dispKineticFunc(disp_kinetic_func) 
      {}
  ScalarFunction rhsM;
  DispKineticFunc dispKineticFunc;
  MoFEMErrorCode doWork(int        side, 
                        EntityType type, 
                        EntData    &data) {
    MoFEMFunctionBegin;
    const int nb_dofs = data.getIndices().size();
    if (nb_dofs) {
      vecF.resize(nb_dofs, false);
      mat.resize(nb_dofs, nb_dofs, false);
      vecF.clear();
      mat.clear();
      const int nb_integration_pts = getGaussPts().size2();
      auto t_mass_value = getFTensor0FromVec(commonData->massValue);
      auto t_dispGrad = getFTensor2FromMat<3, 3>(commonData->dispGrad);
      

      auto t_row_v_base = data.getFTensor0N();
      auto t_w = getFTensor0IntegrationWeight();
      const double vol = getMeasure();

      for (int gg = 0; gg != nb_integration_pts; ++gg) {
        const double a = vol * t_w;
        double slow_value = rhsM(t_mass_value);
        double gRhs = dispKineticFunc(t_dispGrad, d3);
        // double gRhs = 0.0;
        for (int rr = 0; rr != nb_dofs; ++rr) {
          auto t_col_v_base = data.getFTensor0N(gg, 0);
          vecF[rr] += a * (slow_value + gRhs) * t_row_v_base;
          for (int cc = 0; cc != nb_dofs; ++cc) {
            mat(rr, cc) += a * t_row_v_base * t_col_v_base;
            ++t_col_v_base;
          }
          ++t_row_v_base;
        }
        ++t_dispGrad;
        ++t_mass_value;
        ++t_w;
      }
      cholesky_decompose(mat);
      cholesky_solve(mat, vecF, ublas::lower());
      CHKERR VecSetOption(getFEMethod()->ts_F, VEC_IGNORE_NEGATIVE_INDICES,
                          PETSC_TRUE);
      CHKERR VecSetValues(getFEMethod()->ts_F, data, &*vecF.begin(),
                          ADD_VALUES);
    }
    MoFEMFunctionReturn(0);
  }

private:
  boost::shared_ptr<PreviousData> commonData;
  VectorDouble vecF;
  MatrixDouble mat;

};

struct OpStiffRhsTau : OpVolEle {
  OpStiffRhsTau(std::string                     flux_field,
                boost::shared_ptr<PreviousData> &common_data,
                std::map<int, BlockData>        &block_map,
                DiffuseFunc                     diffuse_func)
      : OpVolEle(flux_field, OpVolEle::OPROW)
      , commonData(common_data)
      , setOfBlock(block_map)
      , diffuseFunc(diffuse_func) {}
  DiffuseFunc diffuseFunc;
  MoFEMErrorCode doWork(int        side, 
                        EntityType type, 
                        EntData    &data) {
    MoFEMFunctionBegin;

    const int nb_dofs = data.getIndices().size();
    if (nb_dofs) {
      auto find_block_data = [&]() {
        EntityHandle fe_ent = getFEEntityHandle();
        BlockData *block_raw_ptr = nullptr;
        for (auto &m : setOfBlock) {
          if (m.second.block_ents.find(fe_ent) != m.second.block_ents.end()) {
            block_raw_ptr = &m.second;
            break;
          }
        }
        return block_raw_ptr;
      };

      auto block_data_ptr = find_block_data();
      if (!block_data_ptr)
        SETERRQ(PETSC_COMM_SELF, MOFEM_DATA_INCONSISTENCY, "Block not found");
      auto &block_data = *block_data_ptr;

      block_data.getMatParam();

      vecF.resize(nb_dofs, false);
      vecF.clear();

      const int nb_integration_pts = getGaussPts().size2();
      // auto t_dispGrad = getFTensor2FromMat<3, 3>(commonData->dispGrad);
      auto t_flux_value = getFTensor1FromMat<3>(commonData->fluxValue);
      auto t_mass_value = getFTensor0FromVec(commonData->massValue);
      auto t_tau_base = data.getFTensor1N<3>();

      auto t_tau_grad = data.getFTensor2DiffN<3, 3>();

      auto t_w = getFTensor0IntegrationWeight();
      const double vol = getMeasure();

      FTensor::Tensor2_symmetric<double, 3> t_stress(0.0, 0.0, 0.0, 
                                                     0.0, 0.0, 0.0);
      FTensor::Tensor2<double, 3, 3> t_invH;

      double detH = 0;


      for (int gg = 0; gg < nb_integration_pts; ++gg) {

        // t_stress(i, j) = block_data.tD(i, j, k, l) * t_dispGrad(k, l);

        auto t_H = diffuseFunc(t_stress, c0, c1, c2);


        CHKERR determinantTensor3by3(t_H, detH);
        CHKERR invertTensor3by3(t_H, detH, t_invH);
        // const double K = block_data.B0;
        // const double K_inv = 1. / K;
        const double a = vol * t_w;
        for (int rr = 0; rr < nb_dofs; ++rr) {
          double div_base = t_tau_grad(0, 0) + t_tau_grad(1, 1) + t_tau_grad(2, 2);
          vecF[rr] += ( (t_tau_base(i) * (t_invH(i, j) * t_flux_value(j))) -
                       div_base * t_mass_value) * a;
          ++t_tau_base;
          ++t_tau_grad;
        }
        // ++t_dispGrad;
        ++t_flux_value;
        ++t_mass_value;
        ++t_w;
      }
      CHKERR VecSetOption(getFEMethod()->ts_F, VEC_IGNORE_NEGATIVE_INDICES,
                          PETSC_TRUE);
      CHKERR VecSetValues(getFEMethod()->ts_F, data, &*vecF.begin(),
                          ADD_VALUES);
    }
    MoFEMFunctionReturn(0);
  }

private:
  boost::shared_ptr<PreviousData> commonData;
  VectorDouble vecF;
  std::map<int, BlockData> setOfBlock;
};

struct OpStiffRhsV : OpVolEle {
  OpStiffRhsV(std::string                     flux_field,
              boost::shared_ptr<PreviousData> &data,
              std::map<int, BlockData>        &block_map)
      : OpVolEle(flux_field, OpVolEle::OPROW)
      , commonData(data)
      , setOfBlock(block_map) 
      {}

  MoFEMErrorCode doWork(int        side, 
                        EntityType type, 
                        EntData    &data) {
    MoFEMFunctionBegin;
    const int nb_dofs = data.getIndices().size();

    if (nb_dofs) {
      auto find_block_data = [&]() {
        EntityHandle fe_ent = getFEEntityHandle();
        BlockData *block_raw_ptr = nullptr;
        for (auto &m : setOfBlock) {
          if (m.second.block_ents.find(fe_ent) != m.second.block_ents.end()) {
            block_raw_ptr = &m.second;
            break;
          }
        }
        return block_raw_ptr;
      };

      auto block_data_ptr = find_block_data();
      if (!block_data_ptr)
        SETERRQ(PETSC_COMM_SELF, MOFEM_DATA_INCONSISTENCY, "Block not found");
      auto &block_data = *block_data_ptr;

      block_data.getMatParam();

      vecF.resize(nb_dofs, false);
      vecF.clear();
      const int nb_integration_pts = getGaussPts().size2();
      auto t_mass_dot = getFTensor0FromVec(commonData->massDot);
      auto t_flux_div = getFTensor0FromVec(commonData->fluxDiv);
      auto t_row_v_base = data.getFTensor0N();
      auto t_w = getFTensor0IntegrationWeight();
      const double vol = getMeasure();

      const double ct = getFEMethod()->ts_t;
      for (int gg = 0; gg < nb_integration_pts; ++gg) {
        const double a = vol * t_w;
    
        for (int rr = 0; rr < nb_dofs; ++rr) {
          vecF[rr] += (t_row_v_base * (t_mass_dot + t_flux_div)) * a;
          ++t_row_v_base;
        }
        ++t_mass_dot;
        ++t_flux_div;
        ++t_w;
      }
      CHKERR VecSetOption(getFEMethod()->ts_F, VEC_IGNORE_NEGATIVE_INDICES,
                          PETSC_TRUE);
      CHKERR VecSetValues(getFEMethod()->ts_F, data, &*vecF.begin(),
                          ADD_VALUES);
    }
    MoFEMFunctionReturn(0);
  }

private:
  boost::shared_ptr<PreviousData> commonData;
  VectorDouble vecF;
  std::map<int, BlockData> setOfBlock;
};

struct OpLhsTauTau : OpVolEle {
  OpLhsTauTau(std::string                     flux_field,
              boost::shared_ptr<PreviousData> &commonData,
              std::map<int, BlockData>        &block_map,
              DiffuseFunc                     diffuse_func)
      : OpVolEle(flux_field, flux_field, OpVolEle::OPROWCOL)
      , setOfBlock(block_map)
      , commonData(commonData)
      , diffuseFunc(diffuse_func) 
      {
        sYmm = false;
      }
  DiffuseFunc diffuseFunc;
  MoFEMErrorCode doWork(int row_side, int col_side, EntityType row_type,
                        EntityType col_type, EntData &row_data,
                        EntData &col_data) {
    MoFEMFunctionBegin;
    const int nb_row_dofs = row_data.getIndices().size();
    const int nb_col_dofs = col_data.getIndices().size();

    if (nb_row_dofs && nb_col_dofs) {
      auto find_block_data = [&]() {
        EntityHandle fe_ent = getFEEntityHandle();
        BlockData *block_raw_ptr = nullptr;
        for (auto &m : setOfBlock) {
          if (m.second.block_ents.find(fe_ent) != m.second.block_ents.end()) {
            block_raw_ptr = &m.second;
            break;
          }
        }
        return block_raw_ptr;
      };

      auto block_data_ptr = find_block_data();
      if (!block_data_ptr)
        SETERRQ(PETSC_COMM_SELF, MOFEM_DATA_INCONSISTENCY, "Block not found");
      auto &block_data = *block_data_ptr;

      block_data.getMatParam();

      mat.resize(nb_row_dofs, nb_col_dofs, false);
      mat.clear();
      const int nb_integration_pts = getGaussPts().size2();
      // auto t_mass_value = getFTensor0FromVec(commonData->massValue);

      auto t_row_tau_base = row_data.getFTensor1N<3>();
      // auto t_dispGrad = getFTensor2FromMat<3, 3>(commonData->dispGrad);

      FTensor::Tensor2_symmetric<double, 3> t_stress(0.0, 0.0, 0.0,
                                                     0.0, 0.0, 0.0);
      FTensor::Tensor2<double, 3, 3> t_invH;
      double detH = 0.0;

      auto t_w = getFTensor0IntegrationWeight();
      const double vol = getMeasure();

      for (int gg = 0; gg != nb_integration_pts; ++gg) {
        const double a = vol * t_w;
        // t_stress(i, j) = block_data.tD(i, j, k, l) * t_dispGrad(k, l);
        auto t_H = diffuseFunc(t_stress, c0, c1, c2);
        CHKERR determinantTensor3by3(t_H, detH);
        CHKERR invertTensor3by3(t_H, detH, t_invH);
        // const double K = block_data.B0;
        // const double K_inv = 1. / K;
        for (int rr = 0; rr != nb_row_dofs; ++rr) {
          auto t_col_tau_base = col_data.getFTensor1N<3>(gg, 0);
          for (int cc = 0; cc != nb_col_dofs; ++cc) {
            mat(rr, cc) += (t_row_tau_base(i) * (t_invH(i, j) * t_col_tau_base(j))) * a;
            ++t_col_tau_base;
          }
          ++t_row_tau_base;
        }
        // ++t_mass_value;
        // ++t_dispGrad;
        ++t_w;
      }
      CHKERR MatSetValues(getFEMethod()->ts_B, row_data, col_data, &mat(0, 0),
                          ADD_VALUES);
      // if (row_side != col_side || row_type != col_type) {
      //   transMat.resize(nb_col_dofs, nb_row_dofs, false);
      //   noalias(transMat) = trans(mat);
      //   CHKERR MatSetValues(getFEMethod()->ts_B, col_data, row_data,
      //                       &transMat(0, 0), ADD_VALUES);
      // }
    }
    MoFEMFunctionReturn(0);
  }

private:
  boost::shared_ptr<PreviousData> commonData;
  MatrixDouble mat;
  std::map<int, BlockData> setOfBlock;
};

struct OpLhsTauV : OpVolEle {
  OpLhsTauV(std::string                     flux_field, 
                    std::string                     mass_field,
                    std::map<int, BlockData>        &block_map)
      : OpVolEle(flux_field, mass_field, OpVolEle::OPROWCOL), 
        setOfBlock(block_map) 
        {
          sYmm = false;
        }

  MoFEMErrorCode doWork(int        row_side, 
                        int        col_side, 
                        EntityType row_type,
                        EntityType col_type, 
                        EntData    &row_data,
                        EntData    &col_data) {
    MoFEMFunctionBegin;
    const int nb_row_dofs = row_data.getIndices().size();
    const int nb_col_dofs = col_data.getIndices().size();

    if (nb_row_dofs && nb_col_dofs) {
      auto find_block_data = [&]() {
        EntityHandle fe_ent = getFEEntityHandle();
        BlockData *block_raw_ptr = nullptr;
        for (auto &m : setOfBlock) {
          if (m.second.block_ents.find(fe_ent) != m.second.block_ents.end()) {
            block_raw_ptr = &m.second;
            break;
          }
        }
        return block_raw_ptr;
      };

      auto block_data_ptr = find_block_data();
      if (!block_data_ptr)
        SETERRQ(PETSC_COMM_SELF, MOFEM_DATA_INCONSISTENCY, "Block not found");
      auto &block_data = *block_data_ptr;

      block_data.getMatParam();

      mat.resize(nb_row_dofs, nb_col_dofs, false);
      mat.clear();
      const int nb_integration_pts = getGaussPts().size2();
      auto t_w = getFTensor0IntegrationWeight();

      auto t_row_tau_grad = row_data.getFTensor2DiffN<3, 3>();
      const double vol = getMeasure();

      for (int gg = 0; gg != nb_integration_pts; ++gg) {
        const double a = vol * t_w;

        for (int rr = 0; rr != nb_row_dofs; ++rr) {
          auto t_col_v_base = col_data.getFTensor0N(gg, 0);
          for (int cc = 0; cc != nb_col_dofs; ++cc) {
            double div_row_base = t_row_tau_grad(i, i);
            mat(rr, cc) += - div_row_base * t_col_v_base * a;
            ++t_col_v_base;
          }

          ++t_row_tau_grad;
        }
        ++t_w;

      }
      CHKERR MatSetValues(getFEMethod()->ts_B, row_data, col_data, &mat(0, 0),
                          ADD_VALUES);
    }
    MoFEMFunctionReturn(0);
  }

private:
  MatrixDouble mat;
  std::map<int, BlockData> setOfBlock;
};

struct OpLhsVTau : OpVolEle {
  OpLhsVTau(std::string        mass_field, 
            std::string        flux_field)
      : OpVolEle(mass_field, flux_field, OpVolEle::OPROWCOL) {
    sYmm = false;
  }

  MoFEMErrorCode doWork(int        row_side, 
                        int        col_side, 
                        EntityType row_type,
                        EntityType col_type, 
                        EntData    &row_data,
                        EntData    &col_data) {
    MoFEMFunctionBegin;
    const int nb_row_dofs = row_data.getIndices().size();
    const int nb_col_dofs = col_data.getIndices().size();

    if (nb_row_dofs && nb_col_dofs) {
      mat.resize(nb_row_dofs, nb_col_dofs, false);
      mat.clear();
      const int nb_integration_pts = getGaussPts().size2();
      auto t_w = getFTensor0IntegrationWeight();
      auto t_row_v_base = row_data.getFTensor0N();
      const double vol = getMeasure();

      for (int gg = 0; gg != nb_integration_pts; ++gg) {
        const double a = vol * t_w;
        for (int rr = 0; rr != nb_row_dofs; ++rr) {
          auto t_col_tau_grad = col_data.getFTensor2DiffN<3, 3>(gg, 0);
          for (int cc = 0; cc != nb_col_dofs; ++cc) {
            double div_col_base = t_col_tau_grad(0, 0) + t_col_tau_grad(1, 1) + t_col_tau_grad(2, 2);
            mat(rr, cc) += (t_row_v_base * div_col_base) * a;
            ++t_col_tau_grad;
          }
          ++t_row_v_base;
        }
        ++t_w;
      }
      CHKERR MatSetValues(getFEMethod()->ts_B, row_data, col_data, &mat(0, 0),
                          ADD_VALUES);
    }
    MoFEMFunctionReturn(0);
  }

private:
  MatrixDouble mat;
};

struct OpLhsVV : OpVolEle {
  OpLhsVV(std::string mass_field)
      : OpVolEle(mass_field, mass_field, OpVolEle::OPROWCOL) {
    sYmm = false;
  }

  MoFEMErrorCode doWork(int        row_side, 
                        int        col_side, 
                        EntityType row_type,
                        EntityType col_type, 
                        EntData    &row_data,
                        EntData    &col_data) {
    MoFEMFunctionBegin;

    const int nb_row_dofs = row_data.getIndices().size();
    const int nb_col_dofs = col_data.getIndices().size();
    if (nb_row_dofs && nb_col_dofs) {

      mat.resize(nb_row_dofs, nb_col_dofs, false);
      mat.clear();
      const int nb_integration_pts = getGaussPts().size2();

      auto t_row_v_base = row_data.getFTensor0N();

      auto t_w = getFTensor0IntegrationWeight();
      const double ts_a = getFEMethod()->ts_a;
      const double vol = getMeasure();

      for (int gg = 0; gg != nb_integration_pts; ++gg) {
        const double a = vol * t_w;

        for (int rr = 0; rr != nb_row_dofs; ++rr) {
          auto t_col_v_base = col_data.getFTensor0N(gg, 0);
          for (int cc = 0; cc != nb_col_dofs; ++cc) {
            mat(rr, cc) += (ts_a * t_row_v_base * t_col_v_base) * a;

            ++t_col_v_base;
          }
          ++t_row_v_base;
        }
        ++t_w;
      }
      CHKERR MatSetValues(getFEMethod()->ts_B, row_data, col_data, &mat(0, 0),
                          ADD_VALUES);
      // if (row_side != col_side || row_type != col_type) {
      //   transMat.resize(nb_col_dofs, nb_row_dofs, false);
      //   noalias(transMat) = trans(mat);
      //   CHKERR MatSetValues(getFEMethod()->ts_B, col_data, row_data,
      //                       &transMat(0, 0), ADD_VALUES);
      // }
    }
    MoFEMFunctionReturn(0);
  }

private:
  MatrixDouble mat, transMat;
};

//%%%%%%%%%%%%%%%%%%%%%%%%%End Chemo-Operators%%%%%%%%%%%%%%%%%%%%%%%


//%%%%%%%%%%%%%%%%%%%%%%%%%Begin Coupling%%%%%%%%%%%%%%%%%%%%%%%%%%%%
struct OpLHS_UV : OpVolEle {
  OpLHS_UV(std::string                     disp_name, 
           std::string                     mass_name,
           boost::shared_ptr<PreviousData> &common_data,
           std::map<int, BlockData>        &block_map,
           VectorFunc                      diff_conc_func)
      : OpVolEle(disp_name, mass_name, OpVolEle::OPROWCOL)
      , setOfBlock(block_map)
      , commonData(common_data) 
      , diffConcFunc(diff_conc_func)
      {
        sYmm = false;
      }

  MoFEMErrorCode doWork(int        row_side, 
                        int        col_side, 
                        EntityType row_type,
                        EntityType col_type, 
                        EntData    &row_data,
                        EntData    &col_data) {
    MoFEMFunctionBegin;
    const int nb_row_dofs = row_data.getIndices().size();
    const int nb_col_dofs = col_data.getIndices().size();

    if (nb_row_dofs && nb_col_dofs) {

      auto find_block_data = [&]() {
        EntityHandle fe_ent = getFEEntityHandle();
        BlockData *block_raw_ptr = nullptr;
        for (auto &m : setOfBlock) {
          if (m.second.block_ents.find(fe_ent) != m.second.block_ents.end()) {
            block_raw_ptr = &m.second;
            break;
          }
        }
        return block_raw_ptr;
      };

      auto block_data_ptr = find_block_data();
      if (!block_data_ptr)
        SETERRQ(PETSC_COMM_SELF, MOFEM_DATA_INCONSISTENCY, "Block not found");
      auto &block_data = *block_data_ptr;

      block_data.getMatParam();

      auto get_tensor1 = [](MatrixDouble &m, const int r, const int c) {
        return FTensor::Tensor1<double *, 3>(
                                             &m(r + 0, c), 
                                             &m(r + 1, c), 
                                             &m(r + 2, c));
      };

      locMat.resize(nb_row_dofs, nb_col_dofs, false);
      locMat.clear();

      const int nb_integration_pts = getGaussPts().size2();
      const int row_nb_base_fun = row_data.getN().size2();

      auto t_massValue = getFTensor0FromVec(commonData->massValue);

      auto t_row_disp_diff = row_data.getFTensor1DiffN<3>();

      auto t_w = getFTensor0IntegrationWeight();
      const double vol = getMeasure();

      
      for (int gg = 0; gg != nb_integration_pts; ++gg) {
        const double a = vol * t_w;

        int rr = 0;
        for (; rr != nb_row_dofs / 3; ++rr) {

          auto t_col_mass = col_data.getFTensor0N(gg, 0);
          auto t_diffConcFunc = diffConcFunc(t_massValue, d2);

          for (int cc = 0; cc != nb_col_dofs; ++cc) {
            auto t_subLocMat = get_tensor1(locMat, 3 * rr, cc);
            
            t_subLocMat(i) += - a * t_diffConcFunc(i, j) * t_row_disp_diff(j) * t_col_mass;

            ++t_col_mass;
          }
          ++t_row_disp_diff;
        }
        ++t_massValue;
        for (; rr != row_nb_base_fun; ++rr)
          ++t_row_disp_diff;
        ++t_w;
      }
      // cout << "===================================="<<endl;
      // cout << "Indices UV : " << row_data.getIndices() << endl;
      CHKERR MatSetValues(getFEMethod()->ts_B, row_data, col_data,
                          &locMat(0, 0), ADD_VALUES);

      // if (row_side != col_side || row_type != col_type) {

      //   locTransMat.resize(nb_col_dofs, nb_row_dofs, false);
      //   noalias(locTransMat) = trans(locMat);
      //   CHKERR MatSetValues(getFEMethod()->ts_B, col_data, row_data,
      //                       &locTransMat(0, 0), ADD_VALUES);

      // }
    }
    MoFEMFunctionReturn(0);
  }

private:
  boost::shared_ptr<PreviousData> commonData;
  MatrixDouble locMat, locTransMat;
  std::map<int, BlockData> setOfBlock;
  VectorFunc  diffConcFunc;
};
//%%%%%%%%%%%%%%%%%%%%%%%%%End Coupling%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
struct EraseLHSRows : public FEMethod {
  EraseLHSRows(MoFEM::Interface &m_field, std::string problem_name,
               Range &essential_boundary_entsX, Range &essential_boundary_entsY,
               Range &essential_boundary_entsZ)
      : mField(m_field), entX(essential_boundary_entsX),
        entY(essential_boundary_entsY), entZ(essential_boundary_entsZ),
        problemName(problem_name) {}

  MoFEMErrorCode preProcess(){
    MoFEMFunctionBegin;

    auto pre_proc = [&](Range &boundary_ents, int comp)  {
      MoFEMFunctionBegin;

      IS bc_is;

      mField.getInterface<ISManager>()->isCreateProblemFieldAndRank(
          problemName, ROW, "U", comp, comp, &bc_is, &boundary_ents);

      // CHKERR ISView(bc_is, PETSC_VIEWER_STDOUT_SELF);

      CHKERR MatZeroRowsIS(ts_B, bc_is, 0.0, PETSC_NULL, PETSC_NULL);

     

      CHKERR ISDestroy(&bc_is);
      MoFEMFunctionReturn(0);
    };

    MatSetOption(ts_B,MAT_KEEP_NONZERO_PATTERN,PETSC_TRUE);
    
    
    // CHKERR MatAssemblyBegin(ts_B, MAT_FINAL_ASSEMBLY);
    // CHKERR MatAssemblyEnd(ts_B, MAT_FINAL_ASSEMBLY);
    // cout << "EraseLHSRows : entX " << endl;
    CHKERR pre_proc(entX, 0);
    // cout << "EraseLHSRows : entY " << endl;
    CHKERR pre_proc(entY, 1);
    // cout << "EraseLHSRows : entZ " << endl;
    CHKERR pre_proc(entZ, 2);
    // CHKERR MatAssemblyBegin(ts_B, MAT_FLUSH_ASSEMBLY);
    // CHKERR MatAssemblyEnd(ts_B, MAT_FLUSH_ASSEMBLY);

    MoFEMFunctionReturn(0);
  }



  private: 
    Range            entX;
    Range            entY;
    Range            entZ;
    MoFEM::Interface &mField;
    std::string      problemName;
};



struct EraseRHSRows : public FEMethod {
  EraseRHSRows(MoFEM::Interface &m_field, std::string problem_name,
               Range &essential_boundary_entsX, Range &essential_boundary_entsY,
               Range &essential_boundary_entsZ)
      : mField(m_field), entX(essential_boundary_entsX),
        entY(essential_boundary_entsY), entZ(essential_boundary_entsZ),
        problemName(problem_name) {}

  MoFEMErrorCode preProcess() {
    MoFEMFunctionBegin;

    auto pre_proc = [&](Range &boundary_ents, int comp) {
      MoFEMFunctionBegin;

      IS bc_is;

      mField.getInterface<ISManager>()->isCreateProblemFieldAndRank(
          problemName, ROW, "U", comp, comp, &bc_is, &boundary_ents);
  
      // CHKERR ISView(bc_is, PETSC_VIEWER_STDOUT_SELF);

      CHKERR VecAssemblyBegin(ts_F);
      CHKERR VecAssemblyEnd(ts_F);

      CHKERR VecISSet(ts_F, bc_is, 0.0);

      CHKERR ISDestroy(&bc_is);
      MoFEMFunctionReturn(0);
    };
    // cout << "EraseRHSRows : entX " << endl;
    CHKERR pre_proc(entX, 0);
    // cout << "EraseRHSRows : entY " << endl;
    CHKERR pre_proc(entY, 1);
    // cout << "EraseRHSRows : entZ " << endl;
    CHKERR pre_proc(entZ, 2);

    MoFEMFunctionReturn(0);
  }

private:
  Range entX;
  Range entY;
  Range entZ;
  MoFEM::Interface &mField;
  std::string problemName;
};

struct Monitor : public FEMethod {
  Monitor(MPI_Comm                                     &comm, 
          const int                                    &rank, 
          SmartPetscObj<DM>                            &dm,
          MoFEM::Interface                             &m_field,
          boost::shared_ptr<PostProcVolumeOnRefinedMesh> &post_proc)
      : mpiComm(comm)
      , mpiRank(rank)
      , dM(dm)
      , postProc(post_proc)
      , mField(m_field)
      {
      }
  MoFEMErrorCode preProcess() {
    MoFEMFunctionBegin;
    // for (_IT_GET_ENT_FIELD_BY_NAME_FOR_LOOP_(mField, "U", it)) {
    //   if (it->get()->getEntType() == MBVERTEX) {
        
    //     EntityHandle ent = it->get()->getEnt();
    //     VectorDouble3 coords(3);

    //     CHKERR mField.get_moab().get_coords(&ent, 1, &*coords.begin());
    //     for (int dd = 0; dd != 3; ++dd)
    //       coords[dd] += it->get()->getEntFieldData()[dd];
    //     CHKERR mField.get_moab().set_coords(&ent, 1, &*coords.begin());
    //   }
    // }
    MoFEMFunctionReturn(0);
  }
  MoFEMErrorCode operator()() { return 0; }

  MoFEMErrorCode postProcess() {
    MoFEMFunctionBegin;
    int save_every_nth_step = 1;
    CHKERR PetscOptionsGetInt(PETSC_NULL, "", "-save_every_nth_step",
                              &save_every_nth_step, PETSC_NULL);
    if (ts_step % save_every_nth_step == 0) {
      CHKERR DMoFEMLoopFiniteElements(dM, "dFE", postProc);
      CHKERR postProc->writeFile(
          "output_" + boost::lexical_cast<std::string>(ts_step) + ".h5m");
    }
    MoFEMFunctionReturn(0);
  }

private:
  SmartPetscObj<DM>                              dM;
  boost::shared_ptr<PostProcVolumeOnRefinedMesh> postProc;
  MPI_Comm                                       mpiComm;
  const int                                      mpiRank;
  MoFEM::Interface                               &mField;
};

}; // namespace ChemoMechOps

#endif //__CHEMOMECHOPERTATORS_HPP__
