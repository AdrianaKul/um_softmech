 
- a rectangle with the mentioned dimensions is created, that is
\code{.bash}
lx = 0.02;
ly = 0.005;
SetFactory("OpenCASCADE");
Rectangle(1) = {0, 0, 0, lx, ly, 0};
\endcode
the last line creates a rectangular surface with ID 1 and one of its vertices lies at 
the origin, and dimensions \f$lx\f$ along the \f$x\f$-axis and \f$ly\f$ along the \f$x\f$-axis.

- the rest of the four rectangles are obtained by duplicating and rotating surface 1 by 
appropriate angles and an axis of rotation, for example
\code{.bash}
Rotate {{0, 0, 1}, {lx, ly/2, 0}, Pi/3} { Duplicata { Surface{1}; }}
\endcode
this line make a duplicate of Surface 1 (to obtain a new surface 2), and rotates by \f$\pi/3\f$ in the 
counter clockwise direction along an axis parallel to the \f$z\f$-axis passing through the point with 
coordinates \f$(l_x, l_y/2, 0)\f$.

- since the Gmsh BooleanUnion operator only creates the union of 2 entities, we create the union of 
the 5 rectangle step by step deleting the original ones at each step, for instance 
\code{.bash}
BooleanUnion { Surface{1}; } { Surface{2}; }
Recursive Delete { Surface{1}; Surface{2}; }
\endcode
here the first line unites surface 1 and 2 to create a new surface, while the second deleted 
the original rectangles once the a surface which is the union of the two has been created.
 
- once we have the desired surface 9, the next step is to define where the physical curves and 
surfaces used to define the initial and boundary conditions. Since we can have parts of regions 
with different initial conditions, each region has to be created as physical surface in its own right. 
However, in this example problem we only have one type of initial conditions which defined all over the 
surface 9. Moreover, to define the boundary conditions one needs to obtain boundary id's in the in Gmsh, 
for instance
\code{.bash}
Physical Curve("ESSENTIAL") = {8, 12, 30};
Physical Surface("INITIAL") = {9};
\endcode
  

 Thus, the file \b paper_network.geo is as shown below 
\code{.bash}
lx = 0.02;
ly = 0.005;
SetFactory("OpenCASCADE");
Rectangle(1) = {0, 0, 0, lx, ly, 0};
Rotate {{0, 0, 1}, {lx, ly/2, 0}, Pi/3} { Duplicata { Surface{1}; }}
Rotate {{0, 0, 1}, {lx, ly/2, 0}, 2*Pi/3} { Duplicata { Surface{1}; }}
Rotate {{0, 0, 1}, {lx, ly/2, 0}, Pi} { Duplicata { Surface{1}; }}
Rotate {{0, 0, 1}, {lx, ly/2, 0}, 3*Pi/2} {Duplicata { Surface{1}; }}

BooleanUnion { Surface{1}; } { Surface{2}; }
Recursive Delete { Surface{1}; Surface{2}; }
BooleanUnion { Surface{6}; } { Surface{3}; }
Recursive Delete {  Surface{6}; Surface{3}; }
BooleanUnion { Surface{7}; } { Surface{4}; }
Recursive Delete {  Surface{7}; Surface{4}; }
BooleanUnion { Surface{8}; } { Surface{5}; }
Recursive Delete {  Surface{8}; Surface{5}; }

Physical Curve("ESSENTIAL") = {8, 12, 30};
Physical Surface("INITIAL") = {9};
\endcode

Having the \b paper_network.geo file created, we then generate the mesh in Gmsh 
to obtain \b paper_network.msh file. MoFEM does not use the \b paper_network.msh 
file directly, instead one needs to convert it into \b paper_network.h5m, a MoFEM 
equivalent mesh file. To do this, we create a configuration file \b bc.cfg that 
tells the converter to associate the physical groups in  \b paper_network.msh into 
BLOCKSET, NODESET and SIDESET that are readable by MoFEM. The corresponding 
configuration file,  \b bc.cfg, is as shown below


\code{.bash}
[block_1]

id=1001
add=BLOCKSET
name=ESSENTIAL

[block_2]

id=1002
add=BLOCKSET
name=INITIAL

\endcode

Now, the mesh is converted by the command

\code{.bash}
../../tools/add_meshsets -my_file paper_network.msh -meshsets_config bc.cfg -output_file paper_network.h5m
\endcode

The mesh \b paper\_network.h5m is now ready to be used as an input in executing the code. 

\section command_options Command options controlling the simulation
There are a bunch of commands that control the execution of the code. Some of the are listed in the 
parameter file \b param\_file.petsc
\code{.bash}
-file_name paper_network.h5m # input the mesh
 
 -ts_monitor     # for printing information at each time step
 -ts_exact_final_time stepover   # to stop the analysis when T = ceil(final_time/dt)*dt
 -ts_dt 0.0000005                       # to specify the time step
 -ts_final_time 5                          # to specify the final time
 -ts_adapt_type none                  # to specify adaptivity in time
 
 -ksp_type gmres                        # to specify the type of linear solver at each Newton step
 -pc_type lu                                 # preconditioner type
 -pc_factor_mat_solver_type mumps.  # preconditoner factoring algorithm 
 
-snes_atol 1e-7    # Newton tolerance for pressure head
-snes_rtol 1e-12   # Newton tolerance for residual


 -snes_monitor     # for displaying errors at each Newton iteration to monitor convergence
 -snes_lag_jacobian 1 # -2 to recompute the Jacobian at each Newton step or 1 to do it 
                      # only once at the beginning of the step and use it of later steps
\endcode

It is also possible to control at which time steps we should produce out put values, 
for example every 2 or 4 time steps. to do that the user should go to the 
\b UnsaturatedFlowOperators.hpp and change the parameter \b save_every_nth_step to a desired value.

\section execution Executing the code
We then execute the code using the command

\code{.bash}
time mpirun -np 1 unsatu2dFlow_prob -file_name mesh.h5m 2>&1 | tee log
\endcode
Here \b tee \b log is used to out put a log file to analyse the result in other suitable platforms for ploting 
such as gnuplot, and the command line option \b -np 1} refers to the number of processors in running the code, 
here in this case is just $1$. To use more than one processor the user should first partition 
the mesh \b paper\_network.h5m, for example, the following command

\code{.bash}
../../tools/mofem_part -my_file paper_network.cub -output_file paper_network.h5m -my_nparts 4 -dim 2 -adj_dim 1
\endcode
partition the mesh for 4 processors and output with the same name.

\section{Converting the output into vtk files}
The output the run are a bunch of \b .h5m files that needs to be converted to 
\b vtk for visualisation in, for example, paraView. This is done by the command

\code{.bash}
../nonlinear_elasticity/do_vtk.sh out_level_*.h5m
\endcode