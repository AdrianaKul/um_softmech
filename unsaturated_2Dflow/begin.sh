#!/bin/bash

. /software/mofem_install/spack/share/spack/setup-env.sh

spack view --verbose symlink -i um_softmech mofem-softmech

ln -s um_softmech/softmech/unsaturated_2Dflow/


export PATH=$HOME/um_softmech/bin:/software/mofem_install/gmsh-4.5.2-Linux64/bin:/software/mofem_install/ParaView-5.8.0-RC2-MPI-Linux-Python3.7-64bit/bin:$PATH
