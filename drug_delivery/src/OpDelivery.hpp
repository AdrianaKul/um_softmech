#ifndef __OPDELIVERY_HPP__
#define __OPDELIVERY_HPP__

#include <stdlib.h>
#include <Python/Python.h>
#include "pyhelper.hpp"
#include <BasicFiniteElements.hpp>
// #include <boost/python.hpp>

namespace SimpleADDAnalyis {

using FaceEle = MoFEM::FaceElementForcesAndSourcesCoreSwitch<
    FaceElementForcesAndSourcesCore::NO_HO_GEOMETRY |
    FaceElementForcesAndSourcesCore::NO_CONTRAVARIANT_TRANSFORM_HDIV |
    FaceElementForcesAndSourcesCore::NO_COVARIANT_TRANSFORM_HCURL>;

using EdgeEle = MoFEM::EdgeElementForcesAndSourcesCoreSwitch<
    EdgeElementForcesAndSourcesCore::NO_HO_GEOMETRY |
    EdgeElementForcesAndSourcesCore::NO_COVARIANT_TRANSFORM_HCURL>;

using OpFaceEle = FaceEle::UserDataOperator;
using OpEdgeEle = EdgeEle::UserDataOperator;

using EntData = DataForcesAndSourcesCore::EntData;

using PostProc = PostProcFaceOnRefinedMesh;



int save_every_nth_step = 1;
double theta = 0.5;


FTensor::Index<'i', 3> i;

struct BlockData {
  int iD;
  double radialDiffuse;
  double axialDiffuse;
  double oN_ks;
  double oFf_ks;
  double mAx_bs;
  double initVal;

  int nFields;
  int dIm;
  std::vector<std::string> fieldNames;
  Range eNts;

  BlockData()
      : radialDiffuse(-1), axialDiffuse(-1), oN_ks(-1), oFf_ks(-1), mAx_bs(-1),
        nFields(-1), dIm(-1), initVal(0) {}
};

struct CommonData {
  MatrixDouble jAc;
  MatrixDouble invJac;

  boost::shared_ptr<MatrixDouble> locMassMatPtr;
  MatrixDouble locInvMassMat;

  MatrixDouble ctrnGrad;

  boost::shared_ptr<VectorDouble> auxValPtr;
  boost::shared_ptr<VectorDouble> auxDotPtr;

  boost::shared_ptr<VectorDouble> ctrnSourcePtr;

  boost::shared_ptr<VectorDouble> ctrnValPtr;
  boost::shared_ptr<VectorDouble> ctrnDotPtr;
  boost::shared_ptr<MatrixDouble> ctrnFluxPtr;
  boost::shared_ptr<VectorDouble> ctrnFluxDivPtr;

  boost::shared_ptr<MatrixDouble> ctrnGradPtr;

  boost::shared_ptr<VectorDouble> axiSymmPtr;

  CommonData() {
    jAc.resize(3, 3, false);
    invJac.resize(3, 3, false);

    auxValPtr = boost::shared_ptr<VectorDouble>(new VectorDouble());
    auxDotPtr =  boost::shared_ptr<VectorDouble>(new VectorDouble());

    ctrnSourcePtr = boost::shared_ptr<VectorDouble>(new VectorDouble());
    locMassMatPtr = boost::shared_ptr<MatrixDouble>(new MatrixDouble());

    ctrnValPtr = boost::shared_ptr<VectorDouble>(new VectorDouble());
    ctrnDotPtr = boost::shared_ptr<VectorDouble>(new VectorDouble());

    ctrnFluxPtr = boost::shared_ptr<MatrixDouble>(new MatrixDouble());
    ctrnFluxDivPtr = boost::shared_ptr<VectorDouble>(new VectorDouble());

    ctrnGradPtr = boost::shared_ptr<MatrixDouble>(new MatrixDouble());

    axiSymmPtr = boost::shared_ptr<VectorDouble>(new VectorDouble());
  }
};

struct ComputeAuxDot {


  double operator()(double ctrn_val, double aux_val) {

    CPyObject pName;
    CPyObject pModule;
    CPyObject pFunc;
    CPyObject pValue;
    CPyObject pArgs;

    pName = PyUnicode_FromString((char *)"rhsFunc");
    pModule = PyImport_Import(pName);
    pFunc = PyObject_GetAttrString(pModule, (char *)"rhs_func");
    pArgs = PyTuple_New(2);

    PyTuple_SetItem(pArgs, 0, PyFloat_FromDouble(ctrn_val));
    PyTuple_SetItem(pArgs, 1, PyFloat_FromDouble(aux_val));


    pValue = PyObject_CallObject(pFunc, pArgs);

    return PyFloat_AsDouble(pValue);
  }

  ComputeAuxDot(){}
};

struct ComputeAuxVal {
  ComputeAuxDot rhs;
  ComputeAuxVal(){}

  double operator()(double ctrn_val_old, double aux_val_old, double dt,
                    double th) {

    double aux_val = aux_val_old;

    const double k1 = rhs(ctrn_val_old, aux_val);
    aux_val = aux_val_old + 0.5 * dt * k1;
    const double k2 = rhs(ctrn_val_old, aux_val);
    aux_val = aux_val_old + 0.5 * dt * k2;
    const double k3 = rhs(ctrn_val_old, aux_val);
    aux_val += aux_val_old + dt * k3;
    const double k4 = rhs(ctrn_val_old, aux_val);


    return aux_val_old + dt / 6.0 * (k1 + 2 * k2 + 2 * k3 + k4);
  }
};

struct OpJacForAxisymmetric
    : public FaceElementForcesAndSourcesCoreBase::UserDataOperator {
  OpJacForAxisymmetric(MatrixDouble &jac)
      : FaceElementForcesAndSourcesCoreBase::UserDataOperator(NOSPACE),
        jAc(jac) {}

  MoFEMErrorCode doWork(int side, EntityType type,
                        DataForcesAndSourcesCore::EntData &data) {
    MoFEMFunctionBegin;
    size_t nb_guass_pts = getGaussPts().size2();
    auto t_coords = getFTensor1CoordsAtGaussPts();

    auto t_jac = getFaceJac(jAc, FTensor::Number<3>());
    for(size_t gg = 0; gg != nb_guass_pts; ++gg, ++t_jac){
      t_jac(2, 0) = t_coords(0) * t_jac(2, 0);
      t_jac(2, 1) = t_coords(0) * t_jac(2, 1);
      t_jac(2, 2) = t_coords(0) * t_jac(2, 2);
    }

    MoFEMFunctionReturn(0);
  }

  protected:
    MatrixDouble &jAc;
};
struct OpInvJacForAxisymmetric
    : public FaceElementForcesAndSourcesCoreBase::UserDataOperator {
  OpInvJacForAxisymmetric(MatrixDouble &inv_jac)
      : FaceElementForcesAndSourcesCoreBase::UserDataOperator(NOSPACE),
        invJac(inv_jac) {}
  MoFEMErrorCode doWork(int side, EntityType type,
                        DataForcesAndSourcesCore::EntData &data) {
    MoFEMFunctionBegin;
    size_t nb_guass_pts = getGaussPts().size2();
    auto t_coords = getFTensor1CoordsAtGaussPts();

    auto t_inv_jac = getFaceJac(invJac, FTensor::Number<3>());
    for (size_t gg = 0; gg != nb_guass_pts; ++gg, ++t_inv_jac) {
      double r = t_coords(0);
      t_inv_jac(0, 2) = 1.0 / r * t_inv_jac(0, 2);
      t_inv_jac(1, 2) = 1.0 / r * t_inv_jac(1, 2);
      t_inv_jac(2, 2) = 1.0 / r * t_inv_jac(2, 2);
    }

    MoFEMFunctionReturn(0);
  }

  protected:
    MatrixDouble &invJac;
};

struct OpSetAxisymmPiolaTransformFace
    : public FaceElementForcesAndSourcesCoreBase::UserDataOperator {

  OpSetAxisymmPiolaTransformFace(MatrixDouble &jac)
      : FaceElementForcesAndSourcesCoreBase::UserDataOperator(HCURL), jAc(jac) {
  }

  MoFEMErrorCode doWork(int side, EntityType type,
                        DataForcesAndSourcesCore::EntData &data);

protected:
  MatrixDouble &jAc;
  MatrixDouble piolaN;
  MatrixDouble piolaDiffN;
};

MoFEMErrorCode OpSetAxisymmPiolaTransformFace::doWork(
    int side, EntityType type, DataForcesAndSourcesCore::EntData &data) {

  MoFEMFunctionBegin;

  if (type != MBEDGE && type != MBTRI && type != MBQUAD)
    MoFEMFunctionReturnHot(0);

  FTensor::Index<'i', 2> i;
  FTensor::Index<'j', 2> j;
  FTensor::Index<'k', 2> k;

  for (int b = AINSWORTH_LEGENDRE_BASE; b != LASTBASE; b++) {

    FieldApproximationBase base = static_cast<FieldApproximationBase>(b);

    const size_t nb_base_functions = data.getN(base).size2() / 3;
    if (nb_base_functions) {

      const size_t nb_gauss_pts = data.getN(base).size1();
      piolaN.resize(nb_gauss_pts, data.getN(base).size2(), false);
      if (data.getN(base).size2() > 0) {
        auto t_n = data.getFTensor1N<3>(base);
        double *t_transformed_n_ptr = &*piolaN.data().begin();
        FTensor::Tensor1<FTensor::PackPtr<double *, 3>, 3> t_transformed_n(
            t_transformed_n_ptr, // HVEC0
            &t_transformed_n_ptr[HVEC1], &t_transformed_n_ptr[HVEC2]);
        auto t_jac = getFaceJac(jAc, FTensor::Number<2>());
        auto t_coords = getFTensor1CoordsAtGaussPts();
        for (unsigned int gg = 0; gg != nb_gauss_pts; ++gg, ++t_jac, ++t_coords) {
          double  r = t_coords(0);
          double det;
          CHKERR determinantTensor2by2(t_jac, det);
          for (unsigned int bb = 0; bb != nb_base_functions; ++bb) {
            t_transformed_n(i) = t_jac(i, k) * t_n(k) / (r *  det);
            ++t_n;
            ++t_transformed_n;
          }
        }
        data.getN(base).data().swap(piolaN.data());
      }

      piolaDiffN.resize(nb_gauss_pts, data.getDiffN(base).size2(), false);
      if (data.getDiffN(base).size2() > 0) {
        auto t_diff_n = data.getFTensor2DiffN<3, 2>(base);
        double *t_transformed_diff_n_ptr = &*piolaDiffN.data().begin();
        FTensor::Tensor2<FTensor::PackPtr<double *, 6>, 3, 2>
            t_transformed_diff_n(t_transformed_diff_n_ptr,
                                 &t_transformed_diff_n_ptr[HVEC0_1],

                                 &t_transformed_diff_n_ptr[HVEC1_0],
                                 &t_transformed_diff_n_ptr[HVEC1_1],

                                 &t_transformed_diff_n_ptr[HVEC2_0],
                                 &t_transformed_diff_n_ptr[HVEC2_1]);
        auto t_jac = getFaceJac(jAc, FTensor::Number<2>());
        auto t_coords = getFTensor1CoordsAtGaussPts();
        for (unsigned int gg = 0; gg != nb_gauss_pts; ++gg, ++t_jac, ++t_coords) {
          double r = t_coords(0);
          double det;
          CHKERR determinantTensor2by2(t_jac, det);
          for (unsigned int bb = 0; bb != nb_base_functions; ++bb) {
            t_transformed_diff_n(i, j) = t_diff_n(i, j) / (det * r);
            ++t_diff_n;
            ++t_transformed_diff_n;
          }
        }
        data.getDiffN(base).data().swap(piolaDiffN.data());
      }
    }
  }

  MoFEMFunctionReturn(0);
}
struct OpEssentialBC : public OpEdgeEle {
  OpEssentialBC(const std::string &flux_field, 
                Range             &bd_ents,
                double            &ess_val)
      : OpEdgeEle(flux_field, OpEdgeEle::OPROW)
      , essential_bd_ents(bd_ents)
      , essVal(ess_val) {}

  Range &essential_bd_ents;
  double &essVal;
  MoFEMErrorCode doWork(int side, EntityType type, EntData &data) {
    MoFEMFunctionBegin;
    int nb_dofs = data.getIndices().size();
    if (nb_dofs) {
      EntityHandle fe_ent = getFEEntityHandle();
      bool is_essential =
          (essential_bd_ents.find(fe_ent) != essential_bd_ents.end());
      if (is_essential) {
        int nb_gauss_pts = getGaussPts().size2();
        int size2 = data.getN().size2();
        if (3 * nb_dofs != static_cast<int>(data.getN().size2()))
          SETERRQ(PETSC_COMM_WORLD, MOFEM_DATA_INCONSISTENCY,
                  "wrong number of dofs");
        nN.resize(nb_dofs, nb_dofs, false);
        nF.resize(nb_dofs, false);
        nN.clear();
        nF.clear();

        auto t_row_tau = data.getFTensor1N<3>();

        auto dir = getDirection();
        double len = sqrt(dir[0] * dir[0] + dir[1] * dir[1] + dir[2] * dir[2]);

        FTensor::Tensor1<double, 3> t_normal(-dir[1] / len, dir[0] / len,
                                             dir[2] / len);

        auto t_w = getFTensor0IntegrationWeight();
        const double vol = getMeasure();
        auto t_coords = getFTensor1CoordsAtGaussPts();

        for (int gg = 0; gg < nb_gauss_pts; gg++) {
          double r = t_coords(0);
          const double a = t_w * vol * r;
          for (int rr = 0; rr != nb_dofs; rr++) {
            auto t_col_tau = data.getFTensor1N<3>(gg, 0);
            nF[rr] += a * essVal * t_row_tau(i) * t_normal(i);
            for (int cc = 0; cc != nb_dofs; cc++) {
              nN(rr, cc) += a * (t_row_tau(i) * t_normal(i)) *
                            (t_col_tau(i) * t_normal(i));
              ++t_col_tau;
            }
            ++t_row_tau;
          }
          ++t_w;
          ++t_coords;
        }

        cholesky_decompose(nN);
        cholesky_solve(nN, nF, ublas::lower());

        for (auto &dof : data.getFieldDofs()) {
          dof->getFieldData() = nF[dof->getEntDofIdx()];
        }
      }
    }
    MoFEMFunctionReturn(0);
  }

private:
  MatrixDouble nN;
  VectorDouble nF;
  
};

// Assembly of system mass matrix
// //***********************************************

// Mass matrix corresponding to the flux equation.
// 01. Note that it is an identity matrix
struct OpInitialMass : public OpFaceEle {
  OpInitialMass(const std::string &mass_field, 
                Range             &inner_surface,
                double            &init_val, 
                MoFEM::Interface  &m_field)
      : OpFaceEle(mass_field, OpFaceEle::OPROW)
      , innerSurface(inner_surface)
      , initVal(init_val)
      , mField(m_field) {}

  MoFEM::Interface &mField;

  MatrixDouble nN;
  VectorDouble nF;
  double &initVal;
  Range &innerSurface;
  MoFEMErrorCode doWork(int side, EntityType type, EntData &data) {
    MoFEMFunctionBegin;
    int nb_dofs = data.getFieldData().size();
    if (nb_dofs) {
      EntityHandle fe_ent = getFEEntityHandle();
      bool is_inner_side = (innerSurface.find(fe_ent) != innerSurface.end());
      if (is_inner_side) {
        // const int nb_dofs = get_nb_dofs(data, thMaxOrder, mField);
        int nb_gauss_pts = getGaussPts().size2();

        nN.resize(nb_dofs, nb_dofs, false);
        nF.resize(nb_dofs, false);
        nN.clear();
        nF.clear();

        // int ii = nb_dofs;
        // for (; ii < nb_dofs; ++ii)
        //   nN(ii, ii) = 1.0;
        auto t_coords = getFTensor1CoordsAtGaussPts();
        auto t_w = getFTensor0IntegrationWeight();
        const double vol = getMeasure();

        for (int gg = 0; gg < nb_gauss_pts; gg++) {
          double  r = t_coords(0);
          auto t_row_mass = data.getFTensor0N(gg, 0);
          const double a = t_w * vol * r;
          for (int rr = 0; rr != nb_dofs; rr++) {
            auto t_col_mass = data.getFTensor0N(gg, 0);
            nF[rr] += a * initVal * t_row_mass;
            for (int cc = 0; cc != nb_dofs; cc++) {
              nN(rr, cc) += a * t_row_mass * t_col_mass;
              ++t_col_mass;
            }
            ++t_row_mass;
          }
          ++t_w;
          ++t_coords;
        }

        cholesky_decompose(nN);
        cholesky_solve(nN, nF, ublas::lower());

        for (auto &dof : data.getFieldDofs()) {
          dof->getFieldData() = nF[dof->getEntDofIdx()];

          // this is only to check
          // data.getFieldData()[dof->getEntDofIdx()] = nF[dof->getEntDofIdx()];
        }
      }
    }
    MoFEMFunctionReturn(0);
  }
};

struct OpSolveRecovery : public OpFaceEle {

  typedef boost::function<double(double,
                                 double,
                                 const double,
                                 const double)>
                                 AuxFunction;

  OpSolveRecovery(const std::string             &mass_field,
                  boost::shared_ptr<CommonData> &common_data,
                  BlockData                     &block_data,
                  AuxFunction                   aux_func,
                  MoFEM::Interface              &m_field)
      : OpFaceEle(mass_field, OpFaceEle::OPROW)
      , commonData(common_data)
      , auxFunc(aux_func)
      , mField(m_field)
      , blockData(block_data) {}

  boost::shared_ptr<CommonData> commonData;

  MoFEM::Interface &mField;
  AuxFunction      auxFunc;
  BlockData        &blockData;
  MatrixDouble nN;
  VectorDouble nF;
  double initVal;
  MoFEMErrorCode doWork(int side, EntityType type, EntData &data) {
    MoFEMFunctionBegin;
    int nb_dofs = data.getFieldData().size();

    if (nb_dofs) {
      int nb_gauss_pts = getGaussPts().size2();
      // const int nb_dofs = get_nb_dofs(data, thMaxOrder, mField);

      EntityHandle entity = getFEEntityHandle();
      if (blockData.eNts.find(entity) != blockData.eNts.end()) {
        auto t_val_u = getFTensor0FromVec(*commonData->ctrnValPtr);
        auto t_val_v = getFTensor0FromVec(*commonData->auxValPtr);

        nN.resize(nb_dofs, nb_dofs, false);
        nF.resize(nb_dofs, false);
        nN.clear();
        nF.clear();

        // int ii = nb_dofs;
        // for (; ii < nb_dofs; ++ii)
        //   nN(ii, ii) = 1.0;

        double dt;
        CHKERR TSGetTimeStep(getFEMethod()->ts, &dt);

        auto t_w = getFTensor0IntegrationWeight();
        const double vol = getMeasure();

        auto t_coords = getFTensor1CoordsAtGaussPts();

        for (int gg = 0; gg < nb_gauss_pts; gg++) {
          double  r = t_coords(0);
          auto t_row_mass = data.getFTensor0N(gg, 0);
          const double a = t_w * vol * r;
          // double val_u = t_val_u;
          // double val_v = t_val_v;
          const double vn = auxFunc(t_val_u, t_val_v, dt, theta);
          // cout << "aux_val : " << vn << endl;
          for (int rr = 0; rr != nb_dofs; rr++) {
            auto t_col_mass = data.getFTensor0N(gg, 0);
            nF[rr] += a * vn * t_row_mass;
            for (int cc = 0; cc != nb_dofs; cc++) {
              nN(rr, cc) += a * t_row_mass * t_col_mass;
              ++t_col_mass;
            }
            ++t_row_mass;
          }
          ++t_w;
          ++t_val_u;
          ++t_val_v;
          ++t_coords;
        }
        cholesky_decompose(nN);
        cholesky_solve(nN, nF, ublas::lower());

        // cout << "nN : " << nN << endl;

        for (auto &dof : data.getFieldDofs()) {
          dof->getFieldData() = nF[dof->getEntDofIdx()];
          // cout << nF[dof->getEntDofIdx()] << endl;
        }

          // this is only to check
          // data.getFieldData()[dof->getEntDofIdx()] = nF[dof->getEntDofIdx()];
      }
      }
    MoFEMFunctionReturn(0);
  }
};

// Assembly of RHS for explicit (slow)
// part//**************************************

// 2. RHS for explicit part of the mass balance equation
struct OpAssembleSlowRhsV : OpFaceEle // R_V
{
  typedef boost::function<double(double,
                                 double)>
                                 DotAuxFunction;
  OpAssembleSlowRhsV(std::string                   mass_field,
                     boost::shared_ptr<CommonData> &common_data,
                     BlockData                     &block_data, 
                     DotAuxFunction                dot_aux_func,
                     MoFEM::Interface              &m_field)
      : OpFaceEle(mass_field, OpFaceEle::OPROW)
      , commonData(common_data)
      , dotAuxFunc(dot_aux_func)
      , blockData(block_data)
      , mField(m_field) {}

  DotAuxFunction   dotAuxFunc;
  BlockData        &blockData;
  MoFEM::Interface &mField;
  MoFEMErrorCode doWork(int side, EntityType type, EntData &data) {
    MoFEMFunctionBegin;
    const int nb_dofs = data.getIndices().size();
    if (nb_dofs) {
      // const int nb_dofs = get_nb_dofs(data, thMaxOrder, mField);

      

      // auto t_coords = getFTensor1CoordsAtGaussPts();

      // double dt;
      // CHKERR TSGetTimeStep(getFEMethod()->ts, &dt);


        EntityHandle entity = getFEEntityHandle();
        if (blockData.eNts.find(entity) != blockData.eNts.end()) {
          auto t_u_value = getFTensor0FromVec(*commonData->ctrnValPtr);
          auto t_v_value = getFTensor0FromVec(*commonData->auxValPtr);

          vecF.resize(nb_dofs, false);
          mat.resize(nb_dofs, nb_dofs, false);
          vecF.clear();
          mat.clear();

          int ii = nb_dofs;
          for (; ii < nb_dofs; ++ii)
            mat(ii, ii) = 1.0;

          const int nb_integration_pts = getGaussPts().size2();

          auto t_w = getFTensor0IntegrationWeight();
          const double vol = getMeasure();

          auto t_coords = getFTensor1CoordsAtGaussPts();

          for (int gg = 0; gg != nb_integration_pts; ++gg) {
            double r = t_coords(0);
            auto t_row_v_base = data.getFTensor0N(gg, 0);
            const double a = vol * t_w * r;
            // double val_u = t_u_value;
            // double val_v = t_v_value;
            const double dot_aux_val = dotAuxFunc(t_u_value, t_v_value);

            for (int rr = 0; rr != nb_dofs; ++rr) {

              auto t_col_v_base = data.getFTensor0N(gg, 0);
              vecF[rr] += a * dot_aux_val * t_row_v_base;

              for (int cc = 0; cc != nb_dofs; ++cc) {
                mat(rr, cc) += a * t_row_v_base * t_col_v_base;
                ++t_col_v_base;
              }
              ++t_row_v_base;
            }
            ++t_u_value;
            ++t_v_value;
            ++t_w;
            ++t_coords;
          }
          cholesky_decompose(mat);
          cholesky_solve(mat, vecF, ublas::lower());

          CHKERR VecSetOption(getFEMethod()->ts_F, VEC_IGNORE_NEGATIVE_INDICES,
                              PETSC_TRUE);
          CHKERR VecSetValues(getFEMethod()->ts_F, data, &*vecF.begin(),
                              ADD_VALUES);
        }
      }

      // const double ct = getFEMethod()->ts_t - 0.01;
      // auto t_coords = getFTensor1CoordsAtGaussPts();


    MoFEMFunctionReturn(0);
  }

private:
  boost::shared_ptr<CommonData> commonData;
  VectorDouble vecF;
  MatrixDouble mat;

  FTensor::Number<0> NX;
  FTensor::Number<1> NY;
  FTensor::Number<2> NZ;
};

template <int dim>
struct OpAssembleStiffRhsTau : OpFaceEle //  F_tau_1
{
  OpAssembleStiffRhsTau(std::string                   flux_field,
                        boost::shared_ptr<CommonData> &common_data,
                        std::map<std::string, BlockData>    &block_map,
                        MoFEM::Interface              &m_field)
      : OpFaceEle(flux_field, OpFaceEle::OPROW)
      , commonData(common_data)
      , setOfBlock(block_map)
      , mField(m_field) {}

  MoFEM::Interface &mField;
  MoFEMErrorCode doWork(int side, EntityType type, EntData &data) {
    MoFEMFunctionBegin;

    const int nb_dofs = data.getIndices().size();
    if (nb_dofs) {

      auto find_block_data = [&]() {
        EntityHandle fe_ent = getFEEntityHandle();
        BlockData *block_raw_ptr = nullptr;
        for (auto &m : setOfBlock) {
          if (m.second.eNts.find(fe_ent) != m.second.eNts.end())
          {
            block_raw_ptr = &m.second;
            break;
          }
        }
        return block_raw_ptr;
      };

      auto block_data_ptr = find_block_data();
      if (!block_data_ptr)
        SETERRQ(PETSC_COMM_SELF, MOFEM_DATA_INCONSISTENCY, "Block not found");
      auto &block_data = *block_data_ptr;

      // const int nb_dofs = get_nb_dofs(data, thMaxOrder, mField);

      vecF.resize(nb_dofs, false);
      vecF.clear();

      const int nb_integration_pts = getGaussPts().size2();
      auto t_flux_value = getFTensor1FromMat<3>(*commonData->ctrnFluxPtr);
      auto t_mass_value = getFTensor0FromVec(*commonData->ctrnValPtr);

      auto t_w = getFTensor0IntegrationWeight();
      const double vol = getMeasure();

      auto t_coords = getFTensor1CoordsAtGaussPts();

      const double K_inv = 1. / block_data.drugDiffusivity;

      for (int gg = 0; gg < nb_integration_pts; ++gg) {

        double r = t_coords(0);

        auto t_tau_base = data.getFTensor1N<3>(gg, 0);

        auto t_tau_grad = data.getFTensor2DiffN<3, 2>(gg, 0);

        
        const double a = vol * t_w * r;
        for (int rr = 0; rr < nb_dofs; ++rr) {
          double div_base = t_tau_grad(0, 0) + t_tau_grad(1, 1);
          vecF[rr] += (K_inv * t_tau_base(i) * t_flux_value(i) -
                       div_base * t_mass_value) *
                      a;
          ++t_tau_base;
          ++t_tau_grad;
        }
        ++t_flux_value;
        ++t_mass_value;
        ++t_w;
        ++t_coords;
      }
      CHKERR VecSetOption(getFEMethod()->ts_F, VEC_IGNORE_NEGATIVE_INDICES,
                          PETSC_TRUE);
      CHKERR VecSetValues(getFEMethod()->ts_F, data, &*vecF.begin(),
                          ADD_VALUES);
    }
    MoFEMFunctionReturn(0);
  }

private:
  boost::shared_ptr<CommonData> commonData;
  VectorDouble vecF;
  std::map<std::string, BlockData> setOfBlock;
};
// 4. Assembly of F_v
template <int dim>
struct OpAssembleStiffRhsV : OpFaceEle // F_V
{
  OpAssembleStiffRhsV(std::string                   flux_field,
                      boost::shared_ptr<CommonData> &common_data,
                      std::map<std::string, BlockData>    &block_map, 
                      MoFEM::Interface              &m_field)
      : OpFaceEle(flux_field, OpFaceEle::OPROW)
      , commonData(common_data)
      , setOfBlock(block_map)
      , mField(m_field) {}

  MoFEM::Interface &mField;
  MoFEMErrorCode doWork(int side, EntityType type, EntData &data) {
    MoFEMFunctionBegin;
    const int nb_dofs = data.getIndices().size();
    // cerr << "In StiffRhsV ..." << endl;
    if (nb_dofs) {
      // const int nb_dofs = get_nb_dofs(data, thMaxOrder, mField);

      // auto find_block_data = [&]() {
      //   EntityHandle fe_ent = getFEEntityHandle();
      //   BlockData *block_raw_ptr = nullptr;
      //   for (auto &m : setOfBlock) {
      //     if (m.second.eNts.find(fe_ent) != m.second.eNts.end())
      //     {
      //       block_raw_ptr = &m.second;
      //       break;
      //     }
      //   }
      //   return block_raw_ptr;
      // };

      // auto block_data_ptr = find_block_data();
      // if (!block_data_ptr)
      //   SETERRQ(PETSC_COMM_SELF, MOFEM_DATA_INCONSISTENCY, "Block not
      //   found");
      // auto &block_data = *block_data_ptr;

      vecF.resize(nb_dofs, false);
      vecF.clear();
      const int nb_integration_pts = getGaussPts().size2();

      auto t_mass_dot = getFTensor0FromVec(*commonData->ctrnDotPtr);
      auto t_flux_div = getFTensor0FromVec(*commonData->ctrnFluxDivPtr);

      auto t_w = getFTensor0IntegrationWeight();
      const double vol = getMeasure();

      auto t_coords = getFTensor1CoordsAtGaussPts();

      for (int gg = 0; gg < nb_integration_pts; ++gg) {
        double r = t_coords(0);

        auto t_row_v_base = data.getFTensor0N(gg, 0);

        const double a = vol * t_w * r;



        for (int rr = 0; rr < nb_dofs; ++rr) {
          vecF[rr] += (t_row_v_base *
                       (t_mass_dot + t_flux_div )) *
                      a;
          ++t_row_v_base;
        }
        ++t_mass_dot;
        ++t_flux_div;
        ++t_w;
        ++t_coords;
      }
      CHKERR VecSetOption(getFEMethod()->ts_F, VEC_IGNORE_NEGATIVE_INDICES,
                          PETSC_TRUE);
      CHKERR VecSetValues(getFEMethod()->ts_F, data, &*vecF.begin(),
                          ADD_VALUES);
    }
    MoFEMFunctionReturn(0);
  }

private:
  boost::shared_ptr<CommonData> commonData;
  VectorDouble vecF;
  std::map<std::string, BlockData> setOfBlock;

  FTensor::Number<0> NX;
  FTensor::Number<1> NY;
};

// Tangent operator
// //**********************************************
// 7. Tangent assembly for F_tautau excluding the essential boundary condition
template <int dim>
struct OpAssembleLhsTauTau : OpFaceEle // A_TauTau_1
{
  OpAssembleLhsTauTau(std::string                   flux_field,
                      std::map<std::string, BlockData>    &block_map,
                      MoFEM::Interface              &m_field)
      : OpFaceEle(flux_field, flux_field, OpFaceEle::OPROWCOL)
      ,  setOfBlock(block_map)
      , mField(m_field) 
      {
        sYmm = false;
      }

  MoFEM::Interface &mField;

  MoFEMErrorCode doWork(int row_side, int col_side, EntityType row_type,
                        EntityType col_type, EntData &row_data,
                        EntData &col_data) {
    MoFEMFunctionBegin;
    const int nb_row_dofs = row_data.getIndices().size();
    const int nb_col_dofs = col_data.getIndices().size();

    if (nb_row_dofs && nb_col_dofs) {
      auto find_block_data = [&]() {
        EntityHandle fe_ent = getFEEntityHandle();
        BlockData *block_raw_ptr = nullptr;
        for (auto &m : setOfBlock) {
          if (m.second.eNts.find(fe_ent) != m.second.eNts.end())
          {
            block_raw_ptr = &m.second;
            break;
          }
        }
        return block_raw_ptr;
      };

      auto block_data_ptr = find_block_data();
      if (!block_data_ptr)
        SETERRQ(PETSC_COMM_SELF, MOFEM_DATA_INCONSISTENCY, "Block not found");
      auto &block_data = *block_data_ptr;

      // const int nb_row_dofs = get_nb_dofs(row_data, thMaxOrder, mField);
      // const int nb_col_dofs = get_nb_dofs(col_data, thMaxOrder, mField);

      mat.resize(nb_row_dofs, nb_col_dofs, false);
      mat.clear();

      // const bool on_block_diag = row_side == col_side && row_type == col_type;
      // if (on_block_diag) {
      //   int ii = nb_row_dofs;
      //   for (; ii < nn_row_dofs; ++ii)
      //     mat(ii, ii) = 1.0;
      // }

      const int nb_integration_pts = getGaussPts().size2();

      auto t_w = getFTensor0IntegrationWeight();
      const double vol = getMeasure();
      auto t_coords = getFTensor1CoordsAtGaussPts();

      const double K_inv = 1. / block_data.drugDiffusivity;

      for (int gg = 0; gg != nb_integration_pts; ++gg) {
        double r = t_coords(0);
        auto t_row_tau_base = row_data.getFTensor1N<3>(gg, 0);
        const double a = vol * t_w * r;
        
        for (int rr = 0; rr != nb_row_dofs; ++rr) {
          auto t_col_tau_base = col_data.getFTensor1N<3>(gg, 0);
          for (int cc = 0; cc != nb_col_dofs; ++cc) {
            mat(rr, cc) += (K_inv * t_row_tau_base(i) * t_col_tau_base(i)) * a;
            ++t_col_tau_base;
          }
          ++t_row_tau_base;
        }
        ++t_w;
        ++t_coords;
      }
      CHKERR MatSetValues(getFEMethod()->ts_B, row_data, col_data, &mat(0, 0),
                          ADD_VALUES);
    }
    MoFEMFunctionReturn(0);
  }

private:
  MatrixDouble mat, transMat;
  std::map<std::string, BlockData> setOfBlock;
};

// 9. Assembly of tangent for F_tau_v excluding the essential bc
template <int dim>
struct OpAssembleLhsTauV : OpFaceEle // E_TauV
{
  OpAssembleLhsTauV(std::string                   flux_field, 
                    std::string                   mass_field,
                    boost::shared_ptr<CommonData> &common_data,
                    MoFEM::Interface              &m_field)
      : OpFaceEle(flux_field, mass_field, OpFaceEle::OPROWCOL)
      , commonData(common_data)
      , mField(m_field) 
      {
        sYmm = false;
      }
  boost::shared_ptr<CommonData> commonData;    
  MoFEM::Interface &mField;

  MoFEMErrorCode doWork(int row_side, int col_side, EntityType row_type,
                        EntityType col_type, EntData &row_data,
                        EntData &col_data) {
    MoFEMFunctionBegin;
    const int nb_row_dofs = row_data.getIndices().size();
    const int nb_col_dofs = col_data.getIndices().size();

    if (nb_row_dofs && nb_col_dofs) {

      // const int nb_row_dofs = get_nb_dofs(row_data, thMaxOrder, mField);
      // const int nb_col_dofs = get_nb_dofs(col_data, thMaxOrder, mField);
      mat.resize(nb_row_dofs, nb_col_dofs, false);
      mat.clear();
      const int nb_integration_pts = getGaussPts().size2();
      auto t_w = getFTensor0IntegrationWeight();

      const double vol = getMeasure();

      auto t_coords = getFTensor1CoordsAtGaussPts();

      for (int gg = 0; gg != nb_integration_pts; ++gg) {
        double r = t_coords(0);
        auto t_row_tau_base = row_data.getFTensor1N<3>(gg, 0);

        auto t_row_tau_grad = row_data.getFTensor2DiffN<3, 2>(gg, 0);
        const double a = vol * t_w * r;

        for (int rr = 0; rr != nb_row_dofs; ++rr) {
          auto t_col_v_base = col_data.getFTensor0N(gg, 0);
          for (int cc = 0; cc != nb_col_dofs; ++cc) {
            double div_row_base = t_row_tau_grad(0, 0) + t_row_tau_grad(1, 1);
            mat(rr, cc) += - (div_row_base * t_col_v_base) *  a;
            ++t_col_v_base;
          }
          ++t_row_tau_base;
          ++t_row_tau_grad;
        }
        ++t_w;
        ++t_coords;
      }
      CHKERR MatSetValues(getFEMethod()->ts_B, row_data, col_data, &mat(0, 0),
                          ADD_VALUES);
    }
    MoFEMFunctionReturn(0);
  }

private:
  MatrixDouble mat;
};

// 10. Assembly of tangent for F_v_tau
struct OpAssembleLhsVTau : OpFaceEle // C_VTau
{
  OpAssembleLhsVTau(std::string      mass_field, 
                    std::string      flux_field,
                    MoFEM::Interface &m_field)
      : OpFaceEle(mass_field, flux_field, OpFaceEle::OPROWCOL),
        mField(m_field) {
    sYmm = false;
  }

  MoFEM::Interface &mField;
  MoFEMErrorCode doWork(int row_side, int col_side, EntityType row_type,
                        EntityType col_type, EntData &row_data,
                        EntData &col_data) {
    MoFEMFunctionBegin;
    const int nb_row_dofs = row_data.getIndices().size();
    const int nb_col_dofs = col_data.getIndices().size();

    if (nb_row_dofs && nb_col_dofs) {
      // const int nb_row_dofs = get_nb_dofs(row_data, thMaxOrder, mField);
      // const int nb_col_dofs = get_nb_dofs(col_data, thMaxOrder, mField);

      mat.resize(nb_row_dofs, nb_col_dofs, false);
      mat.clear();
      const int nb_integration_pts = getGaussPts().size2();
      auto t_w = getFTensor0IntegrationWeight();

      const double vol = getMeasure();
      auto t_coords = getFTensor1CoordsAtGaussPts();

      for (int gg = 0; gg != nb_integration_pts; ++gg) {
        double r = t_coords(0);
        auto t_row_v_base = row_data.getFTensor0N(gg, 0);
        const double a = vol * t_w * r;
        for (int rr = 0; rr != nb_row_dofs; ++rr) {
          auto t_col_tau_grad = col_data.getFTensor2DiffN<3, 2>(gg, 0);
          for (int cc = 0; cc != nb_col_dofs; ++cc) {
            double div_col_base = t_col_tau_grad(0, 0) + t_col_tau_grad(1, 1);
            mat(rr, cc) += (t_row_v_base * div_col_base) * a;
            ++t_col_tau_grad;
          }
          ++t_row_v_base;
        }
        ++t_w;
        ++t_coords;
      }
      CHKERR MatSetValues(getFEMethod()->ts_B, row_data, col_data, &mat(0, 0),
                          ADD_VALUES);
    }
    MoFEMFunctionReturn(0);
  }

private:
  MatrixDouble mat;
};

// 11. Assembly of tangent for F_v_v
struct OpAssembleLhsVV : OpFaceEle // D
{
  OpAssembleLhsVV(std::string mass_field,
                  MoFEM::Interface &m_field)
      : OpFaceEle(mass_field, mass_field, OpFaceEle::OPROWCOL)
      , mField(m_field) 
      {
          sYmm = true;
      }

  MoFEM::Interface &mField;


  MoFEMErrorCode doWork(int row_side, int col_side, EntityType row_type,
                        EntityType col_type, EntData &row_data,
                        EntData &col_data) {
    MoFEMFunctionBegin;

    const int nb_row_dofs = row_data.getIndices().size();
    const int nb_col_dofs = col_data.getIndices().size();
    if (nb_row_dofs && nb_col_dofs) {
      // const int nb_row_dofs = get_nb_dofs(row_data, thMaxOrder, mField);
      // const int nb_col_dofs = get_nb_dofs(col_data, thMaxOrder, mField);
      mat.resize(nb_row_dofs, nb_col_dofs, false);
      mat.clear();

      // const bool on_block_diag =
      //     ((row_side == col_side) && (row_type == col_type));
      // if (on_block_diag) {
      //   int ii = nb_row_dofs;
      //   for (; ii < nn_row_dofs; ++ii)
      //     mat(ii, ii) = 1.0;
      // }

      const int nb_integration_pts = getGaussPts().size2();

      auto t_w = getFTensor0IntegrationWeight();
      const double ts_a = getFEMethod()->ts_a;
      const double vol = getMeasure();
      auto t_coords = getFTensor1CoordsAtGaussPts();

      for (int gg = 0; gg != nb_integration_pts; ++gg) {
        double r = t_coords(0);
        auto t_row_v_base = row_data.getFTensor0N(gg, 0);
        const double a = vol * t_w * r;
        for (int rr = 0; rr != nb_row_dofs; ++rr) {
          auto t_col_v_base = col_data.getFTensor0N(gg, 0);
          for (int cc = 0; cc != nb_col_dofs; ++cc) {
            mat(rr, cc) += (ts_a * t_row_v_base * t_col_v_base) * a;

            ++t_col_v_base;
          }
          ++t_row_v_base;
        }
        ++t_w;
        ++t_coords;
      }
      CHKERR MatSetValues(getFEMethod()->ts_B, row_data, col_data, &mat(0, 0),
                          ADD_VALUES);
      // if (row_side != col_side || row_type != col_type) {
      //   transMat.resize(nb_col_dofs, nb_row_dofs, false);
      //   noalias(transMat) = trans(mat);
      //   CHKERR MatSetValues(getFEMethod()->ts_B, col_data, row_data,
      //                       &transMat(0, 0), ADD_VALUES);
      // }
    }
    MoFEMFunctionReturn(0);
  }

private:
  MatrixDouble mat, transMat;
};


struct Monitor : public FEMethod {  

  MoFEM::Interface &mField;
  Monitor(MPI_Comm                                     &comm, 
          const int                                    &rank, 
          SmartPetscObj<DM>                            &dm,
          SmartPetscObj<TS>                            &ts, 
          boost::shared_ptr<PostProcFaceOnRefinedMesh> &post_proc, 
          MoFEM::Interface                             &m_field)
      : cOmm(comm)
      , rAnk(rank)
      , dM(dm)
      , tS(ts)
      , postProc(post_proc),
        mField(m_field){};
  MoFEMErrorCode preProcess() {
    MoFEMFunctionBegin;
    MoFEMFunctionReturn(0);
  }
  MoFEMErrorCode operator()() { return 0; }
  MoFEMErrorCode postProcess() {
    MoFEMFunctionBegin;
    CHKERR PetscOptionsGetInt(PETSC_NULL, "", "-save_every_nth_step",
                              &save_every_nth_step, PETSC_NULL);
    if (ts_step % save_every_nth_step == 0) {

      CHKERR DMoFEMLoopFiniteElements(dM, "dFE", postProc);
      CHKERR postProc->writeFile(
          "out_level_s" + boost::lexical_cast<std::string>(ts_step) + ".h5m");
    }


    MoFEMFunctionReturn(0);
  }

private:
  SmartPetscObj<DM> dM;
  SmartPetscObj<TS> tS;
  boost::shared_ptr<PostProcFaceOnRefinedMesh> postProc;
  MPI_Comm cOmm;
  const int rAnk;
};

}; // namespace SimpleADDAnalyis

#endif //__OPDELIVERY_HPP__