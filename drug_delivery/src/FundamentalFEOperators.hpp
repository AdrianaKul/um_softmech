#ifndef __FUNDAMENTAL_FE_OPERATORS_HPP__
#define __FUNDAMENTAL_FE_OPERATORS_HPP__

#include <stdlib.h>
#include <BasicFiniteElements.hpp>

namespace FEOperators {
using FaceEle = MoFEM::FaceElementForcesAndSourcesCoreSwitch<
    FaceElementForcesAndSourcesCore::NO_HO_GEOMETRY |
    FaceElementForcesAndSourcesCore::NO_CONTRAVARIANT_TRANSFORM_HDIV |
    FaceElementForcesAndSourcesCore::NO_COVARIANT_TRANSFORM_HCURL>;

using EdgeEle = MoFEM::EdgeElementForcesAndSourcesCoreSwitch<
    EdgeElementForcesAndSourcesCore::NO_HO_GEOMETRY |
    EdgeElementForcesAndSourcesCore::NO_COVARIANT_TRANSFORM_HCURL>;

using OpFaceEle = FaceEle::UserDataOperator;
using OpEdgeEle = EdgeEle::UserDataOperator;

using EntData = DataForcesAndSourcesCore::EntData;

using PostProc = PostProcFaceOnRefinedMesh;

using pFT0_t = FTensor::Tensor0<FTensor::PackPtr<double *, 1>>;

template <int Tensor_Dim>
using pFT1_t = FTensor::Tensor1<double *, Tensor_Dim>;

using pFTGP = FTensor::Tensor1<FTensor::PackPtr<double *, 3>, 3>;

template<int dim>
using FTPackPtr = FTensor::Tensor2<FTensor::PackPtr<double *, 1>, dim, dim>;


template<int DIM1, int DIM2>
using FTSq = FTensor::Tensor2<double, DIM1, DIM2>;

enum DiffOrder {L2_0 = 0, H1_0, H1_1, HDIV_0, HDIV_1};
enum IMEX { IM = 0, EX };

FTensor::Index<'i', 3> i;
FTensor::Index<'j', 3> j;
FTensor::Index<'k', 3> k;


// Begin definition of OpRowColFE for LHS operators
template <DiffOrder row_order, DiffOrder col_order>
struct OpRowColFE : OpFaceEle {
  typedef boost::function<void(MatrixDouble &, MatrixDouble &, const double)> MatrixFunction;

  OpRowColFE(const std::string row_field_name, const std::string col_field_name,
             MatrixFunction mat_fun, Range *ents = NULL);

  // OpRowColFE(const std::string row_field_name, const std::string col_field_name,
  //            FTSq<2, 2> k_inv, Range *ents = NULL);

  OpRowColFE(const std::string row_field_name, const std::string col_field_name,
             Range *ents = NULL);

  MoFEMErrorCode doWork(int row_side, int col_side, EntityType row_type,
                        EntityType col_type, EntData &row_data,
                        EntData &col_data);
private:

  MatrixDouble matrixData;
  FTSq<2, 2> kInv;
  Range *eNts;
  MatrixDouble loc_mat;

  MatrixFunction matFun;

  MoFEMErrorCode assembleLocMatrix(EntData &row_data, EntData &col_data);
  MoFEMErrorCode calculateLocMatrix(EntData &row_data, EntData &col_data);
  };
  // End definition of OpRowColFE for LHS operators

  // Begin implementation of OpRowColFE for LHS operators
template <DiffOrder row_order, DiffOrder col_order>
OpRowColFE<row_order, col_order>::OpRowColFE(
    const std::string row_field_name, const std::string col_field_name,
    MatrixFunction mat_fun, Range *ents)
    : OpFaceEle(row_field_name, col_field_name, OpFaceEle::OPROWCOL),
      matFun(mat_fun), eNts(ents)
    {
      sYmm = false;
    }

// template <DiffOrder row_order, DiffOrder col_order>
// OpRowColFE<row_order, col_order>::OpRowColFE(const std::string row_field_name,
//                                               const std::string col_field_name,
//                                               FTSq<2, 2> k_inv, Range *ents)
//     : OpFaceEle(row_field_name, col_field_name, OpFaceEle::OPROWCOL), kInv(k_inv),
//       eNts(ents)
//     {
//       sYmm = false;
//     }
template<DiffOrder row_order, DiffOrder col_order>
OpRowColFE<row_order, col_order>::OpRowColFE(const std::string row_field_name, const std::string col_field_name,
             Range *ents): OpFaceEle(row_field_name, col_field_name, OpFaceEle::OPROWCOL), eNts(ents)
{
  sYmm = false;
}

template <DiffOrder row_order, DiffOrder col_order>
MoFEMErrorCode OpRowColFE<row_order, col_order>::doWork(
    int row_side, int col_side, EntityType row_type, EntityType col_type,
    EntData &row_data, EntData &col_data) {
  MoFEMFunctionBegin;

  const int nb_row_dofs = row_data.getIndices().size();
  const int nb_col_dofs = col_data.getIndices().size();

  if (!(nb_row_dofs && nb_col_dofs))
    MoFEMFunctionReturnHot(0);

  if(eNts){
    if (eNts->find(getFEEntityHandle()) == eNts->end())
      MoFEMFunctionReturnHot(0);
  }

  loc_mat.resize(nb_row_dofs, nb_col_dofs, false);
  loc_mat.clear();

  CHKERR calculateLocMatrix(row_data, col_data);
  CHKERR assembleLocMatrix(row_data, col_data);

  MoFEMFunctionReturn(0);
}

template <>
MoFEMErrorCode OpRowColFE<L2_0, L2_0>::calculateLocMatrix(EntData &row_data,
                                                          EntData &col_data) {
  MoFEMFunctionBegin;
  const int nb_integration_pts = getGaussPts().size2();
  const int nb_row_dofs = row_data.getIndices().size();
  const int nb_col_dofs = col_data.getIndices().size();

  auto t_w = getFTensor0IntegrationWeight();
  const double vol = getMeasure();

  const int nb_row_comp = row_data.getFieldDofs()[0]->getNbOfCoeffs();
  const int nb_col_comp = col_data.getFieldDofs()[0]->getNbOfCoeffs();

  const double ts_a = getFEMethod()->ts_a;

  auto gp_coords = getCoordsAtGaussPts();
  matFun(matrixData, gp_coords, ts_a);
  auto t_shift = FTensor::Tensor0<FTensor::PackPtr<double *, 1>>(&matrixData(0,0));

  for (int gg = 0; gg != nb_integration_pts; ++gg) {
    auto t_row_shape = row_data.getFTensor0N(gg, 0);
    const double a = vol * t_w;
    for (int rr = 0; rr != nb_row_dofs; ++rr) {
      auto t_col_shape = col_data.getFTensor0N(gg, 0);
      for (int cc = 0; cc != nb_col_dofs; ++cc) {
        loc_mat(rr, cc) += t_shift * t_row_shape * t_col_shape * a;
        ++t_col_shape;
      }
      ++t_row_shape;
    }
    ++t_w;
    ++t_shift;
  }
  MoFEMFunctionReturn(0);
}
/////////////////////////////////////////////////////////////////////////////
template <>
MoFEMErrorCode OpRowColFE<H1_0, H1_0>::calculateLocMatrix(EntData &row_data,
                                                          EntData &col_data) {
  MoFEMFunctionBegin;
  const int nb_integration_pts = getGaussPts().size2();
  const int nb_row_dofs = row_data.getIndices().size();
  const int nb_col_dofs = col_data.getIndices().size();

  auto t_w = getFTensor0IntegrationWeight();
  const double vol = getMeasure();

  const int nb_row_comp = row_data.getFieldDofs()[0]->getNbOfCoeffs();
  const int nb_col_comp = col_data.getFieldDofs()[0]->getNbOfCoeffs();

  const double ts_a = getFEMethod()->ts_a;

  auto gp_coords = getCoordsAtGaussPts();
  matFun(matrixData, gp_coords, ts_a);
  auto t_shift =
      FTensor::Tensor0<FTensor::PackPtr<double *, 1>>(&matrixData(0, 0));

  for (int gg = 0; gg != nb_integration_pts; ++gg) {
    auto t_row_shape = row_data.getFTensor0N(gg, 0);
    const double a = vol * t_w;
    for (int rr = 0; rr != nb_row_dofs; ++rr) {
      auto t_col_shape = col_data.getFTensor0N(gg, 0);
      for (int cc = 0; cc != nb_col_dofs; ++cc) {
        loc_mat(rr, cc) += t_shift * t_row_shape * t_col_shape * a;
        ++t_col_shape;
      }
      ++t_row_shape;
    }
    ++t_w;
    ++t_shift;
  }
  MoFEMFunctionReturn(0);
}

template <>
MoFEMErrorCode OpRowColFE<H1_1, H1_1>::calculateLocMatrix(EntData &row_data,
                                                          EntData &col_data) {
  MoFEMFunctionBegin;
  const int nb_integration_pts = getGaussPts().size2();
  const int nb_row_dofs = row_data.getIndices().size();
  const int nb_col_dofs = col_data.getIndices().size();

  auto t_w = getFTensor0IntegrationWeight();
  const double vol = getMeasure();

  const int nb_row_comp = row_data.getFieldDofs()[0]->getNbOfCoeffs();
  const int nb_col_comp = col_data.getFieldDofs()[0]->getNbOfCoeffs();

  const double ts_a = getFEMethod()->ts_a;

  auto gp_coords = getCoordsAtGaussPts();
  matFun(matrixData, gp_coords, ts_a);

  double ZERO = 0.0;

  auto getFTensorDiag = [&](MatrixDouble &data, const int pp) {
    return FTensor::Tensor2<double *, 2, 2>(&data(0, pp), &ZERO, 
                                            &ZERO, &data(1, pp));
  };

  FTensor::Index<'i', 2> i;
  FTensor::Index<'j', 2> j;

  for (int gg = 0; gg != nb_integration_pts; ++gg) {
    auto t_row_shape_diff = row_data.getFTensor1DiffN<2>(gg, 0);
    const double a = vol * t_w;
    auto t_cond = getFTensorDiag(matrixData, gg);
    for (int rr = 0; rr != nb_row_dofs; ++rr) {
      auto t_col_shape_diff = col_data.getFTensor1DiffN<2>(gg, 0);
      for (int cc = 0; cc != nb_col_dofs; ++cc) {
        loc_mat(rr, cc) += t_row_shape_diff(i) * t_cond(i, j) * t_col_shape_diff(j) * a;
        ++t_col_shape_diff;
      }
      ++t_row_shape_diff;
    }
    ++t_w;
  }
  MoFEMFunctionReturn(0);
}
/////////////////////////////////////////////////////////////////////////////
template <>
MoFEMErrorCode
OpRowColFE<L2_0, HDIV_1>::calculateLocMatrix(EntData &row_data,
                                              EntData &col_data) {
  MoFEMFunctionBegin;
  const int nb_integration_pts = getGaussPts().size2();
  const int nb_row_dofs = row_data.getIndices().size();
  const int nb_col_dofs = col_data.getIndices().size();

  

  auto t_w = getFTensor0IntegrationWeight();
  const double ts_a = getFEMethod()->ts_a;
  const double vol = getMeasure();

  const int nb_row_comp = row_data.getFieldDofs()[0]->getNbOfCoeffs();
  const int nb_col_comp = col_data.getFieldDofs()[0]->getNbOfCoeffs();

  auto gp_coords = getCoordsAtGaussPts();
  matFun(matrixData, gp_coords, 0);
  auto t_axisymm = FTensor::Tensor0<FTensor::PackPtr<double *, 1>>(&matrixData(0, 0));

  for (int gg = 0; gg != nb_integration_pts; ++gg) {

    auto t_row_shape = row_data.getFTensor0N(gg, 0);
    const double a = vol * t_w ;
    for (int rr = 0; rr != nb_row_dofs; ++rr) {
      auto t_col_shape = col_data.getFTensor2DiffN<3, 2>(gg, 0);
      for (int cc = 0; cc != nb_col_dofs; ++cc) {
        double div_col_shape = t_col_shape(0, 0) + t_col_shape(1, 1);
        loc_mat(rr, cc) += t_row_shape * div_col_shape * t_axisymm * a;
        ++t_col_shape;
      }
      ++t_row_shape;
    }
    ++t_w;
    ++t_axisymm;
  }
  MoFEMFunctionReturn(0);
}
template <>
MoFEMErrorCode
OpRowColFE<HDIV_1, L2_0>::calculateLocMatrix(EntData &row_data,
                                              EntData &col_data) {
  MoFEMFunctionBegin;
  const int nb_integration_pts = getGaussPts().size2();
  const int nb_row_dofs = row_data.getIndices().size();
  const int nb_col_dofs = col_data.getIndices().size();

  

  auto t_w = getFTensor0IntegrationWeight();
  const double vol = getMeasure();

  // const int row_dof_per_basis = row_data.getFieldDofs()[0]->getNbOfCoeffs();
  // const int col_dof_per_basis = col_data.getFieldDofs()[0]->getNbOfCoeffs();

  auto gp_coords = getCoordsAtGaussPts();
  matFun(matrixData, gp_coords, 0);

  auto t_axisymm = FTensor::Tensor0<FTensor::PackPtr<double *, 1>>(&matrixData(0, 0));

  for (int gg = 0; gg != nb_integration_pts; ++gg) {
    auto t_row_shape = row_data.getFTensor2DiffN<3, 2>(gg, 0);
    const double a = vol * t_w;
    for (int rr = 0; rr != nb_row_dofs; ++rr) {
      auto t_col_shape = col_data.getFTensor0N(gg, 0);
      for (int cc = 0; cc != nb_col_dofs; ++cc) {
        double div_row_shape = t_row_shape(0, 0) + t_row_shape(1, 1);
        loc_mat(rr, cc) += div_row_shape * t_col_shape * t_axisymm * a;
        ++t_col_shape;
      }
      ++t_row_shape;
    }
    ++t_w;
    ++t_axisymm;
  }
  MoFEMFunctionReturn(0);
}
template <>
MoFEMErrorCode
OpRowColFE<HDIV_0, HDIV_0>::calculateLocMatrix(EntData &row_data,
                                                EntData &col_data) {
  MoFEMFunctionBegin;
  const int nb_integration_pts = getGaussPts().size2();
  const int nb_row_dofs = row_data.getIndices().size();
  const int nb_col_dofs = col_data.getIndices().size();

  auto t_w = getFTensor0IntegrationWeight();
  const double vol = getMeasure();
  

  const int nb_row_comp = row_data.getFieldDofs()[0]->getNbOfCoeffs();
  const int nb_col_comp = col_data.getFieldDofs()[0]->getNbOfCoeffs();

  auto gp_coords = getCoordsAtGaussPts();
  matFun(matrixData, gp_coords, 0);

  double ZERO = 0.0;

  auto getFTensorDiag =
      [&](MatrixDouble &data, const int pp) {
        return FTensor::Tensor2<double *, 3, 3>(&data(0, pp), &ZERO, &ZERO,
                                                &ZERO, &data(1, pp), &ZERO,
                                                &ZERO, &ZERO, &data(2, pp));
      };

  for (int gg = 0; gg != nb_integration_pts; ++gg) {
    auto t_row_shape = row_data.getFTensor1N<3>(gg, 0);
    const double a = vol * t_w;
    auto t_kinv = getFTensorDiag(matrixData, gg);
    for (int rr = 0; rr != nb_row_dofs; ++rr) {
      auto t_col_shape = col_data.getFTensor1N<3>(gg, 0);
      for (int cc = 0; cc != nb_col_dofs; ++cc) {
        loc_mat(rr, cc) += t_row_shape(i) * t_kinv(i, j) * t_col_shape(j) * a;
        ++t_col_shape;
      }
      ++t_row_shape;
    }
    ++t_w;
  }
  MoFEMFunctionReturn(0);
}

template <DiffOrder row_order, DiffOrder col_order>
MoFEMErrorCode
OpRowColFE<row_order, col_order>::assembleLocMatrix(EntData &row_data,
                                                    EntData &col_data) {
  MoFEMFunctionBegin;
  CHKERR MatSetValues(getFEMethod()->ts_B, row_data, col_data, &loc_mat(0, 0),
                      ADD_VALUES);
  MoFEMFunctionReturn(0);
}

// End implementation of OpRowColFE for LHS operators
// **************************************************
// Begin definition of OpRowFE for RHS operators
template<DiffOrder row_order, IMEX scheme = IM>
struct OpRowFE : OpFaceEle{
  typedef boost::function<void(MatrixDouble &, MatrixDouble &, const double)> MatrixFunction;
  typedef boost::function<void(VectorDouble &, MatrixDouble &, const double)> VectorFunction;
  OpRowFE(std::string                     row_field_name,
          MatrixFunction                  mat_fun,
          Range                           *ents = NULL);

  OpRowFE(std::string                     row_field_name,
          MatrixFunction                  vec_fun,
          MatrixFunction                  axisymm_fun,
          Range                           *ents = NULL);

  OpRowFE(std::string                     row_field_name,
          MatrixFunction                  vec_fun,
          MatrixFunction                  axisymm_fun,
          boost::shared_ptr<MatrixDouble> loc_mass_matrix_ptr,
          Range                           *ents = NULL);

  MoFEMErrorCode doWork(int side, EntityType type, EntData &data);

  private:
    VectorDouble        loc_rhs;
    boost::shared_ptr<MatrixDouble>  locMassPtr;

    MatrixDouble vectorResidual;
    MatrixDouble matrixResidual;
    MatrixDouble axisymmData;

    MatrixFunction vecFun;
    MatrixFunction matFun;
    MatrixFunction axisymmFun;
    

    Range    *eNts;

    MoFEMErrorCode calculateLocRhs(EntData &row_data);
    MoFEMErrorCode assembleLocRhs(EntData &row_data);

}; 
// End definition of OpRowFE for RHS operators
// *******************************************
// Begin implementation of OpRowFE for RHS operators


template <DiffOrder ro, IMEX scheme>
OpRowFE<ro, scheme>::OpRowFE(std::string    row_field_name,
                             MatrixFunction mat_fun,
                             Range          *ents)
    : OpFaceEle(row_field_name, OpFaceEle::OPROW),
      matFun(mat_fun), eNts(ents) {}
template<DiffOrder ro, IMEX scheme>
OpRowFE<ro, scheme>::OpRowFE(std::string    row_field_name,
                             MatrixFunction vec_fun,
                             MatrixFunction axisymm_fun, 
                             Range          *ents)
        : OpFaceEle(row_field_name, OpFaceEle::OPROW)
        , vecFun(vec_fun)
        , axisymmFun(axisymm_fun)
        , eNts(ents){}

template<>
OpRowFE<L2_0, EX>::OpRowFE(std::string      row_field_name,
                             MatrixFunction vec_fun,
                             MatrixFunction axisymm_fun,
                             boost::shared_ptr<MatrixDouble> loc_mass_matrix_ptr, 
                             Range          *ents)
        : OpFaceEle(row_field_name, OpFaceEle::OPROW)
        , vecFun(vec_fun)
        , axisymmFun(axisymm_fun)
        , locMassPtr(loc_mass_matrix_ptr)
        , eNts(ents){}

template<DiffOrder ro, IMEX scheme>
MoFEMErrorCode
OpRowFE<ro, scheme>::doWork(int side, EntityType type, EntData &data){
  MoFEMFunctionBegin;
  const int nb_dofs = data.getIndices().size();

  if(!nb_dofs)
    MoFEMFunctionReturnHot(0);

  if (eNts) {
    if (eNts->find(getFEEntityHandle()) == eNts->end())
      MoFEMFunctionReturnHot(0);
  }

  loc_rhs.resize(nb_dofs, false);
  loc_rhs.clear();

  CHKERR calculateLocRhs(data);
  CHKERR assembleLocRhs(data);

  MoFEMFunctionReturn(0);
}
template <>
MoFEMErrorCode OpRowFE<L2_0, IM>::calculateLocRhs(EntData &row_data) {
  MoFEMFunctionBegin;
  const int nb_row_dofs = row_data.getIndices().size();

  const int nb_integration_pts = getGaussPts().size2();
  auto t_w = getFTensor0IntegrationWeight();
  const double vol = getMeasure();

  auto gp_coords = getCoordsAtGaussPts();
  matFun(matrixResidual, gp_coords, 0);

  auto t_vec_res = FTensor::Tensor0<FTensor::PackPtr<double *, 1>>(&matrixResidual(0, 0));

  for (int gg = 0; gg != nb_integration_pts; ++gg) {
    auto t_row_shape = row_data.getFTensor0N(gg, 0);
    const double a = vol * t_w;
    for (int rr = 0; rr != nb_row_dofs; ++rr) {
      loc_rhs[rr] += t_row_shape * t_vec_res * a;
      ++t_row_shape;
    }
    ++t_vec_res;
    ++t_w;
  }
  MoFEMFunctionReturn(0);
}
//////////////////////////////////////////////////////////////////////////////
template <>
MoFEMErrorCode OpRowFE<H1_0, IM>::calculateLocRhs(EntData &row_data) {
  MoFEMFunctionBegin;
  const int nb_row_dofs = row_data.getIndices().size();

  const int nb_integration_pts = getGaussPts().size2();
  auto t_w = getFTensor0IntegrationWeight();
  const double vol = getMeasure();

  auto gp_coords = getCoordsAtGaussPts();
  matFun(matrixResidual, gp_coords, 0);

  auto t_vec_res =
      FTensor::Tensor0<FTensor::PackPtr<double *, 1>>(&matrixResidual(0, 0));

  for (int gg = 0; gg != nb_integration_pts; ++gg) {
    auto t_row_shape = row_data.getFTensor0N(gg, 0);
    const double a = vol * t_w;
    for (int rr = 0; rr != nb_row_dofs; ++rr) {
      loc_rhs[rr] += t_row_shape * t_vec_res * a;
      ++t_row_shape;
    }
    ++t_vec_res;
    ++t_w;
  }
  MoFEMFunctionReturn(0);
}

template <>
MoFEMErrorCode OpRowFE<H1_1, IM>::calculateLocRhs(EntData &row_data) {
  MoFEMFunctionBegin;
  const int nb_row_dofs = row_data.getIndices().size();

  const int nb_integration_pts = getGaussPts().size2();
  auto t_w = getFTensor0IntegrationWeight();
  const double vol = getMeasure();

  auto gp_coords = getCoordsAtGaussPts();
  matFun(matrixResidual, gp_coords, 0);

  auto t_vec_res = getFTensor1FromMat<2>(matrixResidual);

  FTensor::Index<'i', 2> i;

  for (int gg = 0; gg != nb_integration_pts; ++gg) {
    auto t_row_shape_diff = row_data.getFTensor1DiffN<2>(gg, 0);
    const double a = vol * t_w;
    for (int rr = 0; rr != nb_row_dofs; ++rr) {
      loc_rhs[rr] += t_row_shape_diff(i) * t_vec_res(i) * a;
      ++t_row_shape_diff;
    }
    ++t_vec_res;
    ++t_w;
  }
  MoFEMFunctionReturn(0);
}
//////////////////////////////////////////////////////////////////////////////

template <>
MoFEMErrorCode OpRowFE<L2_0, EX>::calculateLocRhs(EntData &row_data) {
  MoFEMFunctionBegin;
  const int nb_row_dofs = row_data.getIndices().size();

  const int nb_integration_pts = getGaussPts().size2();
  auto t_w = getFTensor0IntegrationWeight();
  const double vol = getMeasure();

  auto gp_coords = getCoordsAtGaussPts();
  vecFun(vectorResidual, gp_coords, 0);
  axisymmFun(axisymmData, gp_coords, 0);

  MatrixDouble loc_mass = *locMassPtr;


  auto t_vec_res =
      FTensor::Tensor0<FTensor::PackPtr<double *, 1>>(&vectorResidual(0, 0));
  auto t_axisymm = 
      FTensor::Tensor0<FTensor::PackPtr<double *, 1>>(&axisymmData(0, 0));

  for (int gg = 0; gg != nb_integration_pts; ++gg) {
    auto t_row_shape = row_data.getFTensor0N(gg, 0);
    const double a = vol * t_w * t_axisymm;
    for (int rr = 0; rr != nb_row_dofs; ++rr) {
      loc_rhs[rr] += t_row_shape * t_vec_res * a;
      ++t_row_shape;
    }
    ++t_vec_res;
    ++t_w;
    ++t_axisymm;
  }

  cholesky_decompose(loc_mass);
  cholesky_solve(loc_mass, loc_rhs, ublas::lower());

  MoFEMFunctionReturn(0);
}

template <>
MoFEMErrorCode OpRowFE<HDIV_0, IM>::calculateLocRhs(EntData &row_data) {
  MoFEMFunctionBegin;
  const int nb_row_dofs = row_data.getIndices().size();

  const int nb_integration_pts = getGaussPts().size2();
  auto t_w = getFTensor0IntegrationWeight();
  const double vol = getMeasure();

  auto gp_coords = getCoordsAtGaussPts();
  matFun(matrixResidual, gp_coords, 0);

  auto t_mat_res = getFTensor1FromMat<3>(matrixResidual);

  for (int gg = 0; gg != nb_integration_pts; ++gg) {
    auto t_row_shape = row_data.getFTensor1N<3>(gg, 0);
    const double a = vol * t_w;
    for (int rr = 0; rr != nb_row_dofs; ++rr) {
      loc_rhs[rr] += t_row_shape(i) * t_mat_res(i) * a;
      ++t_row_shape;
    }
    ++t_mat_res;
    ++t_w;
  }

  MoFEMFunctionReturn(0);
}

template <>
MoFEMErrorCode OpRowFE<HDIV_1, IM>::calculateLocRhs(EntData &row_data) {
  MoFEMFunctionBegin;
  const int nb_row_dofs = row_data.getIndices().size();

  const int nb_integration_pts = getGaussPts().size2();
  auto t_w = getFTensor0IntegrationWeight();
  const double vol = getMeasure();

  auto gp_coords = getCoordsAtGaussPts();
  matFun(matrixResidual, gp_coords, 0);

  auto t_vec_res = FTensor::Tensor0<FTensor::PackPtr<double *, 1>>(&matrixResidual(0, 0));

  for (int gg = 0; gg != nb_integration_pts; ++gg) {
    auto t_row_shape = row_data.getFTensor2DiffN<3, 2>(gg, 0);
    const double a = vol * t_w;
    for (int rr = 0; rr != nb_row_dofs; ++rr) {
      double row_div_shape = t_row_shape(0, 0) + t_row_shape(1, 1);
      loc_rhs[rr] += row_div_shape * t_vec_res * a;
      ++t_row_shape;
    }
    ++t_vec_res;
    ++t_w;
  }

  MoFEMFunctionReturn(0);
}

template <DiffOrder ro, IMEX scheme>
MoFEMErrorCode OpRowFE<ro, scheme>::assembleLocRhs(EntData &row_data) {
  MoFEMFunctionBegin;

  CHKERR VecSetOption(getFEMethod()->ts_F, VEC_IGNORE_NEGATIVE_INDICES,
                      PETSC_TRUE);
  CHKERR VecSetValues(getFEMethod()->ts_F, row_data, &*loc_rhs.begin(), ADD_VALUES);
  MoFEMFunctionReturn(0);
}
    // End implementation of OpRowFE for RHS operators
    // ***********************************************
    // Begin GPFunction, a base function for evaluation at Gauss Points
  // struct GPFunction {
  // GPFunction(boost::shared_ptr<CommonData> common_data)
  // : commonData(common_data){}

  // GPFunction(){}
  // boost::shared_ptr<CommonData> commonData;
  // };
// End GPFunction, a base function for evaluation at Gauss Points
// **************************************************************
// Begin OpComputeResidual, an operator for computing residuals at Gauss Points 
struct OpComputeFieldAtGP : OpFaceEle {
  typedef boost::function<MoFEMErrorCode(EntData &, MatrixDouble &, const double)> Function;
  OpComputeFieldAtGP(std::string field_name, Function eval_fun,
                     const EntityType zero_type = MBVERTEX, Range *ents = NULL)
      : OpFaceEle(field_name, OpFaceEle::OPROW), evalFun(eval_fun),
        zeroType(zero_type), eNts(ents) {}

  MoFEMErrorCode doWork(int side, EntityType type, EntData &data){
    MoFEMFunctionBegin;

    if(type != zeroType)
      MoFEMFunctionReturnHot(0);

    if (eNts) {
      if (eNts->find(getFEEntityHandle()) == eNts->end())
        MoFEMFunctionReturnHot(0);
    }

    int nb_dofs = data.getFieldData().size();
    const int nb_integration_pts = getGaussPts().size2();
    auto gp_coords = getCoordsAtGaussPts();
    double dt;
    CHKERR TSGetTimeStep(getFEMethod()->ts, &dt);
    CHKERR evalFun(data, gp_coords, dt);

    MoFEMFunctionReturn(0);
  }

  private:
    Function evalFun;

    Range *eNts;

    const EntityHandle zeroType;
};
// End OpComputeResidual, an operator for computing residuals at Gauss Points
// **************************************************************************
struct OpComputeFieldDofs : OpFaceEle {
  typedef boost::function<void(MatrixDouble &, MatrixDouble &, const double)> Function;
  OpComputeFieldDofs(std::string      field_name, 
                     Function         mat_func, 
                     Function         axisymm_func,
                     boost::shared_ptr<MatrixDouble> loc_mass_ptr, 
                     Range            *ents = NULL)
      : OpFaceEle(field_name, OpFaceEle::OPROW), 
        locMassPtr(loc_mass_ptr), matFunc(mat_func), axisymmFunc(axisymm_func), eNts(ents) {}

  MoFEMErrorCode doWork(int side, EntityType type, EntData &data) {
    MoFEMFunctionBegin;
    int nb_dofs = data.getFieldData().size();
 
    if (!nb_dofs)
      MoFEMFunctionReturnHot(0);

    if (eNts) {
      if (eNts->find(getFEEntityHandle()) == eNts->end())
        MoFEMFunctionReturnHot(0);
    }
    

    

    // loc_mass.resize(nb_dofs, nb_dofs, false);
    // loc_mass.clear();

    loc_rhs.resize(nb_dofs, false);
    loc_rhs.clear();

    CHKERR calculateLocDofs(data);

    MoFEMFunctionReturn(0);
  }

  private:
    Range *eNts;
    VectorDouble loc_rhs;
    MatrixDouble matrixField; 
    MatrixDouble axisymmData;
    Function matFunc;
    Function axisymmFunc;

    boost::shared_ptr<MatrixDouble>  locMassPtr;

    MoFEMErrorCode calculateLocDofs(EntData &data);   
};

MoFEMErrorCode OpComputeFieldDofs::calculateLocDofs(EntData &data){
  MoFEMFunctionBegin;
  int nb_dofs = data.getFieldData().size();
  const int nb_integration_pts = getGaussPts().size2();

  

  auto t_w = getFTensor0IntegrationWeight();
  const double vol = getMeasure();

  auto gp_coords = getCoordsAtGaussPts();
  matFunc(matrixField, gp_coords, 0);
  axisymmFunc(axisymmData, gp_coords, 0);

  

  auto t_axisymm =
      FTensor::Tensor0<FTensor::PackPtr<double *, 1>>(&axisymmData(0, 0));

  auto t_field =
      FTensor::Tensor0<FTensor::PackPtr<double *, 1>>(&matrixField(0, 0));

  for(int gg = 0; gg != nb_integration_pts; ++gg){
    auto t_row_shape = data.getFTensor0N(gg, 0);
    const double a = vol * t_w * t_axisymm;
    for(int rr = 0; rr != nb_dofs; ++rr){
      // auto t_col_shape = data.getFTensor0N(gg, 0);
      loc_rhs[rr] += t_row_shape * t_field * a;
      // for(int cc = 0; cc != nb_dofs; ++cc){
      //   loc_mass(rr, cc) += t_row_shape * t_col_shape * a;
      //   ++t_col_shape;
      // }
      ++t_row_shape;
    }
    ++t_axisymm;
    ++t_field;
    ++t_w;
  }
  MatrixDouble loc_mass =  *locMassPtr;

  // cerr << "locMass : " << loc_mass << endl;

  cholesky_decompose(loc_mass);
  cholesky_solve(loc_mass, loc_rhs, ublas::lower());

  for (auto &dof : data.getFieldDofs()) {
    dof->getFieldData() = loc_rhs[dof->getEntDofIdx()];
  }


  MoFEMFunctionReturn(0);
  }
  struct OpLocMassMatrix : OpFaceEle {
    OpLocMassMatrix(std::string                     field_name,
                    boost::shared_ptr<MatrixDouble> loc_mass_matrix_ptr,
                    Range                           *ents = NULL)
        : OpFaceEle(field_name, OpFaceEle::OPROW),
          locMassMatrixPtr(loc_mass_matrix_ptr), eNts(ents) {}

    MoFEMErrorCode doWork(int side, EntityType type, EntData &data) {
      MoFEMFunctionBegin;
      const int nb_dofs = data.getIndices().size();
      if (!nb_dofs)
        MoFEMFunctionReturnHot(0);

      if (eNts) {
        if (eNts->find(getFEEntityHandle()) == eNts->end())
          MoFEMFunctionReturnHot(0);
      }



      MatrixDouble &locMass = *locMassMatrixPtr;
      locMass.resize(nb_dofs, nb_dofs, false);
      locMass.clear();

      const int nb_gauss_pts = getGaussPts().size2();
      auto t_w = getFTensor0IntegrationWeight();
      const double vol = getMeasure();

      auto t_coords = getFTensor1CoordsAtGaussPts();

      for (int gg = 0; gg != nb_gauss_pts; ++gg) {
        const double a = vol * t_w * t_coords(0);
        auto t_row_shape = data.getFTensor0N(gg, 0);
        for (int rr = 0; rr != nb_dofs; ++rr) {
          auto t_col_shape = data.getFTensor0N(gg, 0);
          for (int cc = 0; cc != nb_dofs; ++cc) {
            locMass(rr, cc) += t_row_shape * t_col_shape * a;
            ++t_col_shape;
          }
          ++t_row_shape;
        }
        ++t_coords;
        ++t_w;
      }

      MoFEMFunctionReturn(0);
    }

  private:
    boost::shared_ptr<MatrixDouble> locMassMatrixPtr;
    Range *eNts;
  };

  typedef boost::function<double (const double, const double, const double, const double)> GPFunction;

  // MoFEMErrorCode setEssBConDof(std::string      field_name, 
  //                              std::string      boundary_name,
  //                              GPFunction       &function,
  //                              MoFEM::Interface &m_field) {

  //   MoFEMFunctionBegin;

  //   Range boundary_ents;
  //   Range boundary_verts;
  //   for (_IT_CUBITMESHSETS_BY_SET_TYPE_FOR_LOOP_(m_field, BLOCKSET, it)) {
  //     std::string name = it->getName();

  //     const int id = it->getMeshsetId();
  //     if (name.compare(0, name.size(), boundary_name) == 0) {
  //       CHKERR m_field.getInterface<MeshsetsManager>()->getEntitiesByDimension(
  //           id, BLOCKSET, 1, boundary_ents, true);
  //     }
  //   }
  //   auto field_ptr = m_field.get_field_structure(field_name);
  //   auto space = field_ptr->getSpace();
  //   if(space == H1){
  //     for(_IT_GET_DOFS_FIELD_BY_NAME_AND_TYPE_FOR_LOOP_(m_field, field_name, MBVERTEX, dof){
  //       if (boundary_verts->find(getFEEntityHandle()) != boundary_verts->end()){
  //         double coords[3];
  //         auto ent = dof->getEnt();
  //         m_field.get_moab().get_coords(&ent, 1, coords);

  //         dof->get()->getFieldData() =
  //             function(coords[0], coords[1], coords[2], 0.0);
  //       }    
  //     }
  //     boost::shared_ptr<VectorD> 
  //   }else if{

  //   }



    

  //   CHKERR m_field.get_moab().get_adjacencies(
  //       boundary_ents, 0, false, boundary_verts, moab::Interface::UNION);


  //   // struct OpSetBCDof : OpEdgeEle{
  //   //   OpSetBCDof(std::string field_name,
  //   //              GPFunction  function,
  //   //              Range       *ents)
  //   //     : OpEdgeEle(field_name, OpEdgeEle::OPROW)
  //   //     , fUnction(function)
  //   //     , ents(eNts){}
  //   // };
  //   MoFEMFunctionReturn(0);
  // }

  struct OpCopyDofs : OpFaceEle {

    OpCopyDofs(std::string input_field, std::string output_filed, Range *ents = NULL)
    : OpFaceEle(input_field, output_filed, OpFaceEle::OPROWCOL)
    , eNts(ents){}

    Range *eNts;
    MoFEMErrorCode doWork(int        row_side, 
                          int        col_side, 
                          EntityType row_type,
                          EntityType col_type,
                          EntData    &row_data,
                          EntData    &col_data){
      MoFEMFunctionBegin;
      const int nb_row_dofs = row_data.getIndices().size();
      const int nb_col_dofs = col_data.getFieldData().size();
      if(!nb_row_dofs || !nb_col_dofs)
        MoFEMFunctionReturnHot(0);

      if (eNts) {
        if (eNts->find(getFEEntityHandle()) == eNts->end())
          MoFEMFunctionReturnHot(0);
      }

      if(row_side != col_side || row_type != col_type)
        MoFEMFunctionReturnHot(0);

      const auto &row_dofs = row_data.getFieldDofs();
      auto &col_dofs = col_data.getFieldDofs();

      // cerr << "row_dofs : " << row_dofs << endl;

      for(int dd = 0; dd != nb_row_dofs; ++dd){
        col_dofs[dd]->getFieldData() = row_dofs[dd]->getFieldData();
      }
      MoFEMFunctionReturn(0);
    }
  };

  struct OpMassMatrix : OpFaceEle {
    OpMassMatrix(std::string fieldu, SmartPetscObj<Mat> m)
        : OpFaceEle(fieldu, fieldu, OpFaceEle::OPROWCOL), M(m)

    {
      sYmm = true;
    }
    MoFEMErrorCode doWork(int row_side, int col_side, EntityType row_type,
                          EntityType col_type, EntData &row_data,
                          EntData &col_data) {
      MoFEMFunctionBegin;
      const int nb_row_dofs = row_data.getIndices().size();
      const int nb_col_dofs = col_data.getIndices().size();
      if (nb_row_dofs && nb_col_dofs) {
        const int nb_integration_pts = getGaussPts().size2();
        mat.resize(nb_row_dofs, nb_col_dofs, false);
        mat.clear();
        auto t_row_base = row_data.getFTensor0N();
        auto t_w = getFTensor0IntegrationWeight();
        const double vol = getMeasure();
        auto t_coords = getFTensor1CoordsAtGaussPts();
        for (int gg = 0; gg != nb_integration_pts; ++gg) {
          const double a = t_w * vol;
          for (int rr = 0; rr != nb_row_dofs; ++rr) {
            auto t_col_base = col_data.getFTensor0N(gg, 0);
            for (int cc = 0; cc != nb_col_dofs; ++cc) {
              mat(rr, cc) += a * t_row_base * t_col_base * t_coords(0);
              ++t_col_base;
            }
            ++t_row_base;
          }
          ++t_coords;
          ++t_w;
        }
        CHKERR MatSetValues(M, row_data, col_data, &mat(0, 0), ADD_VALUES);
        if (row_side != col_side || row_type != col_type) {
          transMat.resize(nb_col_dofs, nb_row_dofs, false);
          noalias(transMat) = trans(mat);
          CHKERR MatSetValues(M, col_data, row_data, &transMat(0, 0),
                              ADD_VALUES);
        }
      }
      MoFEMFunctionReturn(0);
    }

  private:
    MatrixDouble mat, transMat;
    SmartPetscObj<Mat> M;
  };

}; // namespace FEOperators



#endif