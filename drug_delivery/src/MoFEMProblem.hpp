#include <stdlib.h>
#include <BasicFiniteElements.hpp>

using FaceEle = MoFEM::FaceElementForcesAndSourcesCoreSwitch<
    FaceElementForcesAndSourcesCore::NO_HO_GEOMETRY |
    FaceElementForcesAndSourcesCore::NO_CONTRAVARIANT_TRANSFORM_HDIV |
    FaceElementForcesAndSourcesCore::NO_COVARIANT_TRANSFORM_HCURL>;

using EdgeEle = MoFEM::EdgeElementForcesAndSourcesCoreSwitch<
    EdgeElementForcesAndSourcesCore::NO_HO_GEOMETRY |
    EdgeElementForcesAndSourcesCore::NO_COVARIANT_TRANSFORM_HCURL>;

using OpFaceEle = FaceEle::UserDataOperator;
using OpEdgeEle = EdgeEle::UserDataOperator;

using EntData = DataForcesAndSourcesCore::EntData;

using PostProc = PostProcFaceOnRefinedMesh;

using pFT0_t = FTensor::Tensor0<FTensor::PackPtr<double *, 1>>;

struct Problem {
public:
  Problem(MoFEM::Interface &m_field);

  MoFEMErrorCode runAnalysis();

private:
  MoFEMErrorCode setupInput();
  MoFEMErrorCode findBlocks();
  MoFEMErrorCode setupFields();
  MoFEMErrorCode setupIntegrationRule();
  MoFEMErrorCode applyBIcondition();
  MoFEMErrorCode updateElements(boost::shared_ptr<FaceEle> &fe_ele);
  MoFEMErrorCode setupFETSsystem();
  MoFEMErrorCode setupAssemblySystem();
  MoFEMErrorCode setupPostProcess();
  MoFEMErrorCode executeSetups();
};

MoFEMErrorCode Problem::applyBIcondition(){
  

}
