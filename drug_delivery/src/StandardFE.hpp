#ifndef __STANDARDFE_HPP__
#define __STANDARDFE_HPP__

#include <stdlib.h>
#include <Python/Python.h>
#include "pyhelper.hpp"
#include <BasicFiniteElements.hpp>

using namespace MoFEM;
using namespace ErrorEstimates;
using namespace SimpleADDAnalyis;
using namespace FEOperators;

struct StandardFEProblem {
public:
  StandardFEProblem(MoFEM::Interface &m_field);

  MoFEMErrorCode runAnalysis();

private:
  MoFEMErrorCode setupInput();
  MoFEMErrorCode findBlocks();
  MoFEMErrorCode setupFields();
  MoFEMErrorCode setupIntegrationRule();
  MoFEMErrorCode applyBIcondition();
  MoFEMErrorCode updateElements(boost::shared_ptr<FaceEle> &fe_ele);
  MoFEMErrorCode setupFETSsystem();
  MoFEMErrorCode setupAssemblySystem();
  MoFEMErrorCode setupPostProcess();
  MoFEMErrorCode executeSetups();

  MoFEM::Interface &mField;
  Simple *simpleInterface;

  SmartPetscObj<DM> dM;
  SmartPetscObj<TS> tS;

  SmartPetscObj<Mat> massMatrix;
  SmartPetscObj<KSP> massKsp;

  std::map<std::string, BlockData> setOfBlocks;

  Range essentialBdryEnts;

  Range naturalBdryEnts;

  Range intialValueEnts;

  int oRder;

  boost::shared_ptr<FaceEle> facePipelineNonStiffRhs;
  boost::shared_ptr<FaceEle> facePipelineStiffRhs;
  boost::shared_ptr<FaceEle> facePipelineStiffLhs;

  boost::shared_ptr<FaceEle> facePipelineInitial;

  boost::shared_ptr<FaceEle> massMatrixPipeline;


  boost::shared_ptr<PostProcFaceOnRefinedMesh> postProc;
  boost::shared_ptr<Monitor> monitorPtr;

  boost::shared_ptr<CommonData> commonData;

  boost::shared_ptr<ForcesAndSourcesCore> null;
};

StandardFEProblem::StandardFEProblem(MoFEM::Interface &m_field)
    : mField(m_field), oRder(2) {
  facePipelineNonStiffRhs = boost::shared_ptr<FaceEle>(new FaceEle(m_field));
  facePipelineStiffRhs = boost::shared_ptr<FaceEle>(new FaceEle(m_field));
  facePipelineStiffLhs = boost::shared_ptr<FaceEle>(new FaceEle(m_field));
  facePipelineInitial = boost::shared_ptr<FaceEle>(new FaceEle(m_field));

  massMatrixPipeline = boost::shared_ptr<FaceEle>(new FaceEle(m_field));


  postProc = boost::shared_ptr<PostProcFaceOnRefinedMesh>(
      new PostProcFaceOnRefinedMesh(m_field));

  commonData = boost::shared_ptr<CommonData>(new CommonData());
}

MoFEMErrorCode StandardFEProblem::setupInput() {
  MoFEMFunctionBegin;
  CHKERR mField.getInterface(simpleInterface);
  CHKERR simpleInterface->getOptions();
  CHKERR simpleInterface->loadFile();
  MoFEMFunctionReturn(0);
}

MoFEMErrorCode StandardFEProblem::findBlocks() {
  MoFEMFunctionBegin;

  CPyObject pName;
  CPyObject pModule;
  CPyObject pFunc_rad;
  CPyObject pFunc_axi;
  CPyObject pValue_rad;
  CPyObject pValue_axi;
  CPyObject pArgs;

  pName = PyUnicode_FromString((char *)"rhsFunc");
  pModule = PyImport_Import(pName);
  pFunc_rad = PyObject_GetAttrString(pModule, (char *)"radialDiffuse");
  pFunc_axi = PyObject_GetAttrString(pModule, (char *)"axialDiffuse");

  for (_IT_CUBITMESHSETS_BY_SET_TYPE_FOR_LOOP_(mField, BLOCKSET, it)) {
    std::string name = it->getName();

    char *name_charptr = const_cast<char *>(name.c_str());

    const int id = it->getMeshsetId();
    setOfBlocks[name].iD = id;
    if (name.compare(0, 6, "DOMAIN") == 0) {
      CHKERR mField.getInterface<MeshsetsManager>()->getEntitiesByDimension(
          id, BLOCKSET, 2, setOfBlocks[name].eNts, true);
      // std::vector<double> mat_properties;
      // it->getAttributes(mat_properties);
      // setOfBlocks[name].drugDiffusivity = mat_properties[0];

      pArgs = Py_BuildValue("(z)", name_charptr);

      pValue_rad = PyObject_CallObject(pFunc_rad, pArgs);
      pValue_axi = PyObject_CallObject(pFunc_axi, pArgs);

      setOfBlocks[name].radialDiffuse =
          (timeScale / (lengthScale * lengthScale)) *
          PyFloat_AsDouble(pValue_rad);
      setOfBlocks[name].axialDiffuse =
          (timeScale / (lengthScale * lengthScale)) *
          PyFloat_AsDouble(pValue_axi);

      cerr << "domain name : " << name_charptr << ", D : ("
           << setOfBlocks[name].radialDiffuse << ", "
           << setOfBlocks[name].axialDiffuse << ")" << endl;
    } else if (name.compare(0, 7, "INITIAL") == 0) {
      CHKERR mField.getInterface<MeshsetsManager>()->getEntitiesByDimension(
          id, BLOCKSET, 2, setOfBlocks[name].eNts, true);
      std::vector<double> init_val;
      it->getAttributes(init_val);
      setOfBlocks[name].initVal = 1.0;
    } else if (name.compare(0, 9, "ESSENTIAL") == 0) {
      CHKERR mField.getInterface<MeshsetsManager>()->getEntitiesByDimension(
          id, BLOCKSET, 1, essentialBdryEnts, true);
    }
  }
  MoFEMFunctionReturn(0);
}

MoFEMErrorCode StandardFEProblem::setupFields() {
  MoFEMFunctionBegin;
  CHKERR simpleInterface->addDomainField("CTRN", H1, AINSWORTH_LEGENDRE_BASE, 1);

  CHKERR simpleInterface->addDomainField("AUX", H1, AINSWORTH_LEGENDRE_BASE, 1);

  CHKERR simpleInterface->addDataField("CM", H1, AINSWORTH_LEGENDRE_BASE, 1);



  CHKERR PetscOptionsGetInt(PETSC_NULL, "", "-order", &oRder, PETSC_NULL);

  CHKERR simpleInterface->setFieldOrder("CTRN", oRder - 1);
  CHKERR simpleInterface->setFieldOrder("AUX", oRder - 1);
  CHKERR simpleInterface->setFieldOrder("CM", oRder - 1);

  CHKERR simpleInterface->setUp();

  dM = simpleInterface->getDM();
  tS = createTS(mField.get_comm());
  MoFEMFunctionReturn(0);
}

MoFEMErrorCode StandardFEProblem::setupIntegrationRule() {
  MoFEMFunctionBegin;
  auto vol_rule = [](int, int, int p) -> int { return 2 * p; };

  facePipelineNonStiffRhs->getRuleHook = vol_rule;

  facePipelineStiffRhs->getRuleHook = vol_rule;

  facePipelineStiffLhs->getRuleHook = vol_rule;
  facePipelineInitial->getRuleHook = vol_rule;
  MoFEMFunctionReturn(0);
}

MoFEMErrorCode StandardFEProblem::applyBIcondition() {
  MoFEMFunctionBegin;
  Range surface;
  surface = setOfBlocks["INITIAL"].eNts;
  if (!surface.empty()){
    Range surface_verts;
    CHKERR mField.get_moab().get_connectivity(surface, surface_verts, false);
    CHKERR mField.getInterface<FieldBlas>()->setField(
        setOfBlocks["INITIAL"].initVal, MBVERTEX, surface_verts, "CTRN");
  }

  CHKERR DMCreateMatrix_MoFEM(dM, massMatrix);

  CHKERR MatZeroEntries(massMatrix);
  massMatrixPipeline->getOpPtrVector().push_back(new OpMassMatrix("CTRN", massMatrix));
  massMatrixPipeline->getOpPtrVector().push_back(new OpMassMatrix("AUX", massMatrix));

  CHKERR DMoFEMLoopFiniteElements(dM, simpleInterface->getDomainFEName(),
                                  massMatrixPipeline);
  CHKERR MatAssemblyBegin(massMatrix, MAT_FINAL_ASSEMBLY);
  CHKERR MatAssemblyEnd(massMatrix, MAT_FINAL_ASSEMBLY);

  // Create and septup KSP (linear solver), we need this to calculate g(t,u) =
  // M^-1G(t,u)
  massKsp = createKSP(mField.get_comm());
  CHKERR KSPSetOperators(massKsp, massMatrix, massMatrix);
  CHKERR KSPSetFromOptions(massKsp);
  CHKERR KSPSetUp(massKsp);

  // CHKERR mField.getInterface<ProblemsManager>()->removeDofsOnEntities(
  //     "SimpleProblem", "CTRN_FLUX", essentialBdryEnts);

  MoFEMFunctionReturn(0);
}

MoFEMErrorCode
StandardFEProblem::updateElements(boost::shared_ptr<FaceEle> &fe_ele) {
  MoFEMFunctionBegin;
  // Add operators to calculate the stiff right-hand side
  // facePipelineNonStiffRhs->getOpPtrVector().push_back(
  //     new OpCalculateJacForFace(commonData->jAc));
  fe_ele->getOpPtrVector().push_back(
      new OpCalculateInvJacForFace(commonData->invJac));
  fe_ele->getOpPtrVector().push_back(
      new OpSetInvJacH1ForFace(commonData->invJac));

  MoFEMFunctionReturn(0);
}

MoFEMErrorCode StandardFEProblem::setupFETSsystem() {
  MoFEMFunctionBegin;

  // Non stiff Rhs
  // updateElements(facePipelineNonStiffRhs);
  facePipelineNonStiffRhs->getOpPtrVector().push_back(
      new OpCalculateScalarFieldValues("CTRN", commonData->ctrnValPtr));

  facePipelineNonStiffRhs->getOpPtrVector().push_back(
      new OpCalculateScalarFieldValues("AUX", commonData->auxValPtr));


  facePipelineNonStiffRhs->getOpPtrVector().push_back(
      new OpRowFE<H1_0, IM>("CTRN", CtrnSrcFun(commonData), &setOfBlocks["DOMAIN_2"].eNts));

  facePipelineNonStiffRhs->getOpPtrVector().push_back(
      new OpRowFE<H1_0, IM>("AUX", AuxSrcFun(commonData), &setOfBlocks["DOMAIN_2"].eNts));

  auto solve_for_g = [&]() {
    MoFEMFunctionBegin;
    if (facePipelineNonStiffRhs->vecAssembleSwitch) {
      CHKERR VecGhostUpdateBegin(facePipelineNonStiffRhs->ts_F, ADD_VALUES,
                                 SCATTER_REVERSE);
      CHKERR VecGhostUpdateEnd(facePipelineNonStiffRhs->ts_F, ADD_VALUES,
                               SCATTER_REVERSE);
      CHKERR VecAssemblyBegin(facePipelineNonStiffRhs->ts_F);
      CHKERR VecAssemblyEnd(facePipelineNonStiffRhs->ts_F);
      *facePipelineNonStiffRhs->vecAssembleSwitch = false;
    }
    CHKERR KSPSolve(massKsp, facePipelineNonStiffRhs->ts_F,
                    facePipelineNonStiffRhs->ts_F);
    MoFEMFunctionReturn(0);
  };

  // Add hook to the element to calculate g.
  facePipelineNonStiffRhs->postProcessHook = solve_for_g;

  // Stiff Rhs
  CHKERR updateElements(facePipelineStiffRhs);

  facePipelineStiffRhs->getOpPtrVector().push_back(
      new OpCopyDofs("CTRN", "CM", &setOfBlocks["DOMAIN_2"].eNts));

  facePipelineStiffRhs->getOpPtrVector().push_back(
      new OpCalculateScalarFieldValues("CTRN", commonData->ctrnValPtr));
  facePipelineStiffRhs->getOpPtrVector().push_back(
      new OpCalculateScalarValuesDot("CTRN", commonData->ctrnDotPtr));
  facePipelineStiffRhs->getOpPtrVector().push_back(
      new OpCalculateScalarValuesDot("AUX", commonData->auxDotPtr));

  facePipelineStiffRhs->getOpPtrVector().push_back(
      new OpCalculateScalarFieldGradient<2>("CTRN", commonData->ctrnGradPtr));

  facePipelineStiffRhs->getOpPtrVector().push_back(
    new OpRowFE<H1_0, IM>("CTRN", CtrnDotResidual(commonData)));

  facePipelineStiffRhs->getOpPtrVector().push_back(
      new OpRowFE<H1_0, IM>("AUX", AuxDotResidual(commonData)));

  facePipelineStiffRhs->getOpPtrVector().push_back(
    new OpRowFE<H1_1, IM>("CTRN", CtrnGradRes(setOfBlocks["DOMAIN_1"], 
      commonData), &setOfBlocks["DOMAIN_1"].eNts));

  facePipelineStiffRhs->getOpPtrVector().push_back(
    new OpRowFE<H1_1, IM>("CTRN", CtrnGradRes(setOfBlocks["DOMAIN_2"], 
      commonData), &setOfBlocks["DOMAIN_2"].eNts));

  // Stiff Lhs
  CHKERR updateElements(facePipelineStiffLhs);

  facePipelineStiffLhs->getOpPtrVector().push_back(
      new OpRowColFE<H1_1, H1_1>("CTRN", "CTRN",
          NormalDiffusivity(setOfBlocks["DOMAIN_1"]), &setOfBlocks["DOMAIN_1"].eNts));
  facePipelineStiffLhs->getOpPtrVector().push_back(
      new OpRowColFE<H1_1, H1_1>("CTRN", "CTRN",
          NormalDiffusivity(setOfBlocks["DOMAIN_2"]), &setOfBlocks["DOMAIN_2"].eNts));

  facePipelineStiffLhs->getOpPtrVector().push_back(
      new OpRowColFE<H1_0, H1_0>("CTRN", "CTRN", CtrnTSshift()));

  facePipelineStiffLhs->getOpPtrVector().push_back(
      new OpRowColFE<H1_0, H1_0>("AUX", "AUX", CtrnTSshift()));

  MoFEMFunctionReturn(0);
}

MoFEMErrorCode StandardFEProblem::setupAssemblySystem() {
  MoFEMFunctionBegin;
  CHKERR TSSetType(tS, TSARKIMEX);
  CHKERR TSARKIMEXSetType(tS, TSARKIMEXA2);

  CHKERR DMMoFEMTSSetIJacobian(dM, simpleInterface->getDomainFEName(),
                               facePipelineStiffLhs, null, null);
  CHKERR DMMoFEMTSSetIFunction(dM, simpleInterface->getDomainFEName(),
                               facePipelineStiffRhs, null, null);
  CHKERR DMMoFEMTSSetRHSFunction(dM, simpleInterface->getDomainFEName(),
                                 facePipelineNonStiffRhs, null, null);

  MoFEMFunctionReturn(0);
}

MoFEMErrorCode StandardFEProblem::setupPostProcess() {
  MoFEMFunctionBegin;
  postProc->generateReferenceElementMesh();
  postProc->getOpPtrVector().push_back(
      new OpCalculateJacForFace(commonData->jAc));
  postProc->getOpPtrVector().push_back(
      new OpCalculateInvJacForFace(commonData->invJac));
  postProc->getOpPtrVector().push_back(new OpMakeHdivFromHcurl());

  postProc->getOpPtrVector().push_back(
      new OpSetAxisymmPiolaTransformFace(commonData->jAc));

  postProc->getOpPtrVector().push_back(
      new OpSetInvJacHcurlFace(commonData->invJac));
  postProc->addFieldValuesPostProc("CTRN");
  // postProc->addFieldValuesPostProc("CTRN_FLUX");
  postProc->addFieldValuesPostProc("AUX");
  postProc->addFieldValuesPostProc("CM");

  monitorPtr = boost::shared_ptr<Monitor>(new Monitor(
      mField.get_comm(), mField.get_comm_rank(), dM, tS, postProc, mField));

  CHKERR DMMoFEMTSSetMonitor(dM, tS, simpleInterface->getDomainFEName(),
                             monitorPtr, null, null);

  MoFEMFunctionReturn(0);
}

MoFEMErrorCode StandardFEProblem::executeSetups() {
  MoFEMFunctionBegin;

  TSAdapt adapt;

  // Create solution vector
  SmartPetscObj<Vec> X;
  CHKERR DMCreateGlobalVector_MoFEM(dM, X);
  CHKERR DMoFEMMeshToLocalVector(dM, X, INSERT_VALUES, SCATTER_FORWARD);
  // Solve problem
  double ftime, dt;
  ftime = 1;
  CHKERR TSSetDM(tS, dM);
  CHKERR TSSetMaxTime(tS, ftime);
  CHKERR TSSetSolution(tS, X);
  dt = 1e-5; /* Initial time step */
  CHKERR TSSetTimeStep(tS, dt);

  CHKERR TSGetAdapt(tS, &adapt);
  CHKERR TSAdaptSetType(adapt, TSADAPTBASIC);
  CHKERR TSAdaptSetStepLimits(
      adapt, 1e-12,
      1e-2); /* Also available with -ts_adapt_dt_min/-ts_adapt_dt_max */
  CHKERR TSSetMaxSNESFailures(tS,
                              -1); /* Retry step an unlimited number of times */
  CHKERR TSSetFromOptions(tS);
  CHKERR TSSolve(tS, X);
  MoFEMFunctionReturn(0);
}

MoFEMErrorCode StandardFEProblem::runAnalysis() {
  MoFEMFunctionBegin;
  CHKERR setupInput();
  CHKERR findBlocks();
  CHKERR setupFields();
  CHKERR setupIntegrationRule();
  CHKERR applyBIcondition();
  CHKERR setupFETSsystem();
  CHKERR setupAssemblySystem();
  CHKERR setupPostProcess();
  CHKERR executeSetups();
  MoFEMFunctionReturn(0);
}

#endif //__STANDARDFE_HPP__