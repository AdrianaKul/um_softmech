#include <stdlib.h>
#include <Python/Python.h>
#include "src/pyhelper.hpp"

#include <BasicFiniteElements.hpp>




#include <OpDelivery.hpp>
#include <ErrorEstimateDrafts.hpp>
#include <FundamentalFEOperators.hpp>

using namespace MoFEM;
using namespace ErrorEstimates;
using namespace SimpleADDAnalyis;
using namespace FEOperators;

int axisymm = 0;

static char help[] = "...\n\n";

double dose = 5e-5;
double molar_wt = 853.906;
double vol_coat = 6.0319e-9;
double vol_wall = 1.2517e-6;

double lengthScale = 1e-3;
double timeScale = 12 * 60 * 60;
double ctrnScale = dose / (molar_wt * vol_coat);
double auxScale = ctrnScale; //0 .127;
double rho_wall = 983000;

double k_on = 0.17;
double k_off = 5.27e-4;
double b_max = 0.127;
double D_c = 1e-13;
double Dm_r = 2e-12;
double Dm_z = 5e-11;

struct FreeDrugSrc{
  FreeDrugSrc(boost::shared_ptr<CommonData> common_data_ptr)
      : commonDataPtr(common_data_ptr) {}

  MoFEMErrorCode operator()(MatrixDouble &out_mat, MatrixDouble &gp_coords,
                            const double ts) {
    MoFEMFunctionBegin;
    const size_t nb_gauss_pts = gp_coords.size1();
    out_mat.resize(1, nb_gauss_pts, false);
    out_mat.clear();

    auto t_ctrn_val = getFTensor0FromVec(*commonDataPtr->ctrnValPtr);
    auto t_aux_val = getFTensor0FromVec(*commonDataPtr->auxValPtr);

    auto t_out_vec = FTensor::Tensor0<FTensor::PackPtr<double *, 1>>(&out_mat(0, 0));
    for (int gg = 0; gg != nb_gauss_pts; ++gg) {
      double ctrn_val = ctrnScale * t_ctrn_val;
      double aux_val = auxScale * t_aux_val;
      double out = k_on * ctrn_val * (b_max - aux_val) - k_off * aux_val;

      t_out_vec = - (timeScale / ctrnScale) * out;
      
      ++t_ctrn_val;
      ++t_aux_val;
      ++t_out_vec;
    }

    MoFEMFunctionReturn(0);
  }

  private:
    boost::shared_ptr<CommonData> commonDataPtr;
  };

struct BoundDrugSrc{
  BoundDrugSrc(boost::shared_ptr<CommonData> common_data_ptr)
      : commonDataPtr(common_data_ptr) {}

    MoFEMErrorCode operator()(MatrixDouble &out_mat, MatrixDouble &gp_coords,
                            const double ts) {
    MoFEMFunctionBegin;
    const size_t nb_gauss_pts = gp_coords.size1();
    out_mat.resize(1, nb_gauss_pts, false);
    out_mat.clear();

    auto t_ctrn_val = getFTensor0FromVec(*commonDataPtr->ctrnValPtr);
    auto t_aux_val = getFTensor0FromVec(*commonDataPtr->auxValPtr);

    auto t_out_vec = FTensor::Tensor0<FTensor::PackPtr<double *, 1>>(&out_mat(0, 0));
    for (int gg = 0; gg != nb_gauss_pts; ++gg) {
      double ctrn_val = ctrnScale * t_ctrn_val;
      double aux_val = auxScale * t_aux_val;
      double out = k_on * ctrn_val * (b_max - aux_val) - k_off * aux_val;

      t_out_vec = (timeScale / auxScale) * out;
      
      ++t_ctrn_val;
      ++t_aux_val;
      ++t_out_vec;
    }

    MoFEMFunctionReturn(0);
  }

private:
  boost::shared_ptr<CommonData> commonDataPtr;
};

struct JacFreeDrug {
  JacFreeDrug(boost::shared_ptr<CommonData> common_data_ptr)
      : commonDataPtr(common_data_ptr) {}

  MoFEMErrorCode operator()(MatrixDouble &out_mat, MatrixDouble &gp_coords,
                            const double ts) {
    MoFEMFunctionBegin;
    const size_t nb_gauss_pts = gp_coords.size1();
    out_mat.resize(1, nb_gauss_pts, false);
    out_mat.clear();

    auto t_ctrn_val = getFTensor0FromVec(*commonDataPtr->ctrnValPtr);
    auto t_aux_val = getFTensor0FromVec(*commonDataPtr->auxValPtr);

    auto t_out_vec = FTensor::Tensor0<FTensor::PackPtr<double *, 1>>(&out_mat(0, 0));
    for (int gg = 0; gg != nb_gauss_pts; ++gg) {
      double ctrn_val = ctrnScale * t_ctrn_val;
      double aux_val = auxScale * t_aux_val;
      double out = k_on * (b_max - aux_val) ;

      t_out_vec = (timeScale / ctrnScale) * out;

      ++t_ctrn_val;
      ++t_aux_val;
      ++t_out_vec;
    }

    MoFEMFunctionReturn(0);
  }

private:
  boost::shared_ptr<CommonData> commonDataPtr;
};

struct JacBoundDrug {
  JacBoundDrug(boost::shared_ptr<CommonData> common_data_ptr)
      : commonDataPtr(common_data_ptr) {}

  MoFEMErrorCode operator()(MatrixDouble &out_mat, MatrixDouble &gp_coords,
                            const double ts) {
    MoFEMFunctionBegin;
    const size_t nb_gauss_pts = gp_coords.size1();
    out_mat.resize(1, nb_gauss_pts, false);
    out_mat.clear();

    auto t_ctrn_val = getFTensor0FromVec(*commonDataPtr->ctrnValPtr);
    auto t_aux_val = getFTensor0FromVec(*commonDataPtr->auxValPtr);

    auto t_out_vec = FTensor::Tensor0<FTensor::PackPtr<double *, 1>>(&out_mat(0, 0));
    for (int gg = 0; gg != nb_gauss_pts; ++gg) {
      double ctrn_val = ctrnScale * t_ctrn_val;
      double aux_val = auxScale * t_aux_val;
      double out = - k_on * ctrn_val - k_off;

      t_out_vec = (timeScale / auxScale) * out;

      ++t_ctrn_val;
      ++t_aux_val;
      ++t_out_vec;
    }

    MoFEMFunctionReturn(0);
  }

private:
  boost::shared_ptr<CommonData> commonDataPtr;
};

struct InitValAtGP {
  InitValAtGP(double &init_val) : initVal(init_val) {}
  MoFEMErrorCode operator()(MatrixDouble &out_mat, MatrixDouble &gp_coords,
                            const double ts) {
    MoFEMFunctionBegin;
    const size_t nb_gauss_pts = gp_coords.size1();
    out_mat.resize(1, nb_gauss_pts, false);
    out_mat.clear();
    auto t_outvec =
        FTensor::Tensor0<FTensor::PackPtr<double *, 1>>(&out_mat(0, 0));

    double symm_data;

    auto t_coords = pFTGP(&gp_coords(0, 0), &gp_coords(0, 1), &gp_coords(0, 2));
    for (int gg = 0; gg != nb_gauss_pts; ++gg) {
      if (axisymm) {
        symm_data = t_coords(0);
      } else {
        symm_data = 1;
      }
      t_outvec = initVal;
      ++t_coords;
      ++t_outvec;
    }
    MoFEMFunctionReturn(0);
  }

private:
  double &initVal;
};

struct StepUpAuxField {
  StepUpAuxField(boost::shared_ptr<CommonData> common_data_ptr)
      : commonDataPtr(common_data_ptr) {}

  MoFEMErrorCode operator()(EntData &data, MatrixDouble &gp_coords,
                            const double dt) {
    MoFEMFunctionBegin;
    const size_t nb_gauss_pts = gp_coords.size1();

    CPyObject pName;
    CPyObject pModule;
    CPyObject pFunc_val;
    CPyObject pFunc_jac;
    CPyObject pValue;
    CPyObject pArgs;

    pName = PyUnicode_FromString((char *)"rhsFunc");
    pModule = PyImport_Import(pName);
    pFunc_val = PyObject_GetAttrString(pModule, (char *)"rhs_func");
    pFunc_jac = PyObject_GetAttrString(pModule, (char *)"jac_rhs_func");
    pArgs = PyTuple_New(2);

    auto read_pyFunction = [&](double ctrn, double aux) {
      double scaled_ctrn_val = ctrnScale * ctrn;
      double scaled_aux_val = auxScale * aux;
      PyTuple_SetItem(pArgs, 0, PyFloat_FromDouble(scaled_ctrn_val));
      PyTuple_SetItem(pArgs, 1, PyFloat_FromDouble(scaled_aux_val));

      pValue = PyObject_CallObject(pFunc_val, pArgs);
      return (timeScale / auxScale) * PyFloat_AsDouble(pValue);
    };

    auto read_jacFunc = [&](double ctrn, double aux) {
      double scaled_ctrn_val = ctrnScale * ctrn;
      double scaled_aux_val = auxScale * aux;
      PyTuple_SetItem(pArgs, 0, PyFloat_FromDouble(scaled_ctrn_val));
      PyTuple_SetItem(pArgs, 1, PyFloat_FromDouble(scaled_aux_val));
      pValue = PyObject_CallObject(pFunc_jac, pArgs);
      return (timeScale / auxScale) * PyFloat_AsDouble(pValue);
    };
    MatrixDouble loc_mass;
    VectorDouble loc_rhs;

    int nb_dofs = data.getFieldData().size();
    loc_mass.resize(nb_dofs, nb_dofs, false);
    loc_mass.clear();

    loc_rhs.resize(nb_dofs, false);
    loc_rhs.clear();

    auto t_ctrn_val = getFTensor0FromVec(*commonDataPtr->ctrnValPtr);
    auto t_aux_val = getFTensor0FromVec(*commonDataPtr->auxValPtr);

    const double Tol = 1e-10;
    const double theta = 1.0;
    const int max_iter = 10;

    auto thetaMethod = [&](double ctrn_old, double aux_old) {
      double aux_val = aux_old;
      double Gk = aux_old - aux_val +
                  dt * (1.0 - theta) * read_pyFunction(ctrn_old, aux_old) +
                  dt * theta * read_pyFunction(ctrn_old, aux_val);
      int iter = 0;
      while (abs(Gk) > Tol && iter != max_iter) {
        double diff_Gk = -1 + dt * theta * read_jacFunc(ctrn_old, aux_val);
        aux_val += -Gk / diff_Gk;

        Gk = aux_old - aux_val +
             dt * (1.0 - theta) * read_pyFunction(ctrn_old, aux_old) +
             dt * theta * read_pyFunction(ctrn_old, aux_val);

        iter++;
      }
      // cerr << "Gk : " << Gk << endl;
      // cerr << "Iteration : " << iter << endl;
      // cerr << "Converges" << endl;
      // cerr << "aux_val : " << aux_val << endl;
      return aux_val;
    };

    auto evalRk4 = [&](double ctrn_old, double aux_old) {
      // const double aux_old_val = auxScale * t_aux_val;
      const double ctrn_val_scaled = ctrnScale * t_ctrn_val;

      double aux_val = aux_old;

      const double k1 = read_pyFunction(ctrn_old, aux_val);
      aux_val = (aux_old + 0.5 * dt * k1);
      const double k2 = read_pyFunction(ctrn_old, aux_val);
      aux_val = (aux_old + 0.5 * dt * k2);
      const double k3 = read_pyFunction(ctrn_old, aux_val);
      aux_val = (aux_old + dt * k3);
      const double k4 = read_pyFunction(ctrn_old, aux_val);

      return aux_old + dt / 6.0 * (k1 + 2 * k2 + 2 * k3 + k4);
    };

    auto evalRk2 = [&](double ctrn_old, double aux_old) {
      double aux_val = aux_old;

      const double k1 = read_pyFunction(ctrn_old, aux_val);
      // aux_val = (aux_old + 0.5 * dt * k1);
      // const double k2 = read_pyFunction(ctrn_old, aux_val);

      // return aux_old + dt * k2;
      return aux_old + dt * read_pyFunction(ctrn_old, aux_old + 0.5 * dt * k1);
    };

    for (int gg = 0; gg != nb_gauss_pts; ++gg) {

      // cerr << "t_aux_val : " << t_aux_val << endl;
      // t_aux_val =  evalRk4(t_aux_val);
      t_aux_val = evalRk2(t_ctrn_val, t_aux_val);
      // t_aux_val = thetaMethod(t_ctrn_val, t_aux_val);

      ++t_ctrn_val;
      ++t_aux_val;
    }

    MoFEMFunctionReturn(0);
  }

private:
  boost::shared_ptr<CommonData> commonDataPtr;
};

struct AuxDof {
  AuxDof(boost::shared_ptr<CommonData> common_data_ptr)
      : commonDataPtr(common_data_ptr) {}
  void operator()(MatrixDouble &out_mat, MatrixDouble &gp_coords,
                  const double ts) {
    const size_t nb_gauss_pts = gp_coords.size1();
    out_mat.resize(1, nb_gauss_pts, false);
    out_mat.clear();
    // cout << "Aux size : " << commonDataPtr->auxValPtr->size() << endl;
    auto t_invec = getFTensor0FromVec(*commonDataPtr->auxValPtr);
    auto t_outvec =
        FTensor::Tensor0<FTensor::PackPtr<double *, 1>>(&out_mat(0, 0));
    for (int gg = 0; gg != nb_gauss_pts; ++gg) {
      t_outvec = t_invec;
      ++t_invec;
      ++t_outvec;
    }
  }

private:
  boost::shared_ptr<CommonData> commonDataPtr;
};

struct CtrnSrcFun {
  CtrnSrcFun(boost::shared_ptr<CommonData> common_data_ptr)
      : commonDataPtr(common_data_ptr) {}
  void operator()(MatrixDouble &out_mat, MatrixDouble &gp_coords,
                  const double ts) {
    const size_t nb_gauss_pts = gp_coords.size1();
    out_mat.resize(1, nb_gauss_pts, false);
    out_mat.clear();
    // auto t_invec = getFTensor0FromVec(*commonDataPtr->auxDotPtr);

    CPyObject pName;
    CPyObject pModule;
    CPyObject pFunc_val;
    CPyObject pValue;
    CPyObject pArgs;

    pName = PyUnicode_FromString((char *)"rhsFunc");
    pModule = PyImport_Import(pName);
    pFunc_val = PyObject_GetAttrString(pModule, (char *)"rhs_func");
    pArgs = PyTuple_New(2);

    auto read_pyFunction = [&](double ctrn, double aux) {
      double scaled_ctrn_val = ctrnScale * ctrn;
      double scaled_aux_val = auxScale * aux;
      PyTuple_SetItem(pArgs, 0, PyFloat_FromDouble(scaled_ctrn_val));
      PyTuple_SetItem(pArgs, 1, PyFloat_FromDouble(scaled_aux_val));

      pValue = PyObject_CallObject(pFunc_val, pArgs);
      return (timeScale / ctrnScale) * PyFloat_AsDouble(pValue);
    };

    auto t_ctrn_val = getFTensor0FromVec(*commonDataPtr->ctrnValPtr);
    auto t_aux_val = getFTensor0FromVec(*commonDataPtr->auxValPtr);
    double symm_data;

    auto t_outvec =
        FTensor::Tensor0<FTensor::PackPtr<double *, 1>>(&out_mat(0, 0));
    auto t_coords = pFTGP(&gp_coords(0, 0), &gp_coords(0, 1), &gp_coords(0, 2));
    for (int gg = 0; gg != nb_gauss_pts; ++gg) {
      if (axisymm) {
        symm_data = t_coords(0);
      } else {
        symm_data = 1;
      }
      t_outvec = -read_pyFunction(t_ctrn_val, t_aux_val);
      ++t_ctrn_val;
      ++t_aux_val;
      ++t_outvec;
      ++t_coords;
    }
  }

private:
  boost::shared_ptr<CommonData> commonDataPtr;
};

struct AuxSrcFun {
  AuxSrcFun(boost::shared_ptr<CommonData> common_data_ptr)
      : commonDataPtr(common_data_ptr) {}
  void operator()(MatrixDouble &out_mat, MatrixDouble &gp_coords,
                  const double ts) {
    const size_t nb_gauss_pts = gp_coords.size1();
    out_mat.resize(1, nb_gauss_pts, false);
    out_mat.clear();
    // auto t_invec = getFTensor0FromVec(*commonDataPtr->auxDotPtr);

    CPyObject pName;
    CPyObject pModule;
    CPyObject pFunc_val;
    CPyObject pValue;
    CPyObject pArgs;

    pName = PyUnicode_FromString((char *)"rhsFunc");
    pModule = PyImport_Import(pName);
    pFunc_val = PyObject_GetAttrString(pModule, (char *)"rhs_func");
    pArgs = PyTuple_New(2);

    auto read_pyFunction = [&](double ctrn, double aux) {
      double scaled_ctrn_val = ctrnScale * ctrn;
      double scaled_aux_val = auxScale * aux;
      PyTuple_SetItem(pArgs, 0, PyFloat_FromDouble(scaled_ctrn_val));
      PyTuple_SetItem(pArgs, 1, PyFloat_FromDouble(scaled_aux_val));

      pValue = PyObject_CallObject(pFunc_val, pArgs);
      return (timeScale / auxScale) * PyFloat_AsDouble(pValue);
    };

    auto t_ctrn_val = getFTensor0FromVec(*commonDataPtr->ctrnValPtr);
    auto t_aux_val = getFTensor0FromVec(*commonDataPtr->auxValPtr);

    double symm_data;

    auto t_outvec =
        FTensor::Tensor0<FTensor::PackPtr<double *, 1>>(&out_mat(0, 0));
    auto t_coords = pFTGP(&gp_coords(0, 0), &gp_coords(0, 1), &gp_coords(0, 2));
    for (int gg = 0; gg != nb_gauss_pts; ++gg) {
      if (axisymm) {
        symm_data = t_coords(0);
      } else {
        symm_data = 1;
      }
      // cerr << "x_coord : " << t_coords(0) << endl;
      t_outvec = read_pyFunction(t_ctrn_val, t_aux_val);
      ++t_ctrn_val;
      ++t_aux_val;
      ++t_outvec;
      ++t_coords;
    }
  }

private:
  boost::shared_ptr<CommonData> commonDataPtr;
};

struct AxisymmFun {
  AxisymmFun(double sign) : sIgn(sign) {}

  void operator()(MatrixDouble &out_mat, MatrixDouble &gp_coords,
                  const double ts) {
    const size_t nb_gauss_pts = gp_coords.size1();
    out_mat.resize(1, nb_gauss_pts, false);
    out_mat.clear();

    double symm_data;

    auto t_coords = pFTGP(&gp_coords(0, 0), &gp_coords(0, 1), &gp_coords(0, 2));
    auto t_outvec =
        FTensor::Tensor0<FTensor::PackPtr<double *, 1>>(&out_mat(0, 0));
    for (int gg = 0; gg != nb_gauss_pts; ++gg) {
      if(axisymm){
        symm_data = t_coords(0);
      } else {
        symm_data = 1; 
      }
      t_outvec = symm_data * sIgn;
      ++t_outvec;
      ++t_coords;
    }
  }

private:
  double sIgn;
};

struct CtrnFluxResidual {
  CtrnFluxResidual(boost::shared_ptr<CommonData> common_data_ptr,
                   BlockData &block_data)
      : commonDataPtr(common_data_ptr), blockData(block_data) {}

  void operator()(MatrixDouble &matrix_data, MatrixDouble &gp_coords,
                  const double ts) {
    const size_t nb_gauss_pts = gp_coords.size1();
    matrix_data.resize(3, nb_gauss_pts, false);
    matrix_data.clear();

    const double Kr = blockData.radialDiffuse;
    const double Kz = blockData.axialDiffuse;
    double KR = 1.0 / Kr;
    double KZ = 1.0 / Kz;
    double ZERO = 0.0;
    double symm_data;

    auto t_kinv = FTensor::Tensor2<double *, 3, 3>(
        &KR, &ZERO, &ZERO, &ZERO, &KZ, &ZERO, &ZERO, &ZERO, &KZ);

    auto t_coords = pFTGP(&gp_coords(0, 0), &gp_coords(0, 1), &gp_coords(0, 2));
    auto t_out_data = getFTensor1FromMat<3>(matrix_data);
    auto t_ctrn_flux = getFTensor1FromMat<3>(*commonDataPtr->ctrnFluxPtr);
    for (int gg = 0; gg != nb_gauss_pts; ++gg) {
      if (axisymm) {
        symm_data = t_coords(0);
      } else {
        symm_data = 1;
      }
      t_out_data(k) = (t_kinv(k, j) * t_ctrn_flux(j)) * symm_data;
      ++t_out_data;
      ++t_coords;
      ++t_ctrn_flux;
    }
  }

private:
  boost::shared_ptr<CommonData> commonDataPtr;
  BlockData &blockData;
};
struct CtrnFluxDivResidual {
  CtrnFluxDivResidual(boost::shared_ptr<CommonData> common_data_ptr)
      : commonDataPtr(common_data_ptr) {}
  void operator()(MatrixDouble &out_mat, MatrixDouble &gp_coords,
                  const double ts) {
    const size_t nb_gauss_pts = gp_coords.size1();
    out_mat.resize(1, nb_gauss_pts, false);
    out_mat.clear();
    double symm_data;

    auto t_coords = pFTGP(&gp_coords(0, 0), &gp_coords(0, 1), &gp_coords(0, 2));
    auto t_out =
        FTensor::Tensor0<FTensor::PackPtr<double *, 1>>(&out_mat(0, 0));
    auto t_ctrn = getFTensor0FromVec(*commonDataPtr->ctrnValPtr);
    for (int gg = 0; gg != nb_gauss_pts; ++gg) {
      if (axisymm) {
        symm_data = t_coords(0);
      } else {
        symm_data = 1;
      }
      t_out = -t_ctrn * symm_data;
      ++t_out;
      ++t_coords;
      ++t_ctrn;
    }
  }

private:
  boost::shared_ptr<CommonData> commonDataPtr;
};

struct CtrnResidual {
  CtrnResidual(boost::shared_ptr<CommonData> common_data_ptr)
      : commonDataPtr(common_data_ptr) {}
  void operator()(MatrixDouble &out_mat, MatrixDouble &gp_coords,
                  const double ts) {
    const size_t nb_gauss_pts = gp_coords.size1();
    out_mat.resize(1, nb_gauss_pts, false);
    out_mat.clear();

    double symm_data;

    auto t_coords = pFTGP(&gp_coords(0, 0), &gp_coords(0, 1), &gp_coords(0, 2));
    auto t_out =
        FTensor::Tensor0<FTensor::PackPtr<double *, 1>>(&out_mat(0, 0));
    auto t_ctrn_dot = getFTensor0FromVec(*commonDataPtr->ctrnDotPtr);
    auto t_aux_dot = getFTensor0FromVec(*commonDataPtr->auxDotPtr);
    auto t_ctrn_flux_div = getFTensor0FromVec(*commonDataPtr->ctrnFluxDivPtr);
    for (int gg = 0; gg != nb_gauss_pts; ++gg) {
      if (axisymm) {
        symm_data = t_coords(0);
      } else {
        symm_data = 1;
      }
      t_out = (t_ctrn_dot + t_ctrn_flux_div) * symm_data;
      ++t_out;
      ++t_coords;
      ++t_ctrn_dot;
      ++t_aux_dot;
      ++t_ctrn_flux_div;
    }
  }

private:
  boost::shared_ptr<CommonData> commonDataPtr;
};

struct CtrnDotResidual {
  CtrnDotResidual(boost::shared_ptr<CommonData> common_data_ptr)
      : commonDataPtr(common_data_ptr) {}
  void operator()(MatrixDouble &out_mat, MatrixDouble &gp_coords,
                  const double ts) {
    const size_t nb_gauss_pts = gp_coords.size1();
    out_mat.resize(1, nb_gauss_pts, false);
    out_mat.clear();

    double symm_data;

    auto t_coords = pFTGP(&gp_coords(0, 0), &gp_coords(0, 1), &gp_coords(0, 2));
    auto t_out =
        FTensor::Tensor0<FTensor::PackPtr<double *, 1>>(&out_mat(0, 0));
    auto t_ctrn_dot = getFTensor0FromVec(*commonDataPtr->ctrnDotPtr);

    for (int gg = 0; gg != nb_gauss_pts; ++gg) {
      if (axisymm) {
        symm_data = t_coords(0);
      } else {
        symm_data = 1;
      }
      t_out = t_ctrn_dot * symm_data;
      ++t_out;
      ++t_coords;
      ++t_ctrn_dot;
    }
  }

private:
  boost::shared_ptr<CommonData> commonDataPtr;
};

struct AuxDotResidual {
  AuxDotResidual(boost::shared_ptr<CommonData> common_data_ptr)
      : commonDataPtr(common_data_ptr) {}
  void operator()(MatrixDouble &out_mat, MatrixDouble &gp_coords,
                  const double ts) {
    const size_t nb_gauss_pts = gp_coords.size1();
    out_mat.resize(1, nb_gauss_pts, false);
    out_mat.clear();

    double symm_data;

    auto t_coords = pFTGP(&gp_coords(0, 0), &gp_coords(0, 1), &gp_coords(0, 2));
    auto t_out =
        FTensor::Tensor0<FTensor::PackPtr<double *, 1>>(&out_mat(0, 0));
    auto t_aux_dot = getFTensor0FromVec(*commonDataPtr->auxDotPtr);

    for (int gg = 0; gg != nb_gauss_pts; ++gg) {
      if (axisymm) {
        symm_data = t_coords(0);
      } else {
        symm_data = 1;
      }
      t_out = t_aux_dot * symm_data;
      ++t_out;
      ++t_coords;
      ++t_aux_dot;
    }
  }

private:
  boost::shared_ptr<CommonData> commonDataPtr;
};

struct Diffusivity {
  Diffusivity(BlockData &block_data) : blockData(block_data) {}
  void operator()(MatrixDouble &matrix_data, MatrixDouble &gp_coords,
                  const double ts) {
    const size_t nb_gauss_pts = gp_coords.size1();
    matrix_data.resize(3, nb_gauss_pts, false);
    matrix_data.clear();

    const double Kr = blockData.radialDiffuse;
    const double Kz = blockData.axialDiffuse;
    double KR = 1.0 / Kr;
    double KZ = 1.0 / Kz;

    double symm_data;

    auto t_coords = pFTGP(&gp_coords(0, 0), &gp_coords(0, 1), &gp_coords(0, 2));
    auto t_out_data = FTensor::Tensor1<FTensor::PackPtr<double *, 1>, 3>(
        &matrix_data(0, 0), &matrix_data(1, 0), &matrix_data(2, 0));
    for (int gg = 0; gg != nb_gauss_pts; ++gg) {
      if (axisymm) {
        symm_data = t_coords(0);
      } else {
        symm_data = 1;
      }
      t_out_data(0) = KR * symm_data;
      t_out_data(1) = KZ * symm_data;
      t_out_data(2) = KZ * symm_data;
      ++t_out_data;
      ++t_coords;
    }
  }

private:
  BlockData &blockData;
};

struct NormalDiffusivity {
  NormalDiffusivity(BlockData &block_data) : blockData(block_data) {}
  void operator()(MatrixDouble &matrix_data, MatrixDouble &gp_coords,
                  const double ts) {
    const size_t nb_gauss_pts = gp_coords.size1();
    matrix_data.resize(3, nb_gauss_pts, false);
    matrix_data.clear();

    const double Kr = blockData.radialDiffuse;
    const double Kz = blockData.axialDiffuse;
    double symm_data;

    auto t_coords = pFTGP(&gp_coords(0, 0), &gp_coords(0, 1), &gp_coords(0, 2));
    auto t_out_data = FTensor::Tensor1<FTensor::PackPtr<double *, 1>, 3>(
        &matrix_data(0, 0), &matrix_data(1, 0), &matrix_data(2, 0));
    for (int gg = 0; gg != nb_gauss_pts; ++gg) {
      if (axisymm) {
        symm_data = t_coords(0);
      } else {
        symm_data = 1;
      }
      t_out_data(0) = Kr * symm_data;
      t_out_data(1) = Kz * symm_data;
      t_out_data(2) = 0;
      ++t_out_data;
      ++t_coords;
    }
  }

private:
  BlockData &blockData;
};

struct CtrnGradRes {
  CtrnGradRes(BlockData &block_data, boost::shared_ptr<CommonData> common_data_ptr) 
  : blockData(block_data), commonDataPtr(common_data_ptr) {}
  void operator()(MatrixDouble &matrix_data, MatrixDouble &gp_coords,
                  const double ts) {
    const size_t nb_gauss_pts = gp_coords.size1();
    matrix_data.resize(2, nb_gauss_pts, false);
    matrix_data.clear();

    const double Kr = blockData.radialDiffuse;
    const double Kz = blockData.axialDiffuse;

    double symm_data;

    auto t_coords = pFTGP(&gp_coords(0, 0), &gp_coords(0, 1), &gp_coords(0, 2));
    auto t_ctrn_grad = getFTensor1FromMat<2>(*commonDataPtr->ctrnGradPtr);
    auto t_out_data = FTensor::Tensor1<FTensor::PackPtr<double *, 1>, 2>(
        &matrix_data(0, 0), &matrix_data(1, 0));
    for (int gg = 0; gg != nb_gauss_pts; ++gg) {
      if (axisymm) {
        symm_data = t_coords(0);
      } else {
        symm_data = 1;
      }
      t_out_data(0) = Kr * t_ctrn_grad(0) * symm_data;
      t_out_data(1) = Kz * t_ctrn_grad(1) * symm_data;
      ++t_out_data;
      ++t_ctrn_grad;
      ++t_coords;
    }
  }

private:
  BlockData &blockData;
  boost::shared_ptr<CommonData> commonDataPtr;
};

struct CtrnTSshift {
  CtrnTSshift() {}
  void operator()(MatrixDouble &out_mat, MatrixDouble &gp_coords,
                  const double ts) {
    const size_t nb_gauss_pts = gp_coords.size1();
    out_mat.resize(1, nb_gauss_pts, false);
    out_mat.clear();

    double symm_data;

    auto t_coords = pFTGP(&gp_coords(0, 0), &gp_coords(0, 1), &gp_coords(0, 2));
    auto t_out_data =
        FTensor::Tensor0<FTensor::PackPtr<double *, 1>>(&out_mat(0, 0));
    for (int gg = 0; gg != nb_gauss_pts; ++gg) {
      if (axisymm) {
        symm_data = t_coords(0);
      } else {
        symm_data = 1;
      }
      t_out_data = ts * symm_data;
      ++t_out_data;
      ++t_coords;
    }
  }
};

#include <MixedFE.hpp>
#include <StandardFE.hpp>

int main(int argc, char *argv[]) {

  setenv("PYTHONPATH", ".", 1);
  CPyInstance hInstance;

  const char param_file[] = "param_file.petsc";

  MoFEM::Core::Initialize(&argc, &argv, param_file, help);
  try {
    DMType dm_name = "DMMOFEM";
    CHKERR DMRegister_MoFEM(dm_name);

    cerr << "Length scale : " << lengthScale << endl;
    cerr << "Time scale : " << timeScale << endl;
    cerr << "Ctrn scale : " << ctrnScale << endl;
    cerr << "dt : " << timeScale*0.0001 << endl;

    CHKERR PetscOptionsGetInt(PETSC_NULL, "", "-axisymm", &axisymm, PETSC_NULL);

    moab::Core mb_instance;
    moab::Interface &moab = mb_instance;
    MoFEM::Core core(moab);

    MoFEM::Interface &m_field = core;

    MixedFEProblem drug_delivery_mixed_fe(m_field);
    CHKERR drug_delivery_mixed_fe.runAnalysis();

    // StandardFEProblem drug_delivery_std_fe(m_field);
    // CHKERR drug_delivery_std_fe.runAnalysis();
    }
  CATCH_ERRORS;
  MoFEM::Core::Finalize();
  return 0;
}
    