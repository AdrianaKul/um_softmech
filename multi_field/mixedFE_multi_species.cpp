#include <stdlib.h>
#include <BasicFiniteElements.hpp>

int g_max_order = 1;
double global_error0;
double global_error1;
double post_error;
double maxError;

MatrixDouble jac;
MatrixDouble inv_jac;

Tag quadRule;
Tag maxOrder;

std::vector<std::string> mass_names;
std::vector<std::string> flux_names;

#include <MFOperators.hpp>
#include <ErrorEstimateDrafts.hpp>

using namespace MoFEM;
using namespace ReactionDiffusion;
using namespace ErrorEstimates;

static char help[] = "...\n\n";

// #define M_PI 3.14159265358979323846 /* pi */
template <int nbSpecies> 
struct RDProblem {
public:
  RDProblem(MoFEM::Core &core);

  // RDProblem(const int order) : order(order){}
  MoFEMErrorCode runAnalysis();

private:
  MoFEMErrorCode readMesh();
  MoFEMErrorCode setupSystem();
  MoFEMErrorCode setIntegrationRule();

  MoFEMErrorCode setupBC();
  MoFEMErrorCode setupIC();
  
  MoFEMErrorCode setupPipeline();
  MoFEMErrorCode pushUDOperators();

  MoFEMErrorCode assembleSystem();
  MoFEMErrorCode setOutput();
  MoFEMErrorCode solveSystem();

  boost::shared_ptr<FaceEle> facePipelineNStiffRhs;
  boost::shared_ptr<FaceEle> facePipelineStiffRhs;
  boost::shared_ptr<FaceEle> facePipelineLhs;

  boost::shared_ptr<FaceEle> initialPipeLine;

  boost::shared_ptr<EdgeEle> skeletonPipeline;

  boost::shared_ptr<FaceEle> facePipelineMaxPostError;

  MoFEM::Interface &mField;
  Simple *simpleInterface;
  SmartPetscObj<DM> dM;
  SmartPetscObj<TS> tS;
  MPI_Comm mpiComm;
  const int mpiRank;

  std::map<int, BlockData> materialBlocks;
  std::map<int, Range> initialSurfaces;
  Range essentialBdryEnts;

  boost::shared_ptr<PostProcFaceOnRefinedMeshFor2D> postProcFE;
  boost::shared_ptr<Monitor<nbSpecies>> monitorPtr;

  std::vector<boost::shared_ptr<PreviousData>> dataAtGP;
  std::vector<boost::shared_ptr<MatrixDouble>> fluxValuePtrs;
  std::vector<boost::shared_ptr<VectorDouble>> fluxDivPtrs;
  std::vector<boost::shared_ptr<VectorDouble>> massValuePtrs;
  std::vector<boost::shared_ptr<VectorDouble>> massDotPtrs;
};

const double ramp_t = 1.0;
const double sml = 0.0;
const double T = M_PI / 2.0;
const double R = 0.75;



struct ExactFunction {
  double operator()(const double x, const double y, const double t) const {
    // double g = cos(T * x) * cos(T * y);

    // //##################################################
    double g = 0;
    double r = x * x + y * y;
    double d = R * R - r;

    if (d > 0.0) {
      g = exp(-r / d);
    }
    // //###################################################
    if (t <= ramp_t) {
      return g * t;
    } else {
      return g * ramp_t;
    }
  }
};

struct ExactFunctionGrad {
  FTensor::Tensor1<double, 3> operator()(const double x, const double y,
                                         const double t) const {
    FTensor::Tensor1<double, 3> grad;
    // double mx = - T * sin(T * x) * cos(T * y);
    // double my = - T * cos(T * x) * sin(T * y);

    //##################################################
    double mx = 0.0;
    double my = 0.0;
    double r = x * x + y * y;
    double d = R * R - r;

    if (d > 0.0) {
      double rx = 2.0 * x;
      double ry = 2.0 * y;

      double g = exp(-r / d);

      mx = g * (-rx * R * R / (d * d));
      my = g * (-ry * R * R / (d * d));
    }
    //##################################################

    if (t <= ramp_t) {
      grad(0) = mx * t;
      grad(1) = my * t;
    } else {
      grad(0) = mx * ramp_t;
      grad(1) = my * ramp_t;
    }
    grad(2) = 0.0;
    return grad;
  }
};

struct ExactFunctionLap {
  double operator()(const double x, const double y, const double t) const {
    // double glap = -2.0 * pow(T, 2) * cos(T * x) * cos(T * y);

    // //##################################################
    double glap = 0.0;
    double r = x * x + y * y;
    double d = R * R - r;
    if (d > 0.0) {
      double rx = 2.0 * x;
      double ry = 2.0 * y;

      double rxx = 2.0;
      double ryy = 2.0;
      double g = exp(-r / d);

      double cof = R * R / (d * d);
      glap = g * ((-rx * cof) * (-rx * cof) + (-ry * cof) * (-ry * cof) -
                  R * R * (rxx * d + 2.0 * rx * rx) / (d * d * d) -
                  R * R * (ryy * d + 2.0 * ry * ry) / (d * d * d));
    }
    // //##################################################
    if (t <= ramp_t) {
      return glap * t;
    } else {
      return glap * ramp_t;
    }
  }
};

struct ExactFunctionDot {
  double operator()(const double x, const double y, const double t) const {
    // double gdot = cos(T * x) * cos(T * y);
    // //##################################################
    double gdot = 0;
    double r = x * x + y * y;
    double d = R * R - r;

    if (d > 0.0) {
      gdot = exp(-r / d);
    }
    // //##################################################
    if (t <= ramp_t) {
      return gdot;
    } else {
      return 0;
    }
  }
};

// member function implementations

template <int nbSpecies>
MoFEMErrorCode RDProblem<nbSpecies>::runAnalysis(){
  MoFEMFunctionBegin;
  CHKERR readMesh();
  CHKERR setupSystem();
  CHKERR setIntegrationRule();

  CHKERR setupBC();
  CHKERR setupIC();

  CHKERR setupPipeline();
  CHKERR pushUDOperators();

  CHKERR assembleSystem();
  CHKERR setOutput();
  CHKERR solveSystem();
  MoFEMFunctionReturn(0);
}

template <int nbSpecies>
RDProblem<nbSpecies>::RDProblem(MoFEM::Core &core)
    : mField(core), mpiComm(mField.get_comm()),
      mpiRank(mField.get_comm_rank()) {
  facePipelineNStiffRhs = boost::shared_ptr<FaceEle>(new FaceEle(mField));
  facePipelineStiffRhs = boost::shared_ptr<FaceEle>(new FaceEle(mField));
  facePipelineLhs = boost::shared_ptr<FaceEle>(new FaceEle(mField));

  initialPipeLine = boost::shared_ptr<FaceEle>(new FaceEle(mField));

  skeletonPipeline = boost::shared_ptr<EdgeEle>(new EdgeEle(mField));
  facePipelineMaxPostError = boost::shared_ptr<FaceEle>(new FaceEle(mField));

  postProcFE = boost::shared_ptr<PostProcFaceOnRefinedMeshFor2D>(
      new PostProcFaceOnRefinedMeshFor2D(mField));

  for (int ns = 0; ns < nbSpecies; ns++) {
    dataAtGP.push_back(boost::shared_ptr<PreviousData>(new PreviousData()));

    fluxValuePtrs.push_back(boost::shared_ptr<MatrixDouble>(
        dataAtGP[ns], &dataAtGP[ns]->flux_values));

    fluxDivPtrs.push_back(boost::shared_ptr<VectorDouble>(
        dataAtGP[ns], &dataAtGP[ns]->flux_divs));

    massValuePtrs.push_back(boost::shared_ptr<VectorDouble>(
        dataAtGP[ns], &dataAtGP[ns]->mass_values));

    massDotPtrs.push_back(boost::shared_ptr<VectorDouble>(
        dataAtGP[ns], &dataAtGP[ns]->mass_dots));

        
  }

}

template <int nbSpecies> 
MoFEMErrorCode RDProblem<nbSpecies>::readMesh() {
  MoFEMFunctionBegin;
  // cout << "No. species : " << nbSpecies << endl;
  for (int ns = 0; ns < nbSpecies; ++ns) {
    auto m_name = "M" + boost::lexical_cast<std::string>(ns + 1);
    // cout << "m_name : " << m_name << endl;
    auto f_name = "F" + boost::lexical_cast<std::string>(ns + 1);
    mass_names.push_back(m_name);
    flux_names.push_back(f_name);
  }

  CHKERR mField.getInterface(simpleInterface);
  CHKERR simpleInterface->getOptions();
  CHKERR simpleInterface->loadFile();

  int space_dim = 2;
  for (_IT_CUBITMESHSETS_BY_SET_TYPE_FOR_LOOP_(mField, BLOCKSET, it)) {
    string name = it->getName();
    const int id = it->getMeshsetId();
    if (name.compare(0, 6, "REGION") == 0) {
      CHKERR mField.getInterface<MeshsetsManager>()->getEntitiesByDimension(
          id, BLOCKSET, space_dim, materialBlocks[id].block_ents, true);

      std::vector<double> mat_properties;
      it->getAttributes(mat_properties);
      double D = mat_properties[0];
      CHKERR PetscOptionsGetReal(PETSC_NULL, "", "-conductivity", &D, PETSC_NULL);
      materialBlocks[id].B0 = D;
    }
  }

  int o_rder = 1;
  CHKERR PetscOptionsGetInt(PETSC_NULL, "", "-tag_order", &o_rder, PETSC_NULL);
  CHKERR mField.get_moab().tag_get_handle(
      "_ORDER", 1, MB_TYPE_INTEGER, maxOrder, MB_TAG_CREAT | MB_TAG_DENSE, &o_rder);

  int r_ule = 1;
  CHKERR PetscOptionsGetInt(PETSC_NULL, "", "-tag_rule", &r_ule, PETSC_NULL);
  CHKERR mField.get_moab().tag_get_handle("_RULE", 1, MB_TYPE_INTEGER, quadRule,
                                           MB_TAG_CREAT | MB_TAG_DENSE, &r_ule);

  MoFEMFunctionReturn(0);
}

template <int nbSpecies> 
MoFEMErrorCode RDProblem<nbSpecies>::setupSystem() {
  MoFEMFunctionBegin;
  CHKERR simpleInterface->addDomainField("M", L2, AINSWORTH_LEGENDRE_BASE, 1);
  CHKERR simpleInterface->addDomainField("F", HCURL, DEMKOWICZ_JACOBI_BASE, 1);

  CHKERR simpleInterface->addSkeletonField("F", HCURL, DEMKOWICZ_JACOBI_BASE, 1);

  CHKERR simpleInterface->addDataField("PRERROR", L2, AINSWORTH_LEGENDRE_BASE, 1);
  CHKERR simpleInterface->addDataField("POERROR", L2, AINSWORTH_LEGENDRE_BASE, 1);

  CHKERR PetscOptionsGetInt(PETSC_NULL, "", "-order", &g_max_order, PETSC_NULL);

  CHKERR simpleInterface->setFieldOrder("M", g_max_order - 1);
  CHKERR simpleInterface->setFieldOrder("F", g_max_order);

  CHKERR simpleInterface->setFieldOrder("PRERROR",
                                        0); // approximation order for error
  CHKERR simpleInterface->setFieldOrder("POERROR", 0);

  for (int ns = 0; ns < nbSpecies; ns++) {
    CHKERR simpleInterface->addDataField(mass_names[ns], L2, AINSWORTH_LEGENDRE_BASE, 1);
    CHKERR simpleInterface->addDataField(flux_names[ns], HCURL, DEMKOWICZ_JACOBI_BASE, 1);

    CHKERR simpleInterface->setFieldOrder(mass_names[ns], g_max_order - 1);
    CHKERR simpleInterface->setFieldOrder(flux_names[ns], g_max_order);
  }

  CHKERR simpleInterface->setUp();


  MoFEMFunctionReturn(0);
}

template <int nbSpecies> 
MoFEMErrorCode RDProblem<nbSpecies>::setIntegrationRule(){
  MoFEMFunctionBegin;

  auto set_GP_faceRule = [&](boost::shared_ptr<FaceEle> &fe) {

    auto fe_rule = [&](int, int, int) -> int {
      auto fe_ent = fe->getFEEntityHandle();
      int quad_rule;
      CHKERR mField.get_moab().tag_get_data(quadRule, &fe_ent, 1,
                                              &quad_rule);

      return 2 * (quad_rule + 0);
    };
    fe->getRuleHook = fe_rule;
  };

  auto set_GP_edgeRule = [&](boost::shared_ptr<EdgeEle> &fe) {
    auto fe_rule = [&](int, int, int) -> int {
      auto fe_ent = fe->getFEEntityHandle();
      int quad_rule;
      CHKERR mField.get_moab().tag_get_data(quadRule, &fe_ent, 1, &quad_rule);

      return 2 * (quad_rule + 0);
    };
    fe->getRuleHook = fe_rule;
  };

  set_GP_faceRule(facePipelineNStiffRhs);
  set_GP_faceRule(facePipelineStiffRhs);
  set_GP_faceRule(facePipelineLhs);

  set_GP_edgeRule(skeletonPipeline);

  set_GP_faceRule(initialPipeLine);

  dM = simpleInterface->getDM();
  tS = createTS(mField.get_comm());

  MoFEMFunctionReturn(0);
}

template <int nbSpecies> 
MoFEMErrorCode RDProblem<nbSpecies>::setupIC(){
  MoFEMFunctionBegin;
  const int space_dim = 2; 
  int ns = 0;
  for (_IT_CUBITMESHSETS_BY_SET_TYPE_FOR_LOOP_(mField, BLOCKSET, it)){
    string name = it->getName();
    const int id = it->getMeshsetId();
    if (name.compare(0, 7, "INITIAL") == 0) {
        CHKERR mField.getInterface<MeshsetsManager>()->getEntitiesByDimension(
            id, BLOCKSET, space_dim, initialSurfaces[id], true);
        cout << "Inner Ents : " << initialSurfaces[id] << endl;
        initialPipeLine->getOpPtrVector().push_back(
            new OpInitialMass(mass_names[ns], initialSurfaces[id], mField));
        ns++;
    }   
  }
  CHKERR DMoFEMLoopFiniteElements(dM, simpleInterface->getDomainFEName(),
                                  initialPipeLine);
  MoFEMFunctionReturn(0);
}

template <int nbSpecies> 
MoFEMErrorCode RDProblem<nbSpecies>::setupBC(){
  MoFEMFunctionBegin;
  const int space_dim = 2;
  for (_IT_CUBITMESHSETS_BY_SET_TYPE_FOR_LOOP_(mField, BLOCKSET, it)) {
    string name = it->getName();
    const int id = it->getMeshsetId();
    if (name.compare(0, 9, "ESSENTIAL") == 0) {
      CHKERR mField.getInterface<MeshsetsManager>()->getEntitiesByDimension(
          id, BLOCKSET, space_dim - 1, essentialBdryEnts, true);
    }
  }

  CHKERR mField.getInterface<ProblemsManager>()->removeDofsOnEntities(
      simpleInterface->getProblemName(), "F", essentialBdryEnts);
  MoFEMFunctionReturn(0);
}

template <int nbSpecies>
MoFEMErrorCode RDProblem<nbSpecies>::setupPipeline(){
  MoFEMFunctionBegin;

  auto set_jac = [&](boost::shared_ptr<FaceEle> fe_pipeline) {
    fe_pipeline->getOpPtrVector().push_back(
        new OpCalculateJacForFace(jac));
    fe_pipeline->getOpPtrVector().push_back(
        new OpCalculateInvJacForFace(inv_jac));
    fe_pipeline->getOpPtrVector().push_back(
        new OpMakeHdivFromHcurl());
    fe_pipeline->getOpPtrVector().push_back(
        new OpSetContravariantPiolaTransformFace(jac));
    fe_pipeline->getOpPtrVector().push_back(
        new OpSetInvJacHcurlFace(inv_jac));
  };

  set_jac(facePipelineNStiffRhs);
  set_jac(facePipelineStiffRhs);
  set_jac(facePipelineLhs);

  // set_jac(postProcFE);

  skeletonPipeline->getOpPtrVector().push_back(
      new OpSetContrariantPiolaTransformOnEdge());

  MoFEMFunctionReturn(0);
}

template <int nbSpecies>
MoFEMErrorCode RDProblem<nbSpecies>::pushUDOperators(){
  MoFEMFunctionBegin;
  // facePipelineNStiffRhs
  // facePipelineNStiffRhs->getOpPtrVector().push_back(
  //     new OpCalculateJacForFace(jac));

  // facePipelineNStiffRhs->getOpPtrVector().push_back(
  //     new OpCalculateInvJacForFace(inv_jac));

  // // facePipelineNStiffRhs->getOpPtrVector().push_back(
  // //     new OpMakeHdivFromHcurl());

  // // facePipelineNStiffRhs->getOpPtrVector().push_back(
  // //     new OpSetContravariantPiolaTransformFace(jac));

  // // facePipelineNStiffRhs->getOpPtrVector().push_back(
  // //     new OpSetInvJacHcurlFace(inv_jac));

  // // facePipelineNStiffRhs
  // facePipelineStiffRhs->getOpPtrVector().push_back(
  //     new OpCalculateJacForFace(jac));

  // facePipelineStiffRhs->getOpPtrVector().push_back(
  //     new OpCalculateInvJacForFace(inv_jac));

  // facePipelineStiffRhs->getOpPtrVector().push_back(
  //     new OpMakeHdivFromHcurl());

  // facePipelineStiffRhs->getOpPtrVector().push_back(
  //     new OpSetContravariantPiolaTransformFace(jac));

  // facePipelineStiffRhs->getOpPtrVector().push_back(
  //     new OpSetInvJacHcurlFace(inv_jac));

  // // facePipelineLhs
  // facePipelineLhs->getOpPtrVector().push_back(
  //    new OpCalculateJacForFace(jac));

  // facePipelineLhs->getOpPtrVector().push_back(
  //     new OpCalculateInvJacForFace(inv_jac));

  // facePipelineLhs->getOpPtrVector().push_back(
  //     new OpMakeHdivFromHcurl());

  // facePipelineLhs->getOpPtrVector().push_back(
  //     new OpSetContravariantPiolaTransformFace(jac));

  // facePipelineLhs->getOpPtrVector().push_back(
  //     new OpSetInvJacHcurlFace(inv_jac));

  for(int ns = 0; ns < nbSpecies; ns++){
    facePipelineNStiffRhs->getOpPtrVector().push_back(
        new OpCalculateScalarFieldValues(mass_names[ns], massValuePtrs[ns], MBTRI));

    facePipelineStiffRhs->getOpPtrVector().push_back(
        new OpCalculateScalarFieldValues(mass_names[ns], massValuePtrs[ns], MBTRI));

    facePipelineStiffRhs->getOpPtrVector().push_back(
        new OpCalculateHdivVectorField<3>(flux_names[ns], fluxValuePtrs[ns]));

    facePipelineStiffRhs->getOpPtrVector().push_back(
        new OpCalculateScalarValuesDot(mass_names[ns], massDotPtrs[ns], MBTRI));

    facePipelineStiffRhs->getOpPtrVector().push_back(
        new OpCalculateHdivVectorDivergence<3, 2>(flux_names[ns], fluxDivPtrs[ns]));

    facePipelineLhs->getOpPtrVector().push_back(
        new OpCalculateScalarFieldValues(mass_names[ns], massValuePtrs[ns]));

    facePipelineLhs->getOpPtrVector().push_back(
        new OpCalculateHdivVectorField<3>(flux_names[ns], fluxValuePtrs[ns]));
  }

  facePipelineNStiffRhs->getOpPtrVector().push_back(
      new OpAssembleSlowRhsV("M", dataAtGP, nbSpecies, materialBlocks, 
                               ExactFunction(), ExactFunctionDot(), 
                               ExactFunctionLap(), mField));

  facePipelineStiffRhs->getOpPtrVector().push_back(
      new OpAssembleStiffRhsTau<3>("F", dataAtGP, nbSpecies,
                                     materialBlocks, mField));

  facePipelineStiffRhs->getOpPtrVector().push_back(
    new OpAssembleStiffRhsV<3>("M", dataAtGP, nbSpecies, materialBlocks, 
                                 ExactFunction(), ExactFunctionDot(), 
                                 ExactFunctionLap(), mField));

  facePipelineLhs->getOpPtrVector().push_back(
    new OpAssembleLhsTauTau<3>("F", dataAtGP, 
                                 materialBlocks, nbSpecies, mField));

  facePipelineLhs->getOpPtrVector().push_back(
      new OpAssembleLhsVV("M", mField));

  facePipelineLhs->getOpPtrVector().push_back(
      new OpAssembleLhsTauV<3>("F", "M", dataAtGP, 
                                 materialBlocks, nbSpecies, mField));

  facePipelineLhs->getOpPtrVector().push_back(
      new OpAssembleLhsVTau("M", "F", mField));

  MoFEMFunctionReturn(0);
}

template <int nbSpecies>
MoFEMErrorCode RDProblem<nbSpecies>::assembleSystem(){
  MoFEMFunctionBegin;
  CHKERR TSSetType(tS, TSARKIMEX);
  CHKERR TSARKIMEXSetType(tS, TSARKIMEXA2);

  boost::shared_ptr<ForcesAndSourcesCore> null;

  CHKERR DMMoFEMTSSetIJacobian(dM, simpleInterface->getDomainFEName(),
                               facePipelineLhs, null, null);

  CHKERR DMMoFEMTSSetIFunction(dM, simpleInterface->getDomainFEName(),
                               facePipelineStiffRhs, null, null);

  CHKERR DMMoFEMTSSetRHSFunction(dM, simpleInterface->getDomainFEName(),
                                 facePipelineNStiffRhs, null, null);

  CHKERR DMMoFEMTSSetRHSFunction(dM, simpleInterface->getSkeletonFEName(),
                                 skeletonPipeline, null, null);

  MoFEMFunctionReturn(0);
}

template <int nbSpecies>
MoFEMErrorCode RDProblem<nbSpecies>::setOutput(){
  MoFEMFunctionBegin;

  postProcFE->generateReferenceElementMesh();

  postProcFE->getOpPtrVector().push_back(
    new OpCalculateJacForFace(jac));

  postProcFE->getOpPtrVector().push_back(
    new OpCalculateInvJacForFace(inv_jac));

  postProcFE->getOpPtrVector().push_back(
    new OpMakeHdivFromHcurl());

  postProcFE->getOpPtrVector().push_back(
    new OpSetContravariantPiolaTransformFace(jac));

  postProcFE->getOpPtrVector().push_back(
    new OpSetInvJacHcurlFace(inv_jac));



  for(int ss = 0; ss < nbSpecies; ss++){
    postProcFE->getOpPtrVector().push_back(
        new OpCalculateScalarFieldValues(
        mass_names[ss], massValuePtrs[ss], MBTRI));
    // postProcFE->getOpPtrVector().push_back(
    //     new OpCalculateHdivVectorField<3>(flux_names[ss], fluxValuePtrs[ss]));

    postProcFE->addFieldValuesPostProc(mass_names[ss]);
  }

  maxError = 0;
  facePipelineMaxPostError->getOpPtrVector().push_back(new OpMaxError("POERROR", maxError));
  monitorPtr = boost::shared_ptr<Monitor<nbSpecies>>(
      new Monitor<nbSpecies>(mpiComm, mpiRank, dM, tS, facePipelineMaxPostError, postProcFE,
                  mField));

  boost::shared_ptr<ForcesAndSourcesCore> null;

  CHKERR DMMoFEMTSSetMonitor(dM, tS, simpleInterface->getDomainFEName(),
                             monitorPtr, null, null);

  MoFEMFunctionReturn(0);
}

template <int nbSpecies>
MoFEMErrorCode RDProblem<nbSpecies>::solveSystem(){
  MoFEMFunctionBegin;
  // Create solution vector
  SmartPetscObj<Vec> X;
  CHKERR DMCreateGlobalVector_MoFEM(dM, X);
  CHKERR DMoFEMMeshToLocalVector(dM, X, INSERT_VALUES, SCATTER_FORWARD);
  // Solve problem
  double ftime = 1;
  CHKERR TSSetDM(tS, dM);
  CHKERR TSSetMaxTime(tS, ftime);
  CHKERR TSSetSolution(tS, X);
  CHKERR TSSetFromOptions(tS);

  if (0) {
    SNES snes;
    CHKERR TSGetSNES(tS, &snes);
    KSP ksp;
    CHKERR SNESGetKSP(snes, &ksp);
    PC pc;
    CHKERR KSPGetPC(ksp, &pc);
    PetscBool is_pcfs = PETSC_FALSE;
    PetscObjectTypeCompare((PetscObject)pc, PCFIELDSPLIT, &is_pcfs);
    // Set up FIELDSPLIT
    // Only is user set -pc_type fieldsplit
    if (is_pcfs == PETSC_TRUE) {
      IS is_mass, is_flux;
      const MoFEM::Problem *problem_ptr;
      CHKERR DMMoFEMGetProblemPtr(dM, &problem_ptr);
      CHKERR
      mField.getInterface<ISManager>()->isCreateProblemFieldAndRank(
          problem_ptr->getName(), ROW, "M", 0, 1, &is_mass);
      CHKERR
      mField.getInterface<ISManager>()->isCreateProblemFieldAndRank(
          problem_ptr->getName(), ROW, "F", 0, 1, &is_flux);
      // CHKERR ISView(is_flux, PETSC_VIEWER_STDOUT_SELF);
      // CHKERR ISView(is_mass, PETSC_VIEWER_STDOUT_SELF);

      CHKERR PCFieldSplitSetIS(pc, NULL, is_mass);
      CHKERR PCFieldSplitSetIS(pc, NULL, is_flux);

      CHKERR ISDestroy(&is_flux);
      CHKERR ISDestroy(&is_mass);
    }
  }

  CHKERR TSSolve(tS, X);
  MoFEMFunctionReturn(0);
}


int main(int argc, char *argv[]) {
  const char param_file[] = "param_file.petsc";
  MoFEM::Core::Initialize(&argc, &argv, param_file, help);
  try {
    moab::Core mb_instance;
    moab::Interface &moab = mb_instance;
    MoFEM::Core core(moab);
    DMType dm_name = "DMMOFEM";
    CHKERR DMRegister_MoFEM(dm_name);

    RDProblem<1> reac_diff_problem(core);
    CHKERR reac_diff_problem.runAnalysis();
  }
  CATCH_ERRORS;
  MoFEM::Core::Finalize();
  return 0;
}