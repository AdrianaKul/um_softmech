#ifndef __ERRORESTIMATEDRAFTS_HPP__
#define __ERRORESTIMATEDRAFTS_HPP__

#include <stdlib.h>
#include <BasicFiniteElements.hpp>
#include <MFOperators.hpp>

using namespace ReactionDiffusion;

namespace ErrorEstimates {


using FaceEleOnSide = MoFEM::FaceElementForcesAndSourcesCoreOnSideSwitch<
                      FaceElementForcesAndSourcesCore::NO_CONTRAVARIANT_TRANSFORM_HDIV |
                      FaceElementForcesAndSourcesCore::NO_COVARIANT_TRANSFORM_HCURL>;




using OpFaceEleOnSide = FaceEleOnSide::UserDataOperator;




struct JumpData{
  VectorDouble LFaceVaues;
  VectorDouble RFaceVaues;
  int LeftP;
  int RightP;
};

// struct OpSetEdgeOrder : OpEdgeEle {
//   struct OpGetAdjFaceOrder : OpFaceEleOnSide {
//     OpGetAdjFaceOrder(std::string error_field,
//                       )
//         : OpFaceEleOnSide(error_field, OpFaceEleOnSide::OPROW) {}
//     MoFEMErrorCode doWork(int            side,
//                           EntityType     type,
//                           EntData        &data){
//       MoFEMFunctionBeginHot;
//       if(type ==  MBTRI){
//         data.getFieldDofs()[0]->getFieldData() = 0.0;
//       }
//       MoFEMFunctionReturnHot(0);
//     }
//   };
//   FaceEleOnSide preErrorFaceSideFe;
//   std::string   errorFieldName;

//   OpZeroDof(std::string      skeleton_field_name, 
//             std::string      error_field_name, 
//             MoFEM::Interface &m_field)
//       : OpEdgeEle(skeleton_field_name, OpEdgeEle::OPROW)
//       , errorFieldName(error_field_name)
//       , preErrorFaceSideFe(m_field) {
//     preErrorFaceSideFe.getOpPtrVector().push_back(
//         new OpSetZeroDofs(errorFieldName));
//   }
//     MoFEMErrorCode doWork(int        side, 
//                           EntityType type,
//                           EntData    &data){
//       MoFEMFunctionBeginHot;

//       if (type == MBEDGE){

//         CHKERR loopSideFaces("dFE", preErrorFaceSideFe);
//       }
//       MoFEMFunctionReturnHot(0);
//     }
// };

struct OpCalculateJumpErrors : OpEdgeEle {

  struct OpCalculateTraces : OpFaceEleOnSide {
    JumpData &jumpData;
    OpCalculateTraces(std::string domain_field_name, JumpData &jump_data)
        : jumpData(jump_data),
          OpFaceEleOnSide(domain_field_name, OpFaceEleOnSide::OPROW) {}
    MoFEMErrorCode doWork(int side, EntityType type, EntData &data) {
      MoFEMFunctionBeginHot;
      if (type == MBTRI) {

        // const size_t nb_integration_pts = data.getN().size1();
        const int nb_integration_pts = getGaussPts().size2();

        auto t_shapeValue = data.getFTensor0N();

        VectorDouble *ptr_elem_data = nullptr;            
        if (getFEMethod()->nInTheLoop == 0)
          ptr_elem_data = &jumpData.LFaceVaues;
        else
          ptr_elem_data = &jumpData.RFaceVaues;

        VectorDouble &elem_data = *ptr_elem_data;
        // cout << "no. integration pts : " << nb_integration_pts << endl;
        elem_data.resize(nb_integration_pts, false);

        // cout << "no. integration pts : " << nb_integration_pts << endl;

        for (size_t gg = 0; gg != nb_integration_pts; ++gg) {
          elem_data[gg] = inner_prod(trans(data.getN(gg)), data.getFieldData());
        }
      }
      MoFEMFunctionReturnHot(0);
    }
  };
  struct OpSetErrorDofs : OpFaceEleOnSide {
    double &edgeJumpError;
    OpSetErrorDofs(std::string error_field, double &edge_jump_error)
        : edgeJumpError(edge_jump_error),
          OpFaceEleOnSide(error_field, OpFaceEleOnSide::OPROW) {}
    MoFEMErrorCode doWork(int side, EntityType type, EntData &data) {
      MoFEMFunctionBeginHot;
      if (type == MBTRI) {
        const double vol = getMeasure();
        data.getFieldDofs()[0]->getFieldData() += sqrt(edgeJumpError / sqrt(vol));
      }
      MoFEMFunctionReturnHot(0);
    }
  };
  FaceEleOnSide preErrorFaceSideFe;
  FaceEleOnSide postErrorFaceSideFe;
  MatrixDouble jac;
  JumpData &jumpData;
  double edgeJumpError;
  std::string domainFieldName;
  std::string errorFieldName;
  double &cumJumpError;

  OpCalculateJumpErrors(std::string      skeleton_field_name,
                        std::string      domain_field_name,
                        std::string      error_field_name, 
                        MoFEM::Interface &m_field,
                        JumpData         &jump_data,
                        double           &cum_jump_error)
      : OpEdgeEle(skeleton_field_name, OpEdgeEle::OPROW)
      , domainFieldName(domain_field_name)
      , errorFieldName(error_field_name)
      , jumpData(jump_data)
      , preErrorFaceSideFe(m_field)
      , postErrorFaceSideFe(m_field)
      , cumJumpError(cum_jump_error) {
    preErrorFaceSideFe.getOpPtrVector().push_back(
        new OpCalculateJacForFace(jac));
    postErrorFaceSideFe.getOpPtrVector().push_back(
        new OpCalculateJacForFace(jac));

    preErrorFaceSideFe.getOpPtrVector().push_back(
        new OpCalculateTraces(domainFieldName, jumpData));
    postErrorFaceSideFe.getOpPtrVector().push_back(
        new OpSetErrorDofs(errorFieldName, edgeJumpError));
  }
  MoFEMErrorCode doWork(int side, EntityType type, EntData &data) {
    MoFEMFunctionBeginHot;
    if (type == MBEDGE) {

      const size_t nb_integration_pts = data.getN().size1();

      jumpData.RFaceVaues.resize(0, false);
      jumpData.LFaceVaues.resize(0, false);

      CHKERR loopSideFaces("dFE", preErrorFaceSideFe);

      edgeJumpError = 0.0;

      const double vol = getMeasure();
      auto t_w = getFTensor0IntegrationWeight();

      if (jumpData.RFaceVaues.size() != 0 && jumpData.LFaceVaues.size() != 0) {
        
        for (size_t gg = 0; gg != nb_integration_pts; gg++) {
          double a = t_w * vol;
          edgeJumpError +=
              a * pow(jumpData.RFaceVaues[gg] - jumpData.LFaceVaues[gg], 2);
          ++t_w;
        }
        cumJumpError += edgeJumpError / vol;

        CHKERR loopSideFaces("dFE", postErrorFaceSideFe);
      }

      
    }
    MoFEMFunctionReturnHot(0);
  }

  
};
template<int spaceDim>
struct OpCalculateGradL2 : public OpFaceEle {

  OpCalculateGradL2(std::string field_name, // L2 field
                    MatrixDouble &inv_jac, MatrixDouble &grad_val)
      : OpFaceEle(field_name, OpFaceEle::OPROW), invJac(inv_jac),
        gradVal(grad_val) {}

  MoFEMErrorCode doWork(int side, EntityType type, EntData &data) {
    MoFEMFunctionBegin;
    if (type == MBTRI) {
      const int nb_dofs = data.getIndices().size();
      const int nb_integration_pts = getGaussPts().size2();
      gradVal.resize(spaceDim, nb_integration_pts, false);
      gradVal.clear();

      FTensor::Tensor2<FTensor::PackPtr<double *, 1>, 2, 2> t_inv_jac(
                                              &invJac(0, 0), &invJac(1, 0), 
                                              &invJac(2, 0), &invJac(3, 0));

      auto t_newdiff = FTensor::Tensor1<double, 3>(0.0, 0.0, 0.0);
      

      auto t_diffN = data.getFTensor1DiffN<spaceDim>();
      auto t_grad_val = getFTensor1FromMat<spaceDim>(gradVal);

      FTensor::Index<'I', spaceDim> I;
      FTensor::Index<'J', spaceDim> J;

      // cout << "Inv Jac : " << invJac << endl;

      for (size_t gg = 0; gg != nb_integration_pts; gg++) {
        auto t_field_data = data.getFTensor0FieldData();
        for (size_t dd = 0; dd != nb_dofs;
                                             dd++) {
          t_newdiff(I) = t_inv_jac(J, I) * t_diffN(J);
          t_grad_val(I) += t_field_data * t_newdiff(I);
          ++t_field_data;
          ++t_diffN;
        }
        ++t_grad_val;
      }
    }

    MoFEMFunctionReturn(0);
  }

private:
  MatrixDouble &invJac;
  MatrixDouble &gradVal;
};

struct OpMaxError : public OpFaceEle {

  OpMaxError(std::string post_field_name, double &max_error)
      : OpFaceEle(post_field_name, OpFaceEle::OPROW), maxError(max_error) {}
  MoFEMErrorCode doWork(int side, EntityType type, EntData &data) {
    MoFEMFunctionBegin;
    if (type == MBTRI) {
      double err = data.getFieldDofs()[0]->getFieldData();
      maxError = (maxError >= err) ? maxError : err;
    }
    MoFEMFunctionReturn(0);
  }

private:
  double &maxError;
};


struct Padaptive : public FEMethod {

  Padaptive(MPI_Comm                   &mpi_comm, 
            const int                  &mpi_rank,
            SmartPetscObj<DM>          &petsc_dm, 
            MoFEM::Interface           &m_field,
            boost::shared_ptr<EdgeEle> &skeleton_pipeline,
            boost::shared_ptr<FaceEle> &domain_pipeline, 
            double                     &post_error,
            double                     &max_error)
      : mpiComm(mpi_comm)
      , mpiRank(mpi_rank)
      , petscDm(petsc_dm)
      , skeletonPipeline(skeleton_pipeline)
      , domainPipeline(domain_pipeline)
      , postError(post_error)
      , maxError(max_error)
      , mField(m_field) {}

  MoFEMErrorCode preProcess() {
    MoFEMFunctionBegin;
    
    MoFEMFunctionReturn(0);
  }
  MoFEMErrorCode operator()() { return 0; }

  MoFEMErrorCode postProcess() {
    MoFEMFunctionBegin;
      for (_IT_GET_DOFS_FIELD_BY_NAME_FOR_LOOP_(mField, "ERROR2", dof)) {
        dof->get()->getFieldData() = 0.0;
      }
      CHKERR DMoFEMLoopFiniteElements(petscDm, "dFE", domainPipeline);
      CHKERR DMoFEMLoopFiniteElements(petscDm, "sFE", skeletonPipeline);

      auto gather_errors = [&](double &global_error,
                               Vec    &vector_proc_error) {
        MoFEMFunctionBegin;
        global_error = 0;
        CHKERR VecCreateMPI(mpiComm, 1, PETSC_DECIDE, &vector_proc_error);
        CHKERR VecSetValue(vector_proc_error, mpiRank, global_error, INSERT_VALUES);

        CHKERR VecAssemblyBegin(vector_proc_error);
        CHKERR VecAssemblyEnd(vector_proc_error);

        MoFEMFunctionReturn(0);
      };
      double error_sum = 0;
      double error_max = 0;

      Vec vector_error_sum_proc;
      Vec vector_error_max_proc;

      CHKERR gather_errors(postError, vector_error_sum_proc);
      CHKERR gather_errors(maxError, vector_error_max_proc);

      CHKERR VecSum(vector_error_sum_proc, &error_sum);
      CHKERR VecMax(vector_error_max_proc, PETSC_NULL, &error_max);

      CHKERR PetscPrintf(PETSC_COMM_WORLD, "Post error      : %3.10e \n", sqrt(error_sum));
      CHKERR PetscPrintf(PETSC_COMM_WORLD, "Max local error : %3.10e \n", error_max);



    MoFEMFunctionReturn(0);
  }

private:
  double                     &postError;
  double                     &maxError;
  SmartPetscObj<DM>          petscDm;
  MoFEM::Interface           &mField;
  boost::shared_ptr<EdgeEle> skeletonPipeline;
  boost::shared_ptr<FaceEle> domainPipeline;
  MPI_Comm                   mpiComm;
  const int                  mpiRank;
};

};     // namespace ErrorEstimates
#endif //__ERRORESTIMATEDRAFTS_HPP__