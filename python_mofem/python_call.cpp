#include <stdlib.h>
#include <Python/Python.h>
#include "pyhelper.hpp"
#include <BasicFiniteElements.hpp>

static char help[] = "...\n\n";

double ZERO_CONST = 0.0;

template<int dim>
using pFTGP = FTensor::Tensor2<FTensor::PackPtr<double *, 1>, dim, dim>;
template <int Tensor_Dim>
using pFT1_t = FTensor::Tensor1<double *, Tensor_Dim>;

inline pFTGP<3>
getFTensorDiag(MatrixDouble &data){
  return pFTGP<3>(&data(0, 0), &data(1, 0), &data(1, 0),
                  &data(1, 0), &data(0, 0), &data(1, 0),
                  &data(1, 0), &data(1, 0), &data(0, 0));
}

FTensor::Tensor2<double  *, 3, 3> getFTensor2Diag(VectorDouble &data, int pp){
  return FTensor::Tensor2<double *, 3, 3>(&data[pp], &ZERO_CONST, &ZERO_CONST,
                                          &ZERO_CONST, &data[pp], &ZERO_CONST,
                                          &ZERO_CONST, &ZERO_CONST, &data[pp]);
}

FTensor::Index<'i', 3> i;
FTensor::Index<'j', 3> j;
struct CommonData {
  boost::shared_ptr<MatrixDouble> matPtr;
  boost::shared_ptr<VectorDouble> vecPtr;

  CommonData(){
    matPtr = boost::shared_ptr<MatrixDouble>(new MatrixDouble());
    vecPtr = boost::shared_ptr<VectorDouble>(new VectorDouble());
  }
};


struct RhsFunc{
  RhsFunc(boost::shared_ptr<CommonData> common_data_ptr) 
  : commonDataPtr(common_data_ptr){}
 void operator()(MatrixDouble &outMat, const int nb_gp){
   cout << "inside operator()" << endl;
   outMat.resize(3, nb_gp, false);
   outMat.clear();
   auto t_inmat = getFTensor1FromMat<3>(*commonDataPtr->matPtr);
   auto t_outmat = getFTensor1FromMat<3>(outMat);
   for(int gg = 0; gg != nb_gp; ++gg){
     t_outmat(i) = (gg + 1.0) * t_inmat(i);
     ++t_inmat;
     ++t_outmat;
   } 
 }
  private:
    boost::shared_ptr<CommonData> commonDataPtr;
};


struct CallFun {
typedef boost::function<void(int &)> MatFun;
typedef boost::function<void(double &)> VecFun;

public:
  CallFun(MatFun mat_fun){}
  CallFun(VecFun vec_fun){}

};

struct Func {
  Func(int _a, double _b) : a(_a), b(_b){}
  Func(){};
  void operator()(double &out_mat){}
  private:
    int a;
    double b;
};

// struct PyFuncReader {
//   CPyObject pName;
//   CPyObject pModule;
//   CPyObject pFunc;
//   CPyObject pValue;
//   CPyObject pArgs;

//   CPyObject pArg1;


//   double operator()(double a, double b) {
//     double a1 = a;
//     double b1 = b;
//     pArg1 = PyFloat_FromDouble(a1);
//     cout << "pArg1 : " << PyFloat_AsDouble(pArg1) << endl;
//     cout << "size of Tuple : " << PyTuple_Size(pArgs) << endl;
//     cout << "Item 0 is set :" << PyTuple_SetItem(pArgs, 0, PyFloat_FromDouble(a1)) << endl;

//     pArg1 = PyFloat_FromDouble(b1);
//     cout << "pArg2 : " << PyFloat_AsDouble(pArg1) << endl;
//     PyTuple_SetItem(pArgs, 1, PyFloat_FromDouble(b1));

//     cout << "pArgs[0] : " << PyFloat_AsDouble(PyTuple_GetItem(pArgs, 0))
//          << endl;
//     cout << "pArgs[1] : " << PyFloat_AsDouble(PyTuple_GetItem(pArgs, 1))
//          << endl;

//     // for (int i = 0; i < 1; i++)
//     pValue = PyObject_CallObject(pFunc, pArgs);
    
         
//     return PyFloat_AsDouble(pValue);
//   }
//   PyFuncReader()
//       : pName(PyUnicode_FromString((char *)"rhsFunc")),
//         pModule(PyImport_Import(pName)),
//         pFunc(PyObject_GetAttrString(pModule, (char *)"rhs_func")),
//         pArgs(PyTuple_New(2)) {
//           cout << "PyFuncReader is constructed"  << endl;
//           if(pArgs){
//             cout << "Tuple is defined" << endl;
//           }
//         }
// };

// struct GetRead {

//   GetRead(PyFuncReader &aux_func) : auxFunc(aux_func) {}

//   PyFuncReader &auxFunc;

//   double get_value(double a, double b){
//     return auxFunc(a, b);
//   }
// };

int main(int argc, char *argv[]) {
  // setenv("PYTHONPATH", ".", 1);
  // CPyInstance hInstance;

  const char param_file[] = "param_file.petsc";
  MoFEM::Core::Initialize(&argc, &argv, param_file, help);
  try {
    moab::Core mb_instance;
    moab::Interface &moab = mb_instance;
    MoFEM::Core core(moab);
    DMType dm_name = "DMMOFEM";
    CHKERR DMRegister_MoFEM(dm_name);
    // int a = 5;

    // double b = 0.0;

    // CallFun A(Func(a, b));

    MatrixDouble out_mat;

    int nb_col = 10;
    out_mat.resize(1, nb_col, false);
    out_mat.clear();
    for(int gg = 0; gg != nb_col; ++gg){
      out_mat(0, gg) = gg;
    }


    auto t_out = FTensor::Tensor0<FTensor::PackPtr<double *, 1>>(&out_mat(0, 0));

   cout << "t_out : ";
   for (int gg = 0; gg != nb_col; ++gg) {
     cout << ", " << t_out; 
     ++t_out;
   }
   cout << endl;

    // CallFun B(Func());



    // double res;
    // PyFuncReader reader;
    // GetRead getRead(reader);

    // for (int i = 0; i < 10; i++) {
    //   double a = i;
    //   double b = i - 1;
    //   res = getRead.get_value(a, b);
    //   printf("getInteger(%f, %f) = %f\n", a, b, res);
    // }

    // boost::shared_ptr<MatrixDouble> Aptr;
    // Aptr = boost::shared_ptr<MatrixDouble>(new MatrixDouble()); 
    // MatrixDouble &B = *Aptr;

    // B.resize(2, 2, false);
    // B.clear();

    // B(1, 1) = 2;

    // cout << "A : " << (*Aptr)(1, 1) << endl; 

    // auto commonDataPtr = boost::shared_ptr<CommonData>(new CommonData());
    // int nb_gp = 2;

    // MatrixDouble &matA = *commonDataPtr->matPtr;

    // matA.resize(3, nb_gp, false);
    // matA.clear();

    // matA(0, 0) = 1;
    // matA(0, 1) = 3;
    // matA(1, 0) = 2;
    // matA(1, 1) = 4;
    // matA(2, 0) = 5;
    // matA(2, 1) = 6;

    // typedef boost::function<void(MatrixDouble &, const int)> Func;

    // Func func = RhsFunc(commonDataPtr);

    // MatrixDouble outMat;

    // func(outMat, nb_gp);

    // // cout << "outMat.size1() : " << outMat.size1() << endl;
    // // cout << "outMat.size2() : " << outMat.size2() << endl;

    // auto t_outmat = getFTensor1FromMat<3>(outMat);

    // // for(int gg = 0; gg != nb_gp; ++gg){
    // //   cout << "t_outmat : " << t_outmat << endl;
    // //   ++t_outmat;
    // // }

    // MatrixDouble mat;
    // VectorDouble vec;
    // int nb_rows = 10;
    // mat.resize(2, nb_rows, false);
    // mat.clear();

    // vec.resize(nb_rows, false);
    // vec.clear();

    // int count = 0;

    // for(int cc = 0; cc != 1; ++cc){
    //   for(int rr = 0; rr != nb_rows; ++rr){
    //     mat(cc, rr) = count;
    //     ++count;
    //   }
    // }
    // count = 0;
    // for(int cc = 0; cc != nb_rows; ++cc){
    //   vec[cc] = count;
    //   ++count;
    // }

    // // auto gP = pFTGP<3>(&mat(0, 0), &mat(1, 0), &mat(1, 0),
    // //                    &mat(1, 0), &mat(0, 0), &mat(1, 0),
    // //                    &mat(1, 0), &mat(1, 0), &mat(0, 0));

    // // auto gP = getFTensorDiag(mat);
    
    // // cout << "mat.size1() : " << mat.size1() << endl;
    // // cout << "sizeofdouble : " << SIZEOF_DOUBLE << endl;
    // // cout << "size(mat) : " << sizeof(mat) / SIZEOF_DOUBLE << endl;
    // // cout << "sizeof(gP) : " << sizeof(gP) / SIZEOF_DOUBLE << endl;


    // for(int gg = 0; gg != nb_rows; ++gg){
    //   auto gP = getFTensor2Diag(vec, gg);
    //   gP(i, j) *= gg;
    //   cout << gP(0, 0) << ", " << gP(0, 1) << ", " << gP(0, 2) << endl;
    //   cout << gP(1, 0) << ", " << gP(1, 1) << ", " << gP(1, 2) << endl;
    //   cout << gP(2, 0) << ", " << gP(2, 1) << ", " << gP(2, 2) << endl;
    //   cout << endl;
    //   cout <<"===========================================" << endl;
    //   cout << "vec[" << gg <<"] : " << vec[gg] << endl;
    //   cout << endl;
    // }

    // auto gp_coords = getCoordsAtGaussPts();

    // auto t_mat = getFTensor1FromMat<3>(

      std::string name = "ESSENTIAL";

      cout << "size of name : " << name.size() << endl;
  }
  CATCH_ERRORS;
  MoFEM::Core::Finalize();
  return 0;
}
