// #include <stdlib.h>
// #include <BasicFiniteElements.hpp>
// #include <PoissonOperators.hpp>

// using namespace MoFEM;
// using namespace PoissonOps;

// static char help[] = "...\n\n";

// struct Poisson {
// public:
//   Poisson(moab::Core &mb_instance, MoFEM::Core &core, int order);

//   MoFEMErrorCode runAnalysis();

// private:
//   MoFEMErrorCode readMesh();
//   MoFEMErrorCode setupSystem();
//   MoFEMErrorCode setIntegrationRule();
//   MoFEMErrorCode assembleSystem();
//   MoFEMErrorCode applyBc();
//   MoFEMErrorCode solveSystem();
//   MoFEMErrorCode setOutput();

//   boost::shared_ptr<VolEle> volPipeline;
//   boost::shared_ptr<VolEle> postFaceVolume;

//   MoFEM::Interface &mField;
//   moab::Interface &moab;
//   Simple *simpleInterface;
//   SmartPetscObj<DM> dm;
//   SmartPetscObj<KSP> ksp;

//   MPI_Comm mpiComm;  // mpi parallel communicator
//   const int mpiRank; // number of processors involved

//   int order;

//   std::string domainField;
// };

// Poisson::Poisson(moab::Core &mb_instance, MoFEM::Core &core, const int order)
//     : domainField("U"), moab(mb_instance), mField(core),
//       mpiComm(mField.get_comm()), mpiRank(mField.get_comm_rank()),
//       order(order) {
//   volPipeline = boost::shared_ptr<VolEle>(new VolEle(mField));
// }

// MoFEMErrorCode Poisson::readMesh() {
//   MoFEMFunctionBegin;

//   CHKERR mField.getInterface(simpleInterface);
//   CHKERR simpleInterface->getOptions();
//   CHKERR simpleInterface->loadFile();

//   MoFEMFunctionReturn(0);
// }

// MoFEMErrorCode Poisson::setupSystem() {
//   MoFEMFunctionBegin;

//   CHKERR simpleInterface->addDomainField(domainField, H1,
//                                          AINSWORTH_LEGENDRE_BASE, 1);
//   CHKERR simpleInterface->setFieldOrder(domainField, order);
//   CHKERR simpleInterface->setUp();

//   MoFEMFunctionReturn(0);
// }

// MoFEMErrorCode Poisson::setIntegrationRule() {
//   MoFEMFunctionBegin;
//   auto vol_rule = [](int, int, int p) -> int { return 2 * p; };
//   volPipeline->getRuleHook = vol_rule;
//   MoFEMFunctionReturn(0);
// }

// MoFEMErrorCode Poisson::assembleSystem() {
//   MoFEMFunctionBegin;

//   volPipeline->getOpPtrVector().push_back(
//       new OpLhsUU(domainField, domainField));
//   volPipeline->getOpPtrVector().push_back(new OpRhsU(domainField));

//   dm = simpleInterface->getDM();
//   boost::shared_ptr<VolEle> null;
//   CHKERR DMMoFEMKSPSetComputeOperators(dm, simpleInterface->getDomainFEName(),
//                                        volPipeline, null, null);

//   MoFEMFunctionReturn(0);
// }

// MoFEMErrorCode Poisson::solveSystem() {
//   MoFEMFunctionBegin;

//   Vec global_rhs, global_solution;
//   CHKERR DMCreateGlobalVector(dm, &global_rhs);
//   CHKERR VecDuplicate(global_rhs, &global_solution);

//   ksp = createKSP(mField.get_comm());

//   CHKERR KSPSetFromOptions(ksp);
//   CHKERR KSPSetDM(ksp, dm);

//   CHKERR KSPSetUp(ksp);

//   CHKERR KSPSolve(ksp, global_rhs, global_solution);

//   CHKERR DMoFEMMeshToGlobalVector(dm, global_solution, INSERT_VALUES,
//                                   SCATTER_REVERSE);

//   CHKERR DMoFEMLoopFiniteElements(dm, simpleInterface->getDomainFEName(),
//                                   postFaceVolume);
//   CHKERR boost::static_pointer_cast<PostProcFaceOnRefinedMesh>(postFaceVolume)
//       ->writeFile("out_vol.h5m");

//   CHKERR VecDestroy(&global_solution);
//   CHKERR VecDestroy(&global_rhs);

//   MoFEMFunctionReturn(0);
// }

// MoFEMErrorCode Poisson::setOutput() {
//   MoFEMFunctionBegin;

//   postFaceVolume =
//       boost::shared_ptr<VolEle>(new PostProcFaceOnRefinedMesh(mField));

//   CHKERR boost::static_pointer_cast<PostProcFaceOnRefinedMesh>(postFaceVolume)
//       ->generateReferenceElementMesh();
//   CHKERR boost::static_pointer_cast<PostProcFaceOnRefinedMesh>(postFaceVolume)
//       ->addFieldValuesPostProc(domainField);
//   CHKERR boost::static_pointer_cast<PostProcFaceOnRefinedMesh>(postFaceVolume)
//       ->addFieldValuesGradientPostProc(domainField);

//   MoFEMFunctionReturn(0);
// }

// MoFEMErrorCode Poisson::applyBc() {
//   MoFEMFunctionBegin;
//   Range bdry_entities;
//   for (_IT_CUBITMESHSETS_BY_SET_TYPE_FOR_LOOP_(mField, BLOCKSET, it)) {
//     string name = it->getName();
//     if (name.compare(0, 14, "NATURAL") == 0) {
//       CHKERR it->getMeshsetIdEntitiesByDimension(mField.get_moab(), 1,
//                                                  bdry_entities, true);
//     }
//   }

//   CHKERR mField.getInterface<ProblemsManager>()->removeDofsOnEntities(
//       simpleInterface->getProblemName(), domainField, bdry_entities);

//   MoFEMFunctionReturn(0);
// }

// MoFEMErrorCode Poisson::runAnalysis() {
//   MoFEMFunctionBegin;

//   CHKERR readMesh();
//   CHKERR setupSystem();
//   CHKERR setIntegrationRule();
//   CHKERR assembleSystem();
//   CHKERR setOutput();
//   CHKERR applyBc();
//   CHKERR solveSystem();

//   MoFEMFunctionReturn(0);
// }

// int main(int argc, char *argv[]) {
//   const char param_file[] = "param_file.petsc";
//   MoFEM::Core::Initialize(&argc, &argv, param_file, help);
//   try {
//     moab::Core mb_instance;
//     moab::Interface &moab = mb_instance;
//     MoFEM::Core core(moab);
//     DMType dm_name = "DMMOFEM";
//     CHKERR DMRegister_MoFEM(dm_name);

//     int order = 1;
//     CHKERR PetscOptionsGetInt(PETSC_NULL, "", "-order", &order, PETSC_NULL);

//     Poisson poisson_problem(mb_instance, core, order);

//     CHKERR poisson_problem.runAnalysis();
//   }
//   CATCH_ERRORS;
//   MoFEM::Core::Finalize();
//   return 0;
// }

#ifndef __POISSONOPERTATORS_HPP__
#define __POISSONOPERTATORS_HPP__

#include <stdlib.h>
#include <BasicFiniteElements.hpp>

namespace PoissonOps {

using VolEle = FaceElementForcesAndSourcesCore;
using OpVolEle = FaceElementForcesAndSourcesCore::UserDataOperator;

using FaceEle = MoFEM::EdgeElementForcesAndSourcesCore;
using OpFaceEle = FaceEle::UserDataOperator;

using EntData = DataForcesAndSourcesCore::EntData;

// Operators

FTensor::Index<'i', 3> i;

const double body_source = 1;

struct OpLhsUU : public OpVolEle {
public:
  OpLhsUU(std::string domain_field1, std::string domain_field2)
      : OpVolEle(domain_field1, domain_field2, OpVolEle::OPROWCOL) {
    sYmm = true;
  }

  MoFEMErrorCode doWork(int row_side, int col_side, EntityType row_type,
                        EntityType col_type, EntData &row_data,
                        EntData &col_data) {
    MoFEMFunctionBegin;

    const int nb_row_dofs = row_data.getIndices().size();
    const int nb_col_dofs = col_data.getIndices().size();

    if (nb_row_dofs && nb_col_dofs) {
      locLhs.resize(nb_row_dofs, nb_col_dofs, false);
      locLhs.clear();
      const int nb_integration_pts = getGaussPts().size2();
      auto t_w = getFTensor0IntegrationWeight();

      auto t_row_diff_base = row_data.getFTensor1DiffN<3>();

      const double vol = getMeasure();
      for (int gg = 0; gg != nb_integration_pts; ++gg) {
        const double a = t_w * vol;
        for (int rr = 0; rr != nb_row_dofs; ++rr) {
          auto t_col_diff_base = col_data.getFTensor1DiffN<3>(gg, 0);
          for (int cc = 0; cc != nb_col_dofs; ++cc) {
            locLhs(rr, cc) += a * t_row_diff_base(i) * t_col_diff_base(i);
            ++t_col_diff_base;
          } // endFor cc
          ++t_row_diff_base;
        } // endFor rr
        ++t_w;
      } // endFor gg
      CHKERR MatSetValues(getFEMethod()->ksp_B, row_data, col_data,
                          &locLhs(0, 0), ADD_VALUES);
      if (row_side != col_side || row_type != col_type) {
        transLocLhs.resize(nb_col_dofs, nb_row_dofs, false);
        noalias(transLocLhs) = trans(locLhs);
        CHKERR MatSetValues(getFEMethod()->ksp_B, col_data, row_data,
                            &transLocLhs(0, 0), ADD_VALUES);
      } // (row_side != col_side || row_type != col_type)
    }   // endIf (nb_row_dofs && nb_col_dofs)

    MoFEMFunctionReturn(0);
  }

private:
  MatrixDouble locLhs;
  MatrixDouble transLocLhs;
};

struct OpRhsU : public OpVolEle {
public:
  OpRhsU(std::string domain_field) : OpVolEle(domain_field, OpVolEle::OPROW) {}
  MoFEMErrorCode doWork(int side, EntityType type, EntData &data) {
    MoFEMFunctionBegin;

    const int nb_dofs = data.getIndices().size();
    if (nb_dofs) {
      locRhs.resize(nb_dofs, false);
      locRhs.clear();
      const int nb_integration_pts = getGaussPts().size2();

      auto t_base = data.getFTensor0N();
      auto t_w = getFTensor0IntegrationWeight();
      const double vol = getMeasure();
      for (int gg = 0; gg != nb_integration_pts; ++gg) {
        const double a = vol * t_w;

        for (int rr = 0; rr != nb_dofs; ++rr) {
          locRhs[rr] += a * (t_base * body_source);
          ++t_base;
        } // endFor rr
        ++t_w;
      } // endFor gg
      CHKERR VecSetOption(getFEMethod()->ksp_f, VEC_IGNORE_NEGATIVE_INDICES,
                          PETSC_TRUE);
      CHKERR VecSetValues(getFEMethod()->ksp_f, data, &*locRhs.begin(),
                          ADD_VALUES);
    } // endIf (nb_dofs)

    MoFEMFunctionReturn(0);
  }

private:
  VectorDouble locRhs;
};

}; // namespace PoissonOps

#endif //__POISSONOPERTATORS_HPP__
