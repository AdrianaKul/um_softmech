#include <stdlib.h>
#include <BasicFiniteElements.hpp>
#include <quadOperators.hpp>

using namespace MoFEM;
using namespace QuadOps;

static char help[] = "...\n\n";

template <int p, int q>
struct ShapeFunction {
public:
  ShapeFunction(moab::Core &mb_instance, MoFEM::Core &core);

  MoFEMErrorCode runAnalysis();

private:
  MoFEMErrorCode readMesh();
  MoFEMErrorCode setupSystem();
  MoFEMErrorCode assembleSystem();

  MoFEMErrorCode setOutput();

  boost::shared_ptr<FaceEle> quadPipeline;

  boost::shared_ptr<FaceEle> quadPostproc;


  MoFEM::Interface &mField;
  moab::Interface &moab;
  Simple *simpleInterface;

  DM dm;


  MPI_Comm mpiComm;  // mpi parallel communicator
  const int mpiRank; // number of processors involved

  std::string funcName;
};
template <int p, int q>
ShapeFunction<p, q>::ShapeFunction(moab::Core &mb_instance, MoFEM::Core &core)
    : funcName("N_pq")
    , moab(mb_instance), mField(core)
    , mpiComm(mField.get_comm())
    , mpiRank(mField.get_comm_rank()) 
    {
      quadPipeline = boost::shared_ptr<FaceEle>(new FaceEle(mField));
    }

template <int p, int q>
MoFEMErrorCode ShapeFunction<p, q>::readMesh() {
  MoFEMFunctionBegin;

  CHKERR mField.getInterface(simpleInterface);
  CHKERR simpleInterface->getOptions();
  CHKERR simpleInterface->loadFile();

  MoFEMFunctionReturn(0);
}

template <int p, int q>
MoFEMErrorCode ShapeFunction<p, q>::setupSystem() {
  MoFEMFunctionBegin;

  CHKERR simpleInterface->addDomainField(funcName, H1, AINSWORTH_LEGENDRE_BASE, 2);
  CHKERR simpleInterface->setFieldOrder(funcName, 1);
  CHKERR simpleInterface->setUp();

  MoFEMFunctionReturn(0);
}

template <int p, int q>
MoFEMErrorCode ShapeFunction<p, q>::assembleSystem() {
  MoFEMFunctionBegin;
  quadPipeline->getOpPtrVector().push_back(new OpShapeFunction<p, q>(funcName));

  // dm = simpleInterface->getDM();
  CHKERR simpleInterface->getDM(&dm);

  CHKERR DMoFEMLoopFiniteElements(dm, simpleInterface->getDomainFEName(), quadPipeline);
  MoFEMFunctionReturn(0);
}

template <int p, int q>
MoFEMErrorCode ShapeFunction<p, q>::setOutput() {
  MoFEMFunctionBegin;
  // CHKERR DMoFEMMeshToGlobalVector(dm, global_solution, INSERT_VALUES, SCATTER_REVERSE);
  
  quadPostproc =
      boost::shared_ptr<FaceEle>(new PostProcFaceOnRefinedMesh(mField));

  CHKERR
  boost::static_pointer_cast<PostProcFaceOnRefinedMesh>(quadPostproc)
      ->generateReferenceElementMesh();
  CHKERR
  boost::static_pointer_cast<PostProcFaceOnRefinedMesh>(quadPostproc)
      ->addFieldValuesPostProc(funcName);

  CHKERR DMoFEMLoopFiniteElements(dm, simpleInterface->getDomainFEName(),
                                  quadPostproc);

  CHKERR
  boost::static_pointer_cast<PostProcFaceOnRefinedMesh>(quadPostproc)
      ->writeFile("shape_function.h5m");

  CHKERR DMDestroy(&dm);

  MoFEMFunctionReturn(0);
}




template <int p, int q>
MoFEMErrorCode ShapeFunction<p, q>::runAnalysis() {
  MoFEMFunctionBegin;

  CHKERR readMesh();
  CHKERR setupSystem();
  CHKERR assembleSystem();
  CHKERR setOutput();

  MoFEMFunctionReturn(0);
}

int main(int argc, char *argv[]) {
  const char param_file[] = "param_file.petsc";
  MoFEM::Core::Initialize(&argc, &argv, param_file, help);
  try {
    moab::Core mb_instance;
    moab::Interface &moab = mb_instance;
    MoFEM::Core core(moab);
    DMType dm_name = "DMMOFEM";
    CHKERR DMRegister_MoFEM(dm_name);
    

    ShapeFunction<3, 3> shape_function(mb_instance, core);

    CHKERR shape_function.runAnalysis();
  }
  CATCH_ERRORS;
  MoFEM::Core::Finalize();
  return 0;
}