#ifndef __QUADOPERTATORS_HPP__
#define __QUADOPERTATORS_HPP__

#include <stdlib.h>
#include <BasicFiniteElements.hpp>

namespace QuadOps {

using FaceEle = FaceElementForcesAndSourcesCore;
using OpFaceEle = FaceElementForcesAndSourcesCore::UserDataOperator;

using EdgeEle = MoFEM::EdgeElementForcesAndSourcesCore;
using OpEdgeEle = EdgeEle::UserDataOperator;

using EntData = DataForcesAndSourcesCore::EntData;



template <int p, int q>
struct OpShapeFunction : public OpFaceEle {
public:
  OpShapeFunction(std::string domain_field)
      : OpFaceEle(domain_field, OpFaceEle::OPROW) 
      {}
  MoFEMErrorCode doWork(int side, EntityType type, EntData &data) {
    MoFEMFunctionBegin;
    
    const int nb_dofs = data.getIndices().size();
    
    if (nb_dofs) {

      nF.resize(nb_dofs, false);
      nF.clear();
      auto t_coords = getFTensor1Coords();
      MatrixDouble N(nb_dofs, 4);
      for (int i = 0; i != nb_dofs; i++) {
        double x = t_coords(0);
        double y = t_coords(1);
    
        N(i, 0) = N_MBQUAD0(x, y);
        N(i, 1) = N_MBQUAD1(x, y);
        N(i, 2) = N_MBQUAD2(x, y);
        N(i, 3) = N_MBQUAD3(x, y);
        ++t_coords;
      }
      // // Edge Hcurl basis
      // int sense[] = {1, 1, -1, -1};
      // int order[] = {3, 3};
      // int P[4] = {order[0], order[1], order[0], order[1]};

      // double * curl_val_edges_ptr[4];
      // std::array<MatrixDouble, 4> curl_val_edges;
      // for (auto ee : {0, 1, 2, 3}) {
      //   curl_val_edges[ee] = MatrixDouble(3, 2 * P[ee]);
      //   curl_val_edges[ee].clear();
      //   curl_val_edges_ptr[ee] = &curl_val_edges[ee](0, 0);
      // }
      // double *curl_curl_edges_ptr[4];
      // std::array<MatrixDouble, 4> curl_curl_edges;
      // for (auto ee : {0, 1, 2, 3}) {
      //   curl_curl_edges[ee] = MatrixDouble(3, P[ee]);
      //   curl_curl_edges_ptr[ee] = &curl_curl_edges[ee](0, 0);
      // }

      // CHKERR MoFEM::Hcurl_EdgeShapeFunctions_ONQUAD(sense, order,
      //                                               &*N.data().begin(),
      //                                               curl_val_edges_ptr,
      //                                               curl_curl_edges_ptr, 
      //                                               3);
      // int edge_nb = 2;
      // int base_nb = 1;

      // int nn[6] = {2 * base_nb + 0, 2 * base_nb + 1, 2 * base_nb + 0,
      //              2 * base_nb + 1, 2 * base_nb + 0, 2 * base_nb + 1};
      // int bb[6] = {0, 0, 1, 1, 2, 2};
      // int j = 0;
      // for (auto &dof : data.getFieldDofs()){
      //     dof->getFieldData() = curl_val_edges[edge_nb](bb[j], nn[j]);
      //     j++;
      // }

      // Face Hcurl basis
      int order[] = {p, q};

      int pq[] = {p, q};
      int qp[] = {q, p};

      double *curl_val_face_ptr[2];
      std::array<MatrixDouble, 2> curl_val_face;
      for (auto typ : {0, 1}) {
        curl_val_face[typ] = MatrixDouble(3, 2 * (pq[typ] - 1) * qp[typ]);
        curl_val_face[typ].clear();
        curl_val_face_ptr[typ] = &curl_val_face[typ](0, 0);
      }

      double *curl_curl_face_ptr[2];
      std::array<MatrixDouble, 2> curl_curl_face;
      for (auto typ : {0, 1})
      {
        curl_curl_face[typ] = MatrixDouble(3, (pq[typ] - 1) * qp[typ]);
        curl_curl_face[typ].clear();
        curl_curl_face_ptr[typ] = &curl_curl_face[typ](0, 0);
      }
      CHKERR MoFEM::Hdiv_FaceShapeFunctions_ONQUAD(order, 
                                                    &*N.data().begin(), 
                                                    curl_val_face_ptr,
                                                    curl_curl_face_ptr, 
                                                    3);

                                                 

      int typ = 0;
      int base_nb = 3;

      int nn[6] = {2 * base_nb + 0, 2 * base_nb + 1, 2 * base_nb + 0,
                   2 * base_nb + 1, 2 * base_nb + 0, 2 * base_nb + 1};

      int bb[6] = {0, 0, 1, 1, 2, 2};
      int j = 0;
      
      for (auto &dof : data.getFieldDofs()) {
        dof->getFieldData() = curl_val_face[0](bb[j], nn[j]) +  
                              curl_val_face[1](bb[j], nn[j]) ;
        j++;
      }
    } // nb_dofs != 0 

    MoFEMFunctionReturn(0);
  }

private:
  VectorDouble nF;
};

} // QuadOps

#endif // __QUADOPERTATORS_HPP__