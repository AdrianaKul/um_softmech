#ifndef __HEXOPERTATORS_HPP__
#define __HEXOPERTATORS_HPP__

#include <stdlib.h>
#include <BasicFiniteElements.hpp>

namespace HexOps {

using VolumeEle = VolumeElementForcesAndSourcesCore;
using OpVolumeEle = VolumeEle::UserDataOperator;

using FaceEle = FaceElementForcesAndSourcesCore;
using OpFaceEle = FaceEle::UserDataOperator;

using EntData = DataForcesAndSourcesCore::EntData;




struct OpShapeFunction : public OpVolumeEle {
public:
  OpShapeFunction(std::string domain_field)
      : OpVolumeEle(domain_field, OPROW) {}
  MoFEMErrorCode doWork(int side, EntityType type, EntData &data) {
    MoFEMFunctionBegin;
    
    const int nb_dofs = data.getIndices().size();
    
    if (nb_dofs) {

      int p = 1;
      CHKERR PetscOptionsGetInt(PETSC_NULL, "", "-order", &p, PETSC_NULL);
  

      auto coords = getCoords();

      FTensor::Tensor1<FTensor::PackPtr<double *, 3>, 3> t_coords(
                                          &coords[0], &coords[1], &coords[2]);


      const int nb_tet_verts = 4;
      const int nb_hex_verts = 8;
      const int nb_hex_edges = 12;
      MatrixDouble N(nb_tet_verts, nb_hex_verts);
      N.clear();
      for (int i = 0; i != nb_tet_verts; i++) {

        double x = t_coords(0);
        double y = t_coords(1);
        double z = t_coords(2);


        double xyz[nb_hex_verts][3] ={{1.0 - x, 1.0 - y, 1.0 - z},
                                      {      x, 1.0 - y, 1.0 - z},
                                      {      x,       y, 1.0 - z},
                                      {1.0 - x,       y, 1.0 - z},
                                      {1.0 - x, 1.0 - y,       z},
                                      {      x, 1.0 - y,       z},
                                      {      x,       y,       z},
                                      {1.0 - x,       y,       z}};
        for (int nb = 0; nb != nb_hex_verts; nb++){
          N(i, nb) = xyz[nb][0] * xyz[nb][1] * xyz[nb][2];
        }
        ++t_coords;
      }

      //===================H1 Edge on Hex=====================
      
      // int sense[] = {1, 1, 1, 1, 1, 1, 1, 1};
      // int order[] = {p, q, r};

      // int pq[nb_hex_edges] = {p, q, p, q, r, r, r, r, p, q, p, q};

      // double *h1_val_edge_ptr[nb_hex_edges];
      // double *h1_diff_edge_ptr[nb_hex_edges];
      // std::array<MatrixDouble, nb_hex_edges> h1_val_edge;
      // std::array<MatrixDouble, nb_hex_edges> h1_diff_edge;
      // for (int ee = 0; ee != nb_hex_edges; ee++) {
      //   h1_val_edge[ee] = MatrixDouble(nb_tet_verts, pq[ee]-1) ;
      //   h1_diff_edge[ee] = MatrixDouble(nb_tet_verts, 3 * (pq[ee] - 1));
      //   h1_val_edge[ee].clear();
      //   h1_diff_edge[ee].clear();
      //   h1_val_edge_ptr[ee] = &h1_val_edge[ee](0, 0);
      //   h1_diff_edge_ptr[ee] = &h1_diff_edge[ee](0, 0);
      // }



      // CHKERR MoFEM::H1_EdgeShapeFunctions_ONHEX(sense, 
      //                                           order,
      //                                           &*N.data().begin(),
      //                                           h1_val_edge_ptr,
      //                                           h1_diff_edge_ptr,
      //                                           nb_tet_verts);

      // int n_edge = 0;
      // int n_base = 0;
      // int j = 0;
      // for (auto &dof : data.getFieldDofs()){
      //     dof->getFieldData() = h1_val_edge[n_edge](j, n_base);
      //     j++;
      // }

      // //===================H1 Face on Hex=====================
      //  const int nb_hex_faces = 6;
    
      // int faces_1[6][4] = {{0, 1, 2, 3}, 
      //                    {0, 1, 5, 4}, 
      //                    {1, 2, 6, 5},
      //                    {3, 2, 6, 7}, 
      //                    {0, 3, 7, 4}, 
      //                    {4, 5, 6, 7}};
      // int *faces[6] = {faces_1[0],
      //                  faces_1[1],
      //                  faces_1[2],
      //                  faces_1[3],
      //                  faces_1[4],
      //                  faces_1[5]};

      // int order[] = {p, p, p};


      // int pq[nb_hex_faces][2] = {{p, p}, {p, p}, {q, p}, {p, p}, {q, p}, {p, p}};

      

  

      // double *h1_val_face_ptr[nb_hex_faces];
      // double *h1_diff_face_ptr[nb_hex_faces];
      // std::array<MatrixDouble, nb_hex_faces> h1_val_face;
      // std::array<MatrixDouble, nb_hex_faces> h1_diff_face;
      // for (int ff = 0; ff != nb_hex_faces; ff++) {
      //   h1_val_face[ff] = MatrixDouble(nb_tet_verts, (pq[ff][0] - 1) * (pq[ff][1] - 1));
      //   h1_diff_face[ff] = MatrixDouble(nb_tet_verts, 3 * (pq[ff][0] - 1) * (pq[ff][1] - 1));
      //   h1_val_face[ff].clear();
      //   h1_diff_face[ff].clear();
      //   h1_val_face_ptr[ff] = &h1_val_face[ff](0, 0);
      //   h1_diff_face_ptr[ff] = &h1_diff_face[ff](0, 0);
      // }


      

      // CHKERR MoFEM::H1_FaceShapeFunctions_ONHEX(
      //     faces, order, &*N.data().begin(), h1_val_face_ptr, h1_diff_face_ptr,
      //     nb_tet_verts);

      // int n_face = 0;
      // int n_base = 0;

      // CHKERR PetscOptionsGetInt(PETSC_NULL, "", "-n_face", &n_face, PETSC_NULL);
      // CHKERR PetscOptionsGetInt(PETSC_NULL, "", "-n_base", &n_base, PETSC_NULL);
      // int j = 0;
      // for (auto &dof : data.getFieldDofs()) {
      //   dof->getFieldData() = h1_val_face[n_face](j, n_base);
      //   j++;
      // }

      // //===================H1 Interior on Hex=====================
      // int order[] = {p, p, p};



      // MatrixDouble  h1_val_inte;
      // MatrixDouble  h1_diff_inte;

      // const int sz = (p - 1) * (p - 1) * (p -1);
 
      // h1_val_inte = MatrixDouble(nb_tet_verts, sz);
      // h1_diff_inte = MatrixDouble(nb_tet_verts, 3 * sz);
      // h1_val_inte.clear();
      // h1_diff_inte.clear();
    

      // CHKERR MoFEM::H1_InteriorShapeFunctions_ONHEX(order, 
      //                                               &*N.data().begin(),  
      //                                               &*h1_val_inte.data().begin(),
      //                                               &*h1_diff_inte.data().begin(), 
      //                                               nb_tet_verts);
                                                                                             

      // int n_base = 7;

      // CHKERR PetscOptionsGetInt(PETSC_NULL, "", "-n_base", &n_base, PETSC_NULL);
      // int j = 0;
      // for (auto &dof : data.getFieldDofs()) {
      //   dof->getFieldData() = h1_val_inte(j, n_base);
      //   j++;
      // }

      //===================================================================
      // // Edge Hcurl basis
      // int sense[] = {1, -1, 1, -1};
      // int order[] = {3, 4};
      // int P[4] = {order[0], order[1], order[0], order[1]};

      // double * curl_val_edges_ptr[4];
      // std::array<MatrixDouble, 4> curl_val_edges;
      // for (auto ee : {0, 1, 2, 3}) {
      //   curl_val_edges[ee] = MatrixDouble(3, 2 * P[ee]);
      //   curl_val_edges[ee].clear();
      //   curl_val_edges_ptr[ee] = &curl_val_edges[ee](0, 0);
      // }
      // double *curl_curl_edges_ptr[4];
      // std::array<MatrixDouble, 4> curl_curl_edges;
      // for (auto ee : {0, 1, 2, 3}) {
      //   curl_curl_edges[ee] = MatrixDouble(3, P[ee]);
      //   curl_curl_edges_ptr[ee] = &curl_curl_edges[ee](0, 0);
      // }

     
      // int edge_nb = 3;
      // int base_nb = 0;

      // int nn[6] = {2 * base_nb + 0, 2 * base_nb + 1, 2 * base_nb + 0,
      //              2 * base_nb + 1, 2 * base_nb + 0, 2 * base_nb + 1};
      // int bb[6] = {0, 0, 1, 1, 2, 2};
      // int j = 0;
      // for (auto &dof : data.getFieldDofs()){
      //     dof->getFieldData() = curl_val_edges[edge_nb](bb[j], nn[j]);
      //     j++;
      // }

      // // Face Hcurl basis
      // int order[] = {p, q};

      // int pq[] = {p, q};
      // int qp[] = {q, p};

      // double *curl_val_face_ptr[2];
      // std::array<MatrixDouble, 2> curl_val_face;
      // for (auto typ : {0, 1}) {
      //   curl_val_face[typ] = MatrixDouble(3, 2 * (pq[typ] - 1) * qp[typ]);
      //   curl_val_face[typ].clear();
      //   curl_val_face_ptr[typ] = &curl_val_face[typ](0, 0);
      // }

      // double *curl_curl_face_ptr[2];
      // std::array<MatrixDouble, 2> curl_curl_face;
      // for (auto typ : {0, 1})
      // {
      //   curl_curl_face[typ] = MatrixDouble(3, (pq[typ] - 1) * qp[typ]);
      //   curl_curl_face[typ].clear();
      //   curl_curl_face_ptr[typ] = &curl_curl_face[typ](0, 0);
      // }
      // CHKERR MoFEM::Hdiv_FaceShapeFunctions_ONQUAD(order, 
      //                                               &*N.data().begin(), 
      //                                               curl_val_face_ptr,
      //                                               curl_curl_face_ptr, 
      //                                               3);

                                                 

      // int typ = 0;
      // int base_nb = 4;

      // int nn[6] = {2 * base_nb + 0, 2 * base_nb + 1, 2 * base_nb + 0,
      //              2 * base_nb + 1, 2 * base_nb + 0, 2 * base_nb + 1};

      // int bb[6] = {0, 0, 1, 1, 2, 2};
      // int j = 0;
      // for (auto &dof : data.getFieldDofs()) {
      //   dof->getFieldData() = curl_val_face[0](bb[j], nn[j]) +  
      //                         curl_val_face[1](bb[j], nn[j]) ;
      //   j++;
      // }

      // // Edge Hcurl on Hex **********************************************

      // int sense[12] = {1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1};
      // int order[3] = {p, p, p};

      // int pq[nb_hex_edges] = {p, p, p, p, p, p, p, p, p, p, p, p};

      // double *hcurl_val_edge_ptr[nb_hex_edges];
      // double *hcurl_curl_edge_ptr[nb_hex_edges];
      // std::array<MatrixDouble, nb_hex_edges> hcurl_val_edge;
      // std::array<MatrixDouble, nb_hex_edges> hcurl_curl_edge;
      // for (int ee = 0; ee != nb_hex_edges; ee++) {
      //   hcurl_val_edge[ee] = MatrixDouble(nb_tet_verts, 3 * pq[ee]) ;
      //   hcurl_curl_edge[ee] = MatrixDouble(nb_tet_verts, 3 * pq[ee]);
      //   hcurl_val_edge[ee].clear();
      //   hcurl_curl_edge[ee].clear();
      //   hcurl_val_edge_ptr[ee] = &hcurl_val_edge[ee](0, 0);
      //   hcurl_curl_edge_ptr[ee] = &hcurl_curl_edge[ee](0, 0);
      // }

      // CHKERR MoFEM::Hcurl_EdgeShapeFunctions_ONHEX(sense,
      //                                           order,
      //                                           &*N.data().begin(),
      //                                           hcurl_val_edge_ptr,
      //                                           hcurl_curl_edge_ptr,
      //                                           nb_tet_verts);

      // int n_edge = 0;
      // int n_base = 0;

      // CHKERR PetscOptionsGetInt(PETSC_NULL, "", "-n_edge", &n_edge, PETSC_NULL);
      // CHKERR PetscOptionsGetInt(PETSC_NULL, "", "-n_base", &n_base, PETSC_NULL);

      // int nn[12] = {3 * n_base + 0,  3 * n_base + 1,  3 * n_base + 2,
      //               3 * n_base + 0,  3 * n_base + 1,  3 * n_base + 2,
      //               3 * n_base + 0,  3 * n_base + 1,  3 * n_base + 2,
      //               3 * n_base + 0,  3 * n_base + 1,  3 * n_base + 2};

      

      // int bb[12] = {0, 0, 0, 1, 1, 1, 2, 2, 2, 3, 3, 3};
      // int j = 0;
      // cout << "nb_dofs: " << nb_dofs << endl;
      // for (auto &dof : data.getFieldDofs()) {
      //   dof->getFieldData() = hcurl_val_edge[n_edge](bb[j], nn[j]);
      //   j++;
      // }

    //  // Edge Hcurl on Hex **********************************************

    //   int sense[12] = {1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1};
    //   int order[3] = {p, p, p};

    //   int pq[nb_hex_edges] = {p, p, p, p, p, p, p, p, p, p, p, p};

    //   double *hcurl_val_edge_ptr[nb_hex_edges];
    //   double *hcurl_curl_edge_ptr[nb_hex_edges];
    //   std::array<MatrixDouble, nb_hex_edges> hcurl_val_edge;
    //   std::array<MatrixDouble, nb_hex_edges> hcurl_curl_edge;
    //   for (int ee = 0; ee != nb_hex_edges; ee++) {
    //     hcurl_val_edge[ee] = MatrixDouble(nb_tet_verts, 3 * pq[ee]) ;
    //     hcurl_curl_edge[ee] = MatrixDouble(nb_tet_verts, 3 * pq[ee]);
    //     hcurl_val_edge[ee].clear();
    //     hcurl_curl_edge[ee].clear();
    //     hcurl_val_edge_ptr[ee] = &hcurl_val_edge[ee](0, 0);
    //     hcurl_curl_edge_ptr[ee] = &hcurl_curl_edge[ee](0, 0);
    //   }

    //   CHKERR MoFEM::Hcurl_EdgeShapeFunctions_ONHEX(sense,
    //                                             order,
    //                                             &*N.data().begin(),
    //                                             hcurl_val_edge_ptr,
    //                                             hcurl_curl_edge_ptr,
    //                                             nb_tet_verts);

    //   int n_edge = 0;
    //   int n_base = 0;

    //   CHKERR PetscOptionsGetInt(PETSC_NULL, "", "-n_edge", &n_edge, PETSC_NULL);
    //   CHKERR PetscOptionsGetInt(PETSC_NULL, "", "-n_base", &n_base, PETSC_NULL);

    //   int nn[12] = {3 * n_base + 0,  3 * n_base + 1,  3 * n_base + 2,
    //                 3 * n_base + 0,  3 * n_base + 1,  3 * n_base + 2,
    //                 3 * n_base + 0,  3 * n_base + 1,  3 * n_base + 2,
    //                 3 * n_base + 0,  3 * n_base + 1,  3 * n_base + 2};

      

    //   int bb[12] = {0, 0, 0, 1, 1, 1, 2, 2, 2, 3, 3, 3};
    //   int j = 0;
    //   cout << "nb_dofs: " << nb_dofs << endl;
    //   for (auto &dof : data.getFieldDofs()) {
    //     dof->getFieldData() = hcurl_val_edge[n_edge](bb[j], nn[j]);
    //     j++;
    //   }
      // // //===================H1 Face on Hex=====================
      //  const int nb_hex_faces = 6;

      // int faces_1[6][4] = {{0, 1, 2, 3},
      //                      {0, 1, 5, 4},
      //                      {1, 2, 6, 5},
      //                      {3, 2, 6, 7},
      //                      {0, 3, 7, 4},
      //                      {4, 5, 6, 7}};
      // int *faces[6] = {faces_1[0],
      //                  faces_1[1],
      //                  faces_1[2],
      //                  faces_1[3],
      //                  faces_1[4],
      //                  faces_1[5]};

      // int order[3] = {p, p, p};

      // int pq[nb_hex_faces][6] = {{p, p}, {p, p}, {p, p}, {p, p}, {p, p}, {p, p}};

      // double *hcurl_val_face_ptr[nb_hex_faces][2];
      // double *hcurl_curl_face_ptr[nb_hex_faces][2];
      // std::array<MatrixDouble, 2 * nb_hex_faces> hcurl_val_face;
      // std::array<MatrixDouble, 2 * nb_hex_faces> hcurl_curl_face;
      // for (int ff = 0; ff != nb_hex_faces; ff++) {
      //   hcurl_val_face[2 * ff + 0] = MatrixDouble(nb_tet_verts, 3 * (p - 1) * p); 
      //   hcurl_val_face[2 * ff + 1] = MatrixDouble(nb_tet_verts, 3 * (p - 1) * p);
      //   hcurl_curl_face[2 * ff + 0] = MatrixDouble(nb_tet_verts, 3 * (p - 1) * p);
      //   hcurl_curl_face[2 * ff + 1] = MatrixDouble(nb_tet_verts, 3 * (p - 1) * p);

      //   hcurl_val_face[2 * ff + 0].clear();
      //   hcurl_val_face[2 * ff + 1].clear();
      //   hcurl_curl_face[2 * ff + 0].clear();
      //   hcurl_curl_face[2 * ff + 1].clear();

      //   hcurl_val_face_ptr[ff][0] = &hcurl_val_face[2 * ff + 0](0, 0);
      //   hcurl_val_face_ptr[ff][1] = &hcurl_val_face[2 * ff + 1](0, 0);
      //   hcurl_curl_face_ptr[ff][0] = &hcurl_curl_face[2 * ff + 0](0, 0);
      //   hcurl_curl_face_ptr[ff][1] = &hcurl_curl_face[2 * ff + 1](0, 0);
      // }

      // CHKERR MoFEM::Hcurl_FaceShapeFunctions_ONHEX(
      //     faces, order, &*N.data().begin(), hcurl_val_face_ptr,
      //     hcurl_curl_face_ptr, nb_tet_verts);

      // int n_face = 0;
      // int n_base = 0;

      // CHKERR PetscOptionsGetInt(PETSC_NULL, "", "-n_edge", &n_face,
      // PETSC_NULL); CHKERR PetscOptionsGetInt(PETSC_NULL, "", "-n_base",
      // &n_base, PETSC_NULL);

      // int nn[12] = {3 * n_base + 0,  3 * n_base + 1,  3 * n_base + 2,
      //               3 * n_base + 0,  3 * n_base + 1,  3 * n_base + 2,
      //               3 * n_base + 0,  3 * n_base + 1,  3 * n_base + 2,
      //               3 * n_base + 0,  3 * n_base + 1,  3 * n_base + 2};

      // int bb[12] = {0, 0, 0, 1, 1, 1, 2, 2, 2, 3, 3, 3};
      // int j = 0;
      
      // for (auto &dof : data.getFieldDofs()) {
      //   dof->getFieldData() = hcurl_val_face[2 * n_face + 1](bb[j], nn[j]) +
      //                         hcurl_val_face[2 * n_face + 0](bb[j], nn[j]);
      //   j++;
      // }

      // //===================H1 Face on Hex=====================

      int order[3] = {p, p, p};


      double *hcurl_val_face_ptr[3];
      double *hcurl_curl_face_ptr[3];
      std::array<MatrixDouble, 3> hcurl_val_face;
      std::array<MatrixDouble, 3> hcurl_curl_face;
      for (int fam = 0; fam != 3; fam++) {
        hcurl_val_face[fam] =
            MatrixDouble(nb_tet_verts, 3 * (p - 1) * p * p);
        hcurl_curl_face[fam] =
            MatrixDouble(nb_tet_verts, 3 * (p - 1) * p * p);

        hcurl_val_face[fam].clear();
        hcurl_curl_face[fam].clear();

        hcurl_val_face_ptr[fam] = &hcurl_val_face[fam](0, 0);      
        hcurl_curl_face_ptr[fam] = &hcurl_curl_face[fam](0, 0);
        
      }

      CHKERR MoFEM::Hdiv_InteriorShapeFunctions_ONHEX(
          order, &*N.data().begin(), hcurl_val_face_ptr,
          hcurl_curl_face_ptr, nb_tet_verts);


      int n_base = 0;

      CHKERR PetscOptionsGetInt(PETSC_NULL, "", "-n_base", &n_base, PETSC_NULL);

      int nn[12] = {3 * n_base + 0, 3 * n_base + 1, 3 * n_base + 2,
                    3 * n_base + 0, 3 * n_base + 1, 3 * n_base + 2,
                    3 * n_base + 0, 3 * n_base + 1, 3 * n_base + 2,
                    3 * n_base + 0, 3 * n_base + 1, 3 * n_base + 2};

      int bb[12] = {0, 0, 0, 1, 1, 1, 2, 2, 2, 3, 3, 3};
      int j = 0;

      for (auto &dof : data.getFieldDofs()) {
        dof->getFieldData() = hcurl_val_face[0](bb[j], nn[j]) +
                              hcurl_val_face[1](bb[j], nn[j]) +
                              hcurl_val_face[2](bb[j], nn[j]);
        j++;
      }
    }
    MoFEMFunctionReturn(0);
  }

};

} // HexOps

#endif // __HEXOPERTATORS_HPP__